package com.cashmanager.nwl;


import android.test.InstrumentationTestCase;

import com.cashmanager.constants.AppConstants;
import com.cashmanager.dao.CountDetailsDao;
import com.cashmanager.dao.CountHistoryDao;
import com.cashmanager.dao.stub.CountDetailDaoStub;
import com.cashmanager.dao.stub.CountHistoryDaoStub;
import com.cashmanager.pojo.CountDetails;
import com.cashmanager.pojo.CountHistory;
import com.cashmanager.pojo.Denominations;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Dinesh on 28/06/17.
 */

public class CountHistoryTest extends InstrumentationTestCase {

    private CountHistory countHistory;
    private CountHistoryDao countHistoryDao;
    private CountDetailsDao countDetailsDao;
    private boolean saved;
    private ArrayList<Denominations> denominationsList;
    private ArrayList<CountDetails> countDetailList;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        countHistoryDao = new CountHistoryDaoStub();
        countDetailsDao = new CountDetailDaoStub();
    }

    public void testFloatHistoryData() {
        initilaizeDenomination();
        initializeSampleData();
        saveSampleData();
        //deleteSampleData();
        //verifyException();
    }

    private void updateSampleData(long id) {
        try {
            int k = 1;
            List<CountHistory> countHistoryList = countHistoryDao.findAllUnSyncedCountHistory();
            if (countHistoryList == null) {
                return;
            }
            for (CountHistory temp : countHistoryList) {
                temp.setSynced(true);
                temp.setServerId(k++ + "");
                List<CountDetails> countDetailList = countDetailsDao.getAllCountDetailByCountId(temp.getCountId());
                for (CountDetails temp1 : countDetailList) {
                    temp.setSynced(true);
                    temp.setServerId(k++ + "");
                }
                countHistoryDao.update(temp);
                countDetailsDao.updateAll(countDetailList);
                assertTrue(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }


    private void initializeSampleData() {
        countHistory = new CountHistory();
        countHistory.setRegisterId("aa");
        countHistory.setCountType(AppConstants.COUNT_TYPE_CHECK_BALANCE);
        countHistory.setNotes("Some value chnaged");
        countHistory.setRegisterId("1");
        countHistory.setSynced(false);
        countHistory.setCountBy("dinesh");
        countHistory.setSystemCountValue(20);
        countHistory.setUserCountValue(210);
        countHistory.setCountDate(new Date(System.currentTimeMillis()));

        countDetailList = new ArrayList<>();
        for (Denominations temp : denominationsList) {
            CountDetails countDetail = new CountDetails();
            countDetail.setDenominationQuantity(temp.getDefaultQuantity());
            countDetail.setDenominations(temp);
            countDetailList.add(countDetail);
        }
    }

    private void saveSampleData() {
        try {
            long id = countHistoryDao.insertAll(countHistory);
            saveCountDetail(id);
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    private void saveCountDetail(long id) {
        for (CountDetails temp : countDetailList) {
            temp.setCountId((int) id);
        }
        try {
            countDetailsDao.insertAll(countDetailList);
            updateSampleData(id);
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    private void verifyException() {
        assertTrue(saved);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }


    private void initilaizeDenomination() {
        denominationsList = new ArrayList<>();

        Denominations denomination100 = new Denominations();
        denomination100.setDenominationId(1);
        denomination100.setActive(true);
        denomination100.setDefault(false);
        denomination100.setDenominationName("hundred dollar");
        denomination100.setDenominationValue(100);
        denomination100.setDefaultQuantity(5);
        denominationsList.add(denomination100);

        Denominations denomination50 = new Denominations();
        denomination50.setDenominationId(2);
        denomination50.setActive(true);
        denomination50.setDefault(false);
        denomination50.setDenominationName("fifty dollar");
        denomination50.setDenominationValue(50);
        denomination50.setDefaultQuantity(5);
        denominationsList.add(denomination50);

        Denominations denomination20 = new Denominations();
        denomination20.setDenominationId(3);
        denomination20.setActive(true);
        denomination20.setDefault(false);
        denomination20.setDenominationName("twenty dollar");
        denomination20.setDenominationValue(20);
        denomination20.setDefaultQuantity(10);
        denominationsList.add(denomination20);

        Denominations denomination10 = new Denominations();
        denomination10.setDenominationId(4);
        denomination10.setActive(true);
        denomination10.setDefault(false);
        denomination10.setDenominationName("ten dollar");
        denomination10.setDenominationValue(10);
        denomination10.setDefaultQuantity(10);
        denominationsList.add(denomination10);

        Denominations denomination5 = new Denominations();
        denomination5.setDenominationId(5);
        denomination5.setActive(true);
        denomination5.setDefault(false);
        denomination5.setDenominationName("five dollar");
        denomination5.setDenominationValue(5);
        denomination5.setDefaultQuantity(10);
        denominationsList.add(denomination5);

        Denominations denomination1 = new Denominations();
        denomination1.setDenominationId(6);
        denomination1.setActive(true);
        denomination1.setDefault(false);
        denomination1.setDenominationName("one dollar");
        denomination1.setDenominationValue(1);
        denomination1.setDefaultQuantity(20);
        denominationsList.add(denomination1);

        Denominations denominationCent50 = new Denominations();
        denominationCent50.setDenominationId(7);
        denominationCent50.setActive(true);
        denominationCent50.setDefault(false);
        denominationCent50.setDenominationName("fifty cent");
        denominationCent50.setDenominationValue(500);
        denominationCent50.setDefaultQuantity(20);
        denominationsList.add(denominationCent50);

        Denominations denominationCent25 = new Denominations();
        denominationCent25.setDenominationId(8);
        denominationCent25.setActive(true);
        denominationCent25.setDefault(false);
        denominationCent25.setDenominationName("twenty five cent");
        denominationCent25.setDenominationValue(25);
        denominationCent25.setDefaultQuantity(25);
        denominationsList.add(denominationCent25);

        Denominations denominationCent5 = new Denominations();
        denominationCent5.setDenominationId(9);
        denominationCent5.setActive(true);
        denominationCent5.setDefault(false);
        denominationCent5.setDenominationName("five cent");
        denominationCent5.setDenominationValue(5);
        denominationCent5.setDefaultQuantity(25);
        denominationsList.add(denominationCent5);
    }

}
