package com.cashmanager.nwl;

import android.test.InstrumentationTestCase;

import com.cashmanager.dao.FloatHistoryDao;
import com.cashmanager.dao.stub.FloatHistoryDaoStub;
import com.cashmanager.pojo.FloatHistory;

import java.sql.Date;


/**
 * Created by Dinesh on 06/28/17.
 */

public class FloatHistoryTest extends InstrumentationTestCase {

    private FloatHistory floatHistory;
    private FloatHistoryDao floatHistoryDao;
    private boolean saved;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        floatHistoryDao = new FloatHistoryDaoStub();
    }

    public void testFloatHistoryData() {
        initializeSampleData();
        saveSampleData();
        verifyException();

    }


    private void initializeSampleData() {
        floatHistory = new FloatHistory();
        floatHistory.setRegisterId("aa");
        floatHistory.setFloatValue(200);
        floatHistory.setFloatDate(new Date(System.currentTimeMillis()));
    }

    private void saveSampleData() {
        try {
            floatHistoryDao.insertAll(floatHistory);
            saved = true;
        } catch (Exception e) {
            saved = false;
            e.printStackTrace();
        }
    }

    private void verifyException() {
        assertTrue(saved);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
