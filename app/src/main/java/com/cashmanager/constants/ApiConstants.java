package com.cashmanager.constants;

/**
 * Created by jive on 5/15/2017.
 */

public interface ApiConstants {

    int 				REQUEST_CLAIM		 				= 10008;

    String				NIGHT_ADVISOR_PREFS					= "look_at_me";

    String 				EXTRA_STYLISH 						= "extra_stylish";
    String 				EXTRA_POSITION 						= "extra_position";

    int 	REQUEST_GET_DEVICES 		                    = 1;
    int 	REQUEST_GET_CASH_EVENTS 	                    = 2;
    int 	REQUEST_REGISTER_DEVICE         	            = 3;
    int 	REQUEST_GET_ALL_DENOMINATIONS         	        = 4;
    int 	REQUEST_GET_FLOAT_DETAIL              	        = 5;
    int 	REQUEST_POST_FLOAT_DETAIL              	        = 6;
    int 	REQUEST_GET_COUNT_DETAIL              	        = 7;
    int 	REQUEST_POST_COUNT_DETAIL              	        = 8;
    int 	REQUEST_ROLES                       	        = 9;
    int 	REQUEST_ACTIONS                      	        = 10;
    int 	REQUEST_ROLE_ACTONS              	            = 11;
    int 	REQUEST_GET_CLOVER_ROLE_NAME              	    = 12;

    String  BASE_URL                                        = "http://ec2-54-255-173-20.ap-southeast-1.compute.amazonaws.com:8080";
    //String  BASE_URL                                        = "localhost:8080";
    String 	URL_DEVICES	 			                        = "/v3/merchants/%s/devices?access_token=%s";
    String 	URL_GET_ROLE_NAME	 			                = "/v3/merchants/%s/roles/%s?access_token=%s";
    String 	URL_CASH_EVENTS	 		                        = "/v3/merchants/%s/devices/%s/cash_events?access_token=%s&filter=timestamp>%s";
    String 	URL_REGISTER_DEVICE	 		                    = BASE_URL + "/api/register/create";
    String 	URL_GET_ALL_DENOMINATION	 		            = BASE_URL + "/api/allDenomination";
    String 	URL_GET_FLOAT_DETAIL    	 		            = BASE_URL + "/api/syncFloatHistory?deviceId=%s";
    String 	URL_POST_FLOAT_DETAIL   	 		            = BASE_URL + "/api/syncFloatHistory";
    String 	URL_GET_COUNT_DETAIL    	 		            = BASE_URL + "/api/syncCountHistory?deviceId=%s";
    String 	URL_POST_COUNT_DETAIL   	 		            = BASE_URL + "/api/syncCountHistory";
    String 	URL_ROLES   	            		            = BASE_URL + "/api/roles";
    String 	URL_ACTIONS   	 		                        = BASE_URL + "/api/actions";
    String 	URL_ROLES_ACTIONS   	 		                = BASE_URL + "/api/roleAction";
}
