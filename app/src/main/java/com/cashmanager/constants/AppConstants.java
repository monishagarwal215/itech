package com.cashmanager.constants;

/**
 * Created by jive on 5/15/2017.
 */

public interface AppConstants {

    String COUNT_TYPE_CHECK_BALANCE = "CHECK_BALANCE";
    String COUNT_TYPE_MAKE_DROP = "MAKE_DROP";
    String NIGHT_ADVISOR_PREFS = "look_at_me";

    int REQUEST_ACCOUNT = 1001;

    String CURRENCY_FLOAT_MESSAGE_1 = "Let's get started!\n\nUsing the keypad to the right, enter the number of %s %s %s you are placing in your cash drawer.\n" +
            "\n" +
            "Tap the green check box at the bottom right to continue.";

    String CURRENCY_FLOAT_MESSAGE_MIDDLE = "Using the keypad, enter the number of %s %s %s you are placing in your cash drawer.\n" +
            "\n" +
            "Tap the green check box at the bottom right to continue.";

    String CURRENCY_FLOAT_MESSAGE_FINAL = "You did it!\n" +
            "\n" +
            "You have counted out the correct amount for float  in the denominations recommended.\n" +
            "\n" +
            "Tap the set Float button at the bottom right to set your float.";

    String CURRENCY_FLOAT_MESSAGE_DIFFERENCE = "Do you want to set float with this amount?\n" +
            "\n" +
            "Your float amount does not match the recommended float for the cash drawer. If you would like to set float at this amount, tap Set Float at the bottom right.";

    String CURRENCY_CHECKBALANCE_MESSAGE_1 = "Let's get started!\n" +
            "\n" +
            "Count all of the %s in the cash drawer and enter the number using the keypad to the right.\n" +
            "\n" +
            "Tap the green check box at the bottom right to continue.";
    String CURRENCY_CHECKBALANCE_MESSAGE_MIDDLE = "Count all of the %s in the cash drawer and enter the number using the keypad to the right.\n" +
            "\n" +
            "Tap the green check box at the bottom right to continue.";
    String CURRENCY_CHECKBALANCE_MATCH = "Your cash count matches the amount expected for total cash.\n" +
            "\n" +
            "Tap 'Save' at the bottom right to record this balance check and exit.";
    String CURRENCY_CHECKBALANCE_UNMATCH = "There is a difference between your cash count and the total cash expected based on sales activity.\n" +
            "\n" +
            "Tap 'Cancel' below to check your counts again.\n" +
            "\n" +
            "Tap 'Save' to end the balance check and add a note explaining the difference.";

    String CURRENCY_MAKEDROP_MESSAGE_1 = "Count out the drop amount beginning with the largest bills you have in the till." +
            "\n\nIf you have %s, count these and enter the number using the keypad to the right. Set the bills aside.\n" +
            "\n" +
            "Tap the green check box at the bottom right of the keypad to save your entry and continue.";
    String CURRENCY_MAKEDROP_MESSAGE_MIDDLE = "Count out as many %s as you need to reach your drop amount and enter the number using the keypad to the right. Set the bills aside.\n" +
            "\n" +
            "Tap the green check box at the bottom right of the keypad to save your entry and continue.";
    String CURRENCY_MAKEDROP_MESSAGE_FINAL = "You have reached your drop amount. Tap 'Save' at the bottom right to record this drop and print the receipt.";
    String CURRENCY_MAKEDROP_MESSAGE_DIFFERENCE = "Your cash total is over your drop amount by %s. Make the necessary adjustment to your count and return the cash to the till. Tap 'Save' when you have made the adjustment.\n" +
            "\n" +
            "Tap 'Cancel' to start the count from the beginning.";

    String CURRENCY_CLOSEOUT_CB_1 = "Start the closeout with a balance check:\n" +
            "\n" +
            "Count all %s in the cash drawer and enter the number using the keypad to the right.\n" +
            "\n" +
            "Tap the green check box at the bottom right to continue.";

    String CURRENCY_CLOSEOUT_CB_2 = "Check Balance:\n" +
            "\n" +
            "Count all %s in the cash drawer and enter the number using the keypad to the right.\n" +
            "\n" +
            "Tap the green check box at the bottom right to continue.";

    String CURRENCY_CLOSEOUT_CB_MATCH = "Check Balance:\n" +
            "\n" +
            "Your cash count matches the amount expected for total cash.\n" +
            "\n" +
            "Tap 'Save' at the bottom right to record this balance check and exit.";

    String CURRENCY_CLOSEOUT_CB_UNMATCH = "Check Balance:\n" +
            "\n" +
            "There is a difference between your cash count and the total cash expected based on sales activity.\n" +
            "\n" +
            "Tap 'Cancel' below to check your counts again.\n" +
            "\n" +
            "Tap 'Save' to end the balance check and add a note explaining the difference.";

    String CURRENCY_CLOSEOUT_MD_1 = "Prepare the drop:\n" +
            "\n" +
            "Count out %s %s and enter the number using the keypad to the right. Set the bills aside.\n" +
            "\n" +
            "Tap the green check box at the bottom right to continue.";

    String CURRENCY_CLOSEOUT_MD_PRINTRECEIPT = "Prepare the drop:\n" +
            "\n" +
            "Tap 'Save' at the bottom right to print the drop receipt and continue.\n"+
            "\n" +
            "You are now ready to set float.";

    String CURRENCY_CLOSEOUT_SETFLOAT = "Set the float:\n" +
            "\n" +
            "Using the keypad to the right, count the number of %s %s you are placing in your cash drawer." +
            // " Count out and enter the number of hundreds you are placing in your cash drawer.\n" +
            "\n\n" +
            "Tap the green check box at the bottom right to continue to the next denomination.";


    String DB_NAME = "CashManager";

    String ACTION_SET_FLOAT = "SET_FLOAT";
    String ACTION_CHECK_BALANCE = "CHECK_BALANCE";
    String ACTION_MAKE_DROP = "MAKE_DROP";
    String ACTION_CLOSE_OUT = "CLOSE_OUT";


}
