package com.cashmanager.constants;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.cashmanager.dao.ActionDao;
import com.cashmanager.dao.CountDetailsDao;
import com.cashmanager.dao.CountHistoryDao;
import com.cashmanager.dao.DenominationDao;
import com.cashmanager.dao.DeviceDao;
import com.cashmanager.dao.FloatDetailDao;
import com.cashmanager.dao.FloatHistoryDao;
import com.cashmanager.dao.RolesActionsDao;
import com.cashmanager.dao.RolesDao;
import com.cashmanager.pojo.Actions;
import com.cashmanager.pojo.CountDetails;
import com.cashmanager.pojo.CountHistory;
import com.cashmanager.pojo.Denominations;
import com.cashmanager.pojo.Device;
import com.cashmanager.pojo.FloatDetails;
import com.cashmanager.pojo.FloatHistory;
import com.cashmanager.pojo.RoleActions;
import com.cashmanager.pojo.Roles;

/**
 * Created by Dinesh Adhikari on 06/06/17.
 */

@Database(entities = {Device.class, Denominations.class,
        FloatDetails.class,
        FloatHistory.class,
        CountDetails.class,
        CountHistory.class,
        Roles.class,
        Actions.class,
        RoleActions.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DeviceDao deviceDao();

    public abstract DenominationDao denominationDao();

    public abstract FloatDetailDao floatDetailDao();

    public abstract FloatHistoryDao floatHistoryDao();

    public abstract CountDetailsDao countDetailDao();

    public abstract CountHistoryDao countHistoryDao();

    public abstract RolesDao rolesDao();

    public abstract ActionDao actionDao();

    public abstract RolesActionsDao rolesActionsDao();

}
