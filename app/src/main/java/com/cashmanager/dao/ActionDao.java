package com.cashmanager.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cashmanager.pojo.Actions;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Dinesh Adhikari on 06/06/17.
 */

@Dao
public interface ActionDao {
    @Query("Select * from actions")
    List<Actions> getAllActions();

    @Insert(onConflict = IGNORE)
    void insertAll(Actions... actions);


    @Insert(onConflict = REPLACE)
    void insertAll(List<Actions> actions);


    @Delete
    void delete(Actions actions);

    @Update
    void updateActions(Actions actions);
}
