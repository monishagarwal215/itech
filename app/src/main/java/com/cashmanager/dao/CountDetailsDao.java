package com.cashmanager.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cashmanager.pojo.CountDetails;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Dinesh Adhikari on 6/10/2017.
 */

@Dao
public interface CountDetailsDao {

    @Query("Select * from count_details")
    LiveData<List<CountDetails>> getAllCountDetails();

    @Query("Select * from count_details WHERE countId = :id")
    List<CountDetails> getAllCountDetailByCountId(int id) throws Exception;

    @Insert(onConflict = IGNORE)
    void insertAll(CountDetails... countDetail) throws Exception;

    @Insert(onConflict = IGNORE)
    void insertAll(List<CountDetails> countDetailsList) throws Exception;

    @Update(onConflict = REPLACE)
    void updateAll(List<CountDetails> countDetailsList) throws Exception;

    @Delete
    void delete(CountDetails CountDetails) throws Exception;
}
