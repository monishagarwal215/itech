package com.cashmanager.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import com.cashmanager.pojo.CountHistory;
import com.cashmanager.util.DateConverter;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Dinesh Adhikari on 06/06/17.
 */

@Dao
@TypeConverters(DateConverter.class)
public interface CountHistoryDao {

    @Query("Select * from count_history")
    LiveData<List<CountHistory>> getAllCountHistory() throws Exception;

    @Query("Select * from count_history ORDER BY countId DESC LIMIT 1")
    CountHistory getLatestCountHistory() throws Exception;


    @Insert(onConflict = IGNORE)
    Long insertAll(CountHistory countHistory) throws Exception;


    @Query("Select * from count_history WHERE synced = 0")
    List<CountHistory> findAllUnSyncedCountHistory() throws Exception;


    @Query("Select * from count_history WHERE serverId = :id")
    CountHistory findByServerId(String id) throws Exception;


    @Insert(onConflict = IGNORE)
    void insertAll(CountHistory... countHistory) throws Exception;


    @Insert(onConflict = IGNORE)
    void insertAll(List<CountHistory> countHistoryList) throws Exception;


    @Delete
    void delete(CountHistory countHistory) throws Exception;


    @Update(onConflict = REPLACE)
    void update(CountHistory countHistory) throws Exception;


}
