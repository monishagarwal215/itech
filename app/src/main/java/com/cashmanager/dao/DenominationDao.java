package com.cashmanager.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cashmanager.pojo.Denominations;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Dinesh Adhikari on 06/06/17.
 */

@Dao
public interface DenominationDao {

    @Query("Select * from denominations ORDER BY sortOrder ASC")
    LiveData<List<Denominations>> getAllDenominations();

    @Query("Select * from denominations ORDER BY sortOrder ASC")
    List<Denominations> getAllDenominationFromDatabase();


    @Insert(onConflict = IGNORE)
    void insertAll(Denominations... denominations) throws Exception;

    @Insert(onConflict = REPLACE)
    void insertAll(List<Denominations> denominations) throws Exception;

    @Delete
    void delete(Denominations denominations) throws Exception;

    @Update
    void updateDevice(Denominations denominations) throws Exception;
}
