package com.cashmanager.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cashmanager.pojo.Device;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;

/**
 * Created by Dinesh Adhikari on 06/06/17.
 */

@Dao
public interface DeviceDao {
    @Query("Select * from device")
    LiveData<List<Device>> getAllDevices();

    @Insert(onConflict = IGNORE)
    void insertAll(Device... devices);

    @Delete
    void delete(Device device);

    @Update
    void updateDevice(Device device);
}
