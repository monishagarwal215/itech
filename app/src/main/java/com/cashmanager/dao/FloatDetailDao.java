package com.cashmanager.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cashmanager.pojo.FloatDetails;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Dinesh Adhikari on 06/06/17.
 */

@Dao
public interface FloatDetailDao {

    @Query("Select * from float_details")
    LiveData<List<FloatDetails>> getAllFloatDetail();

    @Query("Select * from float_details WHERE floatId = :id")
    List<FloatDetails> getAllFloatDetailByFloatId(int id) throws Exception;

    @Insert(onConflict = IGNORE)
    void insertAll(FloatDetails... floatDetail) throws Exception;

    @Insert(onConflict = IGNORE)
    void insertAll(List<FloatDetails> floatDetailsList) throws Exception;

    @Update(onConflict = REPLACE)
    void updateAll(List<FloatDetails> floatDetailsList) throws Exception;

    @Delete
    void delete(FloatDetails floatDetails) throws Exception;

}
