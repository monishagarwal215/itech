package com.cashmanager.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import com.cashmanager.pojo.FloatHistory;
import com.cashmanager.util.DateConverter;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Dinesh Adhikari on 06/06/17.
 */

@Dao
@TypeConverters(DateConverter.class)
public interface FloatHistoryDao {

    @Query("Select * from float_history")
    LiveData<List<FloatHistory>> getAllFloatHistory() throws Exception;

    @Query("Select * from float_history ORDER BY floatId DESC LIMIT 1")
    FloatHistory getLatestFloatHistory();

    @Insert(onConflict = IGNORE)
    Long insertAll(FloatHistory floatHistory) throws Exception;

    @Query("Select * from float_history WHERE synced = 0")
    List<FloatHistory> findAllUnSyncedFloatHistory() throws Exception;

    @Query("Select * from float_history WHERE serverId = :id")
    FloatHistory findByServerId(String id) throws Exception;

    @Delete
    void delete(FloatHistory floatHistory) throws Exception;

    @Update(onConflict = REPLACE)
    void update(FloatHistory floatHistory) throws Exception;
}
