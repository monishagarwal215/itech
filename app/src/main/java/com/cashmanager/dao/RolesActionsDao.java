package com.cashmanager.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cashmanager.pojo.RoleActions;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Dinesh Adhikari on 06/06/17.
 */

@Dao
public interface RolesActionsDao {
    @Query("Select * from role_actions")
    List<RoleActions> getAllRoleActions();

    @Query("Select * from role_actions where roleId = :id")
    List<RoleActions> getByRoleId(int id);


    @Insert(onConflict = IGNORE)
    void insertAll(RoleActions... roleActions);


    @Insert(onConflict = REPLACE)
    void insertAll(List<RoleActions> roleActions);


    @Delete
    void delete(RoleActions roleAction);

    @Update
    void updateActions(RoleActions roleAction);
}
