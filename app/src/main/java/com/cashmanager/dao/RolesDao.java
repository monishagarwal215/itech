package com.cashmanager.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cashmanager.pojo.Roles;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Dinesh Adhikari on 06/06/17.
 */

@Dao
public interface RolesDao {
    @Query("Select * from roles")
    List<Roles> getAllRoles();

    @Query("Select * from roles where roleName = :roleName")
    List<Roles> getRoleByName(String roleName);

    @Insert(onConflict = IGNORE)
    void insertAll(Roles... roles);


    @Insert(onConflict = REPLACE)
    void insertAll(List<Roles> roles);

    @Delete
    void delete(Roles role);

    @Update
    void updateRoles(Roles role);
}
