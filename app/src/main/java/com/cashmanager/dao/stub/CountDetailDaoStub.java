package com.cashmanager.dao.stub;

import android.arch.lifecycle.LiveData;

import com.cashmanager.dao.CountDetailsDao;
import com.cashmanager.pojo.CountDetails;
import com.cashmanager.pojo.Denominations;
import com.krapps.utils.StringUtils;

import java.util.List;

/**
 * Created by dinesh on 07/07/17.
 */

public class CountDetailDaoStub implements CountDetailsDao {
    @Override
    public LiveData<List<CountDetails>> getAllCountDetails() {
        return null;
    }

    @Override
    public List<CountDetails> getAllCountDetailByCountId(int id) throws Exception {
        if (id == 0) {
            throw new Exception("Id cannot be zero");
        }
        return null;
    }

    @Override
    public void insertAll(CountDetails... countDetail) throws Exception {
        if (countDetail == null) {
            throw new Exception("Count Detail cannot be null");
        }
        for (int i = 0; i <= countDetail.length - 1; i++) {
            if (countDetail[i].getDenominations() == null) {
                throw new Exception("Denomination cannot be null");
            }
            validateDenomination(countDetail[i].getDenominations());
            if (countDetail[i].getCountId() == 0) {
                throw new Exception("Count id cannot be zero");
            }
        }
    }

    private void validateDenomination(Denominations denominations) throws Exception {
        if (denominations.getDenominationId() == 0) {
            throw new Exception("Denomination Id cannot be zero");
        }
        if (denominations.getDenominationValue() == 0) {
            throw new Exception("Denomination value cannot be zero");
        }
        if (StringUtils.isNullOrEmpty(denominations.getDenominationName())) {
            throw new Exception("Denomination name be empty or null");
        }
    }

    @Override
    public void insertAll(List<CountDetails> countDetailsList) throws Exception {
        if (countDetailsList == null) {
            throw new Exception("Count Detail List cannot be null");
        }
        for (int i = 0; i <= countDetailsList.size() - 1; i++) {
            if (countDetailsList.get(i).getDenominations() == null) {
                throw new Exception("Denomination cannot be null");
            }
            if (countDetailsList.get(i).getCountId() == 0) {
                throw new Exception("Count id cannot be zero");
            }
            validateDenomination(countDetailsList.get(i).getDenominations());
        }
    }

    @Override
    public void updateAll(List<CountDetails> countDetailsList) throws Exception {
        if (countDetailsList == null) {
            throw new Exception("Count Detail List cannot be null");
        }
        for (int i = 0; i <= countDetailsList.size() - 1; i++) {
            if (countDetailsList.get(i).getDenominations() == null) {
                throw new Exception("Denomination cannot be null");
            }
            if (countDetailsList.get(i).getCountDetailsId() == 0) {
                throw new Exception("Count Detail id cannot be zero");
            }
            if (countDetailsList.get(i).getCountId() == 0) {
                throw new Exception("Count id cannot be zero");
            }
            if (countDetailsList.get(i).isSynced() == false) {
                throw new Exception("While updating synced should always be true");
            }
            if (StringUtils.isNullOrEmpty(countDetailsList.get(i).getServerId())) {
                throw new Exception("While updating server id cannot be null");
            }
            validateDenomination(countDetailsList.get(i).getDenominations());
        }
    }

    @Override
    public void delete(CountDetails countDetails) throws Exception {
        if (countDetails == null) {
            throw new Exception("Count Detail cannot be null");
        }
        if (countDetails.getDenominations() == null) {
            throw new Exception("Denomination cannot be null");
        }
        if (countDetails.getCountDetailsId() == 0) {
            throw new Exception("Count Detail id cannot be zero");
        }
        if (countDetails.getCountId() == 0) {
            throw new Exception("Count id cannot be zero");
        }
        validateDenomination(countDetails.getDenominations());
    }
}
