package com.cashmanager.dao.stub;

import android.arch.lifecycle.LiveData;

import com.cashmanager.dao.CountHistoryDao;
import com.cashmanager.pojo.CountHistory;
import com.krapps.utils.StringUtils;

import java.util.List;

/**
 * Created by Dinesh on 05/07/17.
 */

public class CountHistoryDaoStub implements CountHistoryDao {
    @Override
    public LiveData<List<CountHistory>> getAllCountHistory() throws Exception {
        return null;
    }

    @Override
    public CountHistory getLatestCountHistory() throws Exception {
        return null;
    }

    @Override
    public Long insertAll(CountHistory countHistory) throws Exception {
        if (countHistory == null) {
            throw new Exception("Count History object is null");
        }
        if (StringUtils.isNullOrEmpty(countHistory.getRegisterId())) {
            throw new Exception("Register Id cannot be null");
        }

        if (StringUtils.isNullOrEmpty(countHistory.getCountBy())) {
            throw new Exception("Count by cannot be empty or null");
        }

        if (StringUtils.isNullOrEmpty(countHistory.getNotes())) {
            throw new Exception("Note cannot be empty or null");
        }

        if (countHistory.getUserCountValue() == 0) {
            throw new Exception("User count value cannot be zero");
        }

        if (countHistory.getSystemCountValue() == 0) {
            throw new Exception("System count value cannot be zero");
        }

        if (countHistory.getCountDate() == null) {
            throw new Exception("Count Date cannot be null");
        }

        return 1l;
    }

    @Override
    public List<CountHistory> findAllUnSyncedCountHistory() throws Exception {
        return null;
    }

    @Override
    public CountHistory findByServerId(String id) throws Exception {
        if (StringUtils.isNullOrEmpty(id)) {
            throw new Exception("Server Id cannot be null");
        }
        return null;
    }

    @Override
    public void insertAll(CountHistory... countHistory) throws Exception {
        if (countHistory != null) {
            throw new Exception("Count History list cannot be null");
        }

    }

    @Override
    public void insertAll(List<CountHistory> countHistoryList) throws Exception {
        if (countHistoryList != null) {
            throw new Exception("Count History list cannot be null");
        }

    }

    @Override
    public void delete(CountHistory countHistory) throws Exception {
        if (countHistory == null) {
            throw new Exception("Count History object is null");
        }
        if (StringUtils.isNullOrEmpty(countHistory.getRegisterId())) {
            throw new Exception("Register Id cannot be null");
        }

        if (StringUtils.isNullOrEmpty(countHistory.getCountBy())) {
            throw new Exception("Count by cannot be empty or null");
        }

        if (StringUtils.isNullOrEmpty(countHistory.getNotes())) {
            throw new Exception("Note cannot be empty or null");
        }

        if (countHistory.getUserCountValue() == 0) {
            throw new Exception("User count value cannot be zero");
        }

        if (countHistory.getSystemCountValue() == 0) {
            throw new Exception("System count value cannot be zero");
        }

        if (countHistory.getCountDate() != null) {
            throw new Exception("Count Date cannot be null");
        }

        return;

    }

    @Override
    public void update(CountHistory countHistory) throws Exception {
        if (countHistory == null) {
            throw new Exception("Count History object is null");
        }
        if (StringUtils.isNullOrEmpty(countHistory.getRegisterId())) {
            throw new Exception("Register Id cannot be null");
        }

        if (StringUtils.isNullOrEmpty(countHistory.getCountBy())) {
            throw new Exception("Count by cannot be empty or null");
        }

        if (StringUtils.isNullOrEmpty(countHistory.getNotes())) {
            throw new Exception("Note cannot be empty or null");
        }

        if (countHistory.getUserCountValue() == 0) {
            throw new Exception("User count value cannot be zero");
        }

        if (countHistory.getSystemCountValue() == 0) {
            throw new Exception("System count value cannot be zero");
        }

        if (countHistory.getCountDate() != null) {
            throw new Exception("Count Date cannot be null");
        }

        return;

    }
}
