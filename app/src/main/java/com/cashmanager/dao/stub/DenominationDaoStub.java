package com.cashmanager.dao.stub;

import android.arch.lifecycle.LiveData;

import com.cashmanager.dao.DenominationDao;
import com.cashmanager.pojo.Denominations;
import com.krapps.utils.StringUtils;

import java.util.List;

/**
 * Created by dinesh on 07/07/17.
 */

public class DenominationDaoStub implements DenominationDao {
    @Override
    public LiveData<List<Denominations>> getAllDenominations() {
        return null;
    }

    @Override
    public List<Denominations> getAllDenominationFromDatabase() {
        return null;
    }

    @Override
    public void insertAll(Denominations... denominations) throws Exception {
        if (denominations == null) {
            throw new Exception("Denomination cannot be null");
        }
        for (int i = 0; i <= denominations.length - 1; i++) {
            validateDenomination(denominations[i]);
        }

    }

    @Override
    public void insertAll(List<Denominations> denominations) throws Exception {
        if (denominations == null) {
            throw new Exception("Denomination List cannot be null");
        }
        for (int i = 0; i <= denominations.size() - 1; i++) {
            validateDenomination(denominations.get(i));
        }
    }

    @Override
    public void delete(Denominations denominations) throws Exception {
        if (denominations == null) {
            throw new Exception("Denomination cannot be null");
        }
        validateDenomination(denominations);
    }

    @Override
    public void updateDevice(Denominations denominations) throws Exception {

    }

    private void validateDenomination(Denominations denominations) throws Exception {
        if (denominations.getDenominationId() == 0) {
            throw new Exception("Denomination Id cannot be zero");
        }
        if (denominations.getDenominationValue() == 0) {
            throw new Exception("Denomination value cannot be zero");
        }
        if (StringUtils.isNullOrEmpty(denominations.getDenominationName())) {
            throw new Exception("Denomination name be empty or null");
        }
    }
}
