package com.cashmanager.dao.stub;

import android.arch.lifecycle.LiveData;

import com.cashmanager.dao.FloatDetailDao;
import com.cashmanager.pojo.FloatDetails;
import com.krapps.utils.StringUtils;

import java.util.List;

/**
 * Created by Dinesh on 07/07/17.
 */

public class FloatDetailDaoStub implements FloatDetailDao {
    @Override
    public LiveData<List<FloatDetails>> getAllFloatDetail() {
        return null;
    }

    @Override
    public List<FloatDetails> getAllFloatDetailByFloatId(int id) throws Exception {
        if (id == 0) {
            throw new Exception("Id cannot be zero");
        }
        return null;
    }

    @Override
    public void insertAll(FloatDetails... floatDetail) throws Exception {
        if (floatDetail == null) {
            throw new Exception("Float detail cannot be null object");
        }
        for (int i = 0; i <= floatDetail.length - 1; i++) {
            if (floatDetail[i] == null) {
                throw new Exception("Float detail cannot be null object");
            }
            if (floatDetail[i].getDenominations() == null) {
                throw new Exception("Denomination array cannot be null object");
            }
            if (floatDetail[i].getFloatDetailsId() == 0) {
                throw new Exception("Float Detail Id cannot be zero.");
            }
            if (floatDetail[i].getFloatId() == 0) {
                throw new Exception("Float Id cannot be zero.");
            }
        }
    }

    @Override
    public void insertAll(List<FloatDetails> floatDetailsList) throws Exception {
        if (floatDetailsList == null) {
            throw new Exception("Float detail cannot be null object");
        }
        for (int i = 0; i <= floatDetailsList.size() - 1; i++) {
            if (floatDetailsList.get(i) == null) {
                throw new Exception("Float detail cannot be null object");
            }
            if (floatDetailsList.get(i).getDenominations() == null) {
                throw new Exception("Denomination array cannot be null object");
            }
            if (floatDetailsList.get(i).getFloatDetailsId() == 0) {
                throw new Exception("Float Detail Id cannot be zero.");
            }
            if (floatDetailsList.get(i).getFloatId() == 0) {
                throw new Exception("Float Id cannot be zero.");
            }
            if (StringUtils.isNullOrEmpty(floatDetailsList.get(i).getServerId())) {
                throw new Exception("Server Id cannot be null of empty");
            }
        }
    }

    @Override
    public void updateAll(List<FloatDetails> floatDetailsList) throws Exception {
        if (floatDetailsList == null) {
            throw new Exception("Float detail cannot be null object");
        }
        for (int i = 0; i <= floatDetailsList.size() - 1; i++) {
            if (floatDetailsList.get(i) == null) {
                throw new Exception("Float detail cannot be null object");
            }
            if (floatDetailsList.get(i).getDenominations() == null) {
                throw new Exception("Denomination array cannot be null object");
            }
            if (floatDetailsList.get(i).getFloatDetailsId() == 0) {
                throw new Exception("Float Detail Id cannot be zero.");
            }
            if (floatDetailsList.get(i).getFloatId() == 0) {
                throw new Exception("Float Id cannot be zero.");
            }
            if (StringUtils.isNullOrEmpty(floatDetailsList.get(i).getServerId())) {
                throw new Exception("Server Id cannot be null of empty");
            }
        }
    }

    @Override
    public void delete(FloatDetails floatDetails) throws Exception {
        if (floatDetails == null) {
            throw new Exception("Float detail cannot be null object");
        }
        if (floatDetails.getDenominations() == null) {
            throw new Exception("Denomination array cannot be null object");
        }
        if (floatDetails.getFloatDetailsId() == 0) {
            throw new Exception("Float Detail Id cannot be zero.");
        }
        if (floatDetails.getFloatId() == 0) {
            throw new Exception("Float Id cannot be zero.");
        }
    }
}
