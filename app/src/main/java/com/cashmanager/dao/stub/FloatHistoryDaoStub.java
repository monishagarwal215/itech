package com.cashmanager.dao.stub;

import android.arch.lifecycle.LiveData;

import com.cashmanager.dao.FloatHistoryDao;
import com.cashmanager.pojo.FloatHistory;
import com.krapps.utils.StringUtils;

import java.util.List;

/**
 * Created by Dinesh on 28/06/17.
 */

public class FloatHistoryDaoStub implements FloatHistoryDao {

    @Override
    public LiveData<List<FloatHistory>> getAllFloatHistory() throws Exception {
        return null;
    }

    @Override
    public FloatHistory getLatestFloatHistory() {
        return null;
    }

    @Override
    public Long insertAll(FloatHistory floatHistory) throws Exception {
        if (floatHistory == null) {
            throw new Exception("Float History object is null");
        }
        if (StringUtils.isNullOrEmpty(floatHistory.getRegisterId())) {
            throw new Exception("Register Id cannot be null");
        }

        if (floatHistory.getFloatValue() == 0) {
            throw new Exception("Float value cannot be zero");
        }
        return 1l;
    }

    @Override
    public List<FloatHistory> findAllUnSyncedFloatHistory() throws Exception {
        return null;
    }

    @Override
    public FloatHistory findByServerId(String id) throws Exception {
        return null;
    }


    @Override
    public void delete(FloatHistory floatHistory) throws Exception {
        if (floatHistory == null) {
            throw new Exception("Float History object is null");
        }
        if (StringUtils.isNullOrEmpty(floatHistory.getRegisterId())) {
            throw new Exception("Register Id cannot be null");
        }

        if (floatHistory.getFloatValue() == 0) {
            throw new Exception("Float value cannot be zero");
        }
        return;
    }

    @Override
    public void update(FloatHistory floatHistory) throws Exception {
        if (floatHistory == null) {
            throw new Exception("Float History object is null");
        }
        if (StringUtils.isNullOrEmpty(floatHistory.getRegisterId())) {
            throw new Exception("Register Id cannot be null");
        }

        if (floatHistory.getFloatValue() == 0) {
            throw new Exception("Float value cannot be zero");
        }
        return;
    }

}
