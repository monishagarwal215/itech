package com.cashmanager.gateway;

import android.content.Context;
import android.os.AsyncTask;

import com.cashmanager.util.SyncDatabase;

/**
 * Created by Dinesh on 6/8/17.
 */

public class SyncCountHistoryTask extends AsyncTask<Void, Void, Void> {

    Context mContex;

    public SyncCountHistoryTask(Context mContex) {
        this.mContex = mContex;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        SyncDatabase.getInstance(mContex).syncCountHistory();
        return null;
    }
}
