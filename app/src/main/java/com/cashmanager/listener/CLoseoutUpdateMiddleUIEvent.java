package com.cashmanager.listener;

/**
 * Created by Dinesh on 7/15/2017.
 */

public class CLoseoutUpdateMiddleUIEvent {

    public int getCloseout_phase() {
        return closeout_phase;
    }

    public void setCloseout_phase(int closeout_phase) {
        this.closeout_phase = closeout_phase;
    }

    int closeout_phase;

    public CLoseoutUpdateMiddleUIEvent(int closeout_phase) {
        this.closeout_phase = closeout_phase;
    }

}
