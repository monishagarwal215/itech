package com.cashmanager.listener;

/**
 * Created by Dinesh Adhikari on 04/06/17.
 */

public class CurrencyEvent {
    int num;

    public CurrencyEvent(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
