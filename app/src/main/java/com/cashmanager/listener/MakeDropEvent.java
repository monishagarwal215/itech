package com.cashmanager.listener;

/**
 * Created by Dinesh Adhikari on 6/21/2017.
 */

public class MakeDropEvent {

    int dropAmount;

    public MakeDropEvent(int dropAmount) {
        this.dropAmount = dropAmount;
    }

    public int getDropAmount() {
        return dropAmount;
    }

    public void setDropAmount(int dropAmount) {
        this.dropAmount = dropAmount;
    }
}
