package com.cashmanager.listener;

/**
 * Created by Dinesh Adhikari on 05/06/17.
 */

public class MessageEvent {
    private String message;
    private boolean hideTip;

    public MessageEvent(String message) {
        this.message = message;
    }

    public boolean isHideTip() {
        return hideTip;
    }

    public void setHideTip(boolean hideTip) {
        this.hideTip = hideTip;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
