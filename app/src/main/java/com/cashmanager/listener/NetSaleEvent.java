package com.cashmanager.listener;

/**
 * Created by Dinesh Adhikari on 6/21/2017.
 */

public class NetSaleEvent {

    int netsale;

    public int getNetsale() {
        return netsale;
    }

    public void setNetsale(int netsale) {
        this.netsale = netsale;
    }

    public NetSaleEvent(int netsale) {
        this.netsale = netsale;
    }


}
