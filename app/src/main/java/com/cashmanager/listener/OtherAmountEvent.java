package com.cashmanager.listener;

/**
 * Created by Dinesh Adhikari on 04/06/17.
 */

public class OtherAmountEvent {
    int num;

    public OtherAmountEvent(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
