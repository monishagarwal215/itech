package com.cashmanager.listener;

/**
 * Created by Dinesh Adhikari on 08/06/17.
 */

public class SaveCheckBalanceEvent {
    private boolean save;
    private String note;

    public SaveCheckBalanceEvent(boolean save, String note) {
        this.save = save;
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }
}
