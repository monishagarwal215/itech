package com.cashmanager.listener;

/**
 * Created by Dinesh on 7/16/2017.
 */

public class SaveCloseOutMDEvent {

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    private boolean save;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    private int flag;

    public SaveCloseOutMDEvent(boolean save, int flag) {
        this.save = save;
        this.flag = flag;
    }
}
