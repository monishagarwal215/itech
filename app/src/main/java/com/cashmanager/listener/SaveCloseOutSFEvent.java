package com.cashmanager.listener;

/**
 * Created by Dinesh on 7/17/2017.
 */

public class SaveCloseOutSFEvent {

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    private boolean save;

    public SaveCloseOutSFEvent(boolean save) {
        this.save = save;
    }
}
