package com.cashmanager.listener;

/**
 * Created by Dinesh Adhikari on 08/06/17.
 */

public class SaveMakeDropEvent {
    private boolean save;

    public SaveMakeDropEvent(boolean save) {
        this.save = save;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }
}
