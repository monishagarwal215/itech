package com.cashmanager.model;

/**
 * Created by jive on 19/05/17.
 */

public class DeviceData {
    private String href;
    private String id;
    private String name;
    private String model;
    private String orderPrefix;
    private String terminalPrefix;
    private String serial;
    private String secureId;
    private String deviceTypeName;
    private boolean pinDisabled;
    private boolean offlinePayments;
    private boolean offlinePaymentsAll;
    private Merchant merchant;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOrderPrefix() {
        return orderPrefix;
    }

    public void setOrderPrefix(String orderPrefix) {
        this.orderPrefix = orderPrefix;
    }

    public String getTerminalPrefix() {
        return terminalPrefix;
    }

    public void setTerminalPrefix(String terminalPrefix) {
        this.terminalPrefix = terminalPrefix;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getSecureId() {
        return secureId;
    }

    public void setSecureId(String secureId) {
        this.secureId = secureId;
    }

    public String getDeviceTypeName() {
        return deviceTypeName;
    }

    public void setDeviceTypeName(String deviceTypeName) {
        this.deviceTypeName = deviceTypeName;
    }

    public boolean isPinDisabled() {
        return pinDisabled;
    }

    public void setPinDisabled(boolean pinDisabled) {
        this.pinDisabled = pinDisabled;
    }

    public boolean isOfflinePayments() {
        return offlinePayments;
    }

    public void setOfflinePayments(boolean offlinePayments) {
        this.offlinePayments = offlinePayments;
    }

    public boolean isOfflinePaymentsAll() {
        return offlinePaymentsAll;
    }

    public void setOfflinePaymentsAll(boolean offlinePaymentsAll) {
        this.offlinePaymentsAll = offlinePaymentsAll;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }
}
