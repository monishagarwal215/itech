package com.cashmanager.model;

/**
 * Created by jive on 26/05/17.
 */

public class Employee {
    private String href;
    private String id;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
