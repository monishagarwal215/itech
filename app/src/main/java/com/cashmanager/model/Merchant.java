package com.cashmanager.model;

/**
 * Created by jive on 19/05/17.
 */

public class Merchant {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
