package com.cashmanager.model;

/**
 * Created by jive on 26/05/17.
 */

public class Orders {
    private String href;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
