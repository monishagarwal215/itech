package com.cashmanager.model.request;

/**
 * Created by Dinesh Adhikari on 19/06/17.
 */

public class RegisterDeviceRequest {
    private String deviceDescription;
    private String deviceId;
    private String deviceName;

    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
