package com.cashmanager.model.response;

import com.cashmanager.pojo.Actions;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

public class ActionResponse extends CommonJsonResponse {
    private List<Actions> actionsList;

    public List<Actions> getActionsList() {
        return actionsList;
    }

    public void setActionsList(List<Actions> actionsList) {
        this.actionsList = actionsList;
    }
}
