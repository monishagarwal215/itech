package com.cashmanager.model.response;

import com.cashmanager.model.CashEventData;
import com.google.gson.annotations.SerializedName;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by jive on 26/05/17.
 */

public class CashEventResponse extends CommonJsonResponse {

    @SerializedName(value = "elements")
    private List<CashEventData> cashEventDataList;

    public List<CashEventData> getCashEventDataList() {
        return cashEventDataList;
    }

    public void setCashEventDataList(List<CashEventData> cashEventDataList) {
        this.cashEventDataList = cashEventDataList;
    }
}
