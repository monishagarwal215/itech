package com.cashmanager.model.response;

import com.cashmanager.pojo.CountHistory;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by Dinesh on 02/07/17.
 */

public class CountHistoryResponse extends CommonJsonResponse {
    private List<CountHistory> countHistoryList;

    public List<CountHistory> getCountHistoryList() {
        return countHistoryList;
    }

    public void setCountHistoryList(List<CountHistory> countHistoryList) {
        this.countHistoryList = countHistoryList;
    }
}
