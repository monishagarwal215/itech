package com.cashmanager.model.response;

import com.cashmanager.pojo.Denominations;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by Dinesh on 29/06/17.
 */

public class DenominationResponse extends CommonJsonResponse {
    private List<Denominations> denominations;

    public List<Denominations> getDenominations() {
        return denominations;
    }

    public void setDenominations(List<Denominations> denominations) {
        this.denominations = denominations;
    }
}
