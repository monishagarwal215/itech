package com.cashmanager.model.response;

import com.cashmanager.model.DeviceData;
import com.google.gson.annotations.SerializedName;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by jive on 19/05/17.
 */

public class DeviceListResponse extends CommonJsonResponse {

    @SerializedName(value = "elements")
    private List<DeviceData> deviceDataList;

    public List<DeviceData> getDeviceDataList() {
        return deviceDataList;
    }

    public void setDeviceDataList(List<DeviceData> deviceDataList) {
        this.deviceDataList = deviceDataList;
    }
}
