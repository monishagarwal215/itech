package com.cashmanager.model.response;

import com.cashmanager.pojo.FloatHistory;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by Dinesh on 02/07/17.
 */

public class FloatHistoryResponse extends CommonJsonResponse {
    private List<FloatHistory> floatHistoryList;

    public List<FloatHistory> getFloatHistoryList() {
        return floatHistoryList;
    }

    public void setFloatHistoryList(List<FloatHistory> floatHistoryList) {
        this.floatHistoryList = floatHistoryList;
    }
}
