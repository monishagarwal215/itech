package com.cashmanager.model.response;

import com.krapps.model.CommonJsonResponse;

/**
 * Created by Dinesh Adhikari on 19/06/17.
 */

public class RegisterDeviceResponse extends CommonJsonResponse {
    private String deviceDescription;
    private String deviceId;
    private String deviceName;
    private String registerId;

    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }
}
