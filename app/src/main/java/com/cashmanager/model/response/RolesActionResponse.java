package com.cashmanager.model.response;

import com.cashmanager.pojo.RoleActions;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

public class RolesActionResponse extends CommonJsonResponse {
    private List<RoleActions> roleActionsList;

    public List<RoleActions> getRoleActionsList() {
        return roleActionsList;
    }

    public void setRoleActionsList(List<RoleActions> roleActionsList) {
        this.roleActionsList = roleActionsList;
    }
}
