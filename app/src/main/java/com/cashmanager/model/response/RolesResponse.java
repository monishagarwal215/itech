package com.cashmanager.model.response;

import com.cashmanager.pojo.Roles;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

public class RolesResponse extends CommonJsonResponse {
    private List<Roles> rolesList;

    public List<Roles> getRolesList() {
        return rolesList;
    }

    public void setRolesList(List<Roles> rolesList) {
        this.rolesList = rolesList;
    }
}
