package com.cashmanager.numpad;

import android.view.View;

import static com.cashmanager.numpad.NumPadButton.BACKSPACE;
import static com.cashmanager.numpad.NumPadButton.NEXT;
import static com.cashmanager.numpad.NumPadButton.NUM_0;
import static com.cashmanager.numpad.NumPadButton.NUM_1;
import static com.cashmanager.numpad.NumPadButton.NUM_2;
import static com.cashmanager.numpad.NumPadButton.NUM_3;
import static com.cashmanager.numpad.NumPadButton.NUM_4;
import static com.cashmanager.numpad.NumPadButton.NUM_5;
import static com.cashmanager.numpad.NumPadButton.NUM_6;
import static com.cashmanager.numpad.NumPadButton.NUM_7;
import static com.cashmanager.numpad.NumPadButton.NUM_8;
import static com.cashmanager.numpad.NumPadButton.NUM_9;


public class NumPadClickListener implements View.OnClickListener {

    private static OnNumPadClickListener mListener;

    public NumPadClickListener(OnNumPadClickListener listener) {
        mListener = listener;
    }


    @Override
    public void onClick(View v) {
        if (mListener == null) {
            throw new NumPadListenerException("Number pad listener is not set");
        }
        mListener.onPadClicked(tagToNumPadButton((String) v.getTag()));
    }

    private NumPadButton tagToNumPadButton(String tag) {
        switch (tag) {
            case "0":
                return NUM_0;
            case "1":
                return NUM_1;
            case "2":
                return NUM_2;
            case "3":
                return NUM_3;
            case "4":
                return NUM_4;
            case "5":
                return NUM_5;
            case "6":
                return NUM_6;
            case "7":
                return NUM_7;
            case "8":
                return NUM_8;
            case "9":
                return NUM_9;
            case "next":
                return NEXT;
            case "backspace":
                return BACKSPACE;
            default:
                return BACKSPACE;
        }
    }
}
