package com.cashmanager.numpad;

public class NumPadListenerException extends RuntimeException {

    public NumPadListenerException(String message) {
        super(message);
    }
}
