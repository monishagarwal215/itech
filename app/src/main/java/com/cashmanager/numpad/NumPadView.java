package com.cashmanager.numpad;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cashmanager1.R;

/**
 * Draw the number pad view.
 */
public class NumPadView extends LinearLayout {


    private TextView txt0, txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9;
    private ImageView txtNext, txtBackspace;

    public NumPadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpView(context);
    }

    public void setNumberPadClickListener(OnNumPadClickListener onNumberPadClickListener) {
        setUpPadButtons(new NumPadClickListener(onNumberPadClickListener));
    }

    private void setUpView(Context context) {
        View view = inflate(context, R.layout.layout_numpad, this);
        initButtons(view);
    }

    private void initButtons(View view) {
        txt0 = (TextView) view.findViewById(R.id.txt0);
        txt1 = (TextView) view.findViewById(R.id.txt1);
        txt2 = (TextView) view.findViewById(R.id.txt2);
        txt3 = (TextView) view.findViewById(R.id.txt3);
        txt4 = (TextView) view.findViewById(R.id.txt4);
        txt5 = (TextView) view.findViewById(R.id.txt5);
        txt6 = (TextView) view.findViewById(R.id.txt6);
        txt7 = (TextView) view.findViewById(R.id.txt7);
        txt8 = (TextView) view.findViewById(R.id.txt8);
        txt9 = (TextView) view.findViewById(R.id.txt9);

        txtNext = (ImageView) view.findViewById(R.id.txtNext);
        txtBackspace = (ImageView) view.findViewById(R.id.txtBackspace);
    }

    private void setUpPadButtons(NumPadClickListener numberPadClickListener) {
        txt0.setOnClickListener(numberPadClickListener);
        txt1.setOnClickListener(numberPadClickListener);
        txt2.setOnClickListener(numberPadClickListener);
        txt3.setOnClickListener(numberPadClickListener);
        txt4.setOnClickListener(numberPadClickListener);
        txt5.setOnClickListener(numberPadClickListener);
        txt6.setOnClickListener(numberPadClickListener);
        txt7.setOnClickListener(numberPadClickListener);
        txt8.setOnClickListener(numberPadClickListener);
        txt9.setOnClickListener(numberPadClickListener);
        txtNext.setOnClickListener(numberPadClickListener);
        txtBackspace.setOnClickListener(numberPadClickListener);
    }

}
