package com.cashmanager.pojo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.cashmanager.util.DateConverter;

import java.util.Date;
import java.util.List;

/**
 * Created by Dinesh Adhikari on 07/06/17.
 */
@TypeConverters(DateConverter.class)
@Entity(tableName = "count_history", indices = {@Index(value = {"serverId"}, unique = true)})
public class CountHistory {
    @PrimaryKey(autoGenerate = true)
    private int countId;
    private String countType;
    private String countBy;
    private String countRecordedBy;
    private Date countDate;
    private String registerId;
    private int userCountValue;
    @NonNull
    private String deviceId;
    private Date syncDate;
    private int systemCountValue;
    private boolean synced;
    private String serverId;
    private String notes;
    @Ignore
    private List<CountDetails> countDetails;

    public int getCountId() {
        return countId;
    }

    public void setCountId(int countId) {
        this.countId = countId;
    }

    public String getCountBy() {
        return countBy;
    }

    public void setCountBy(String countBy) {
        this.countBy = countBy;
    }

    public Date getCountDate() {
        return countDate;
    }

    public void setCountDate(Date countDate) {
        this.countDate = countDate;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public int getUserCountValue() {
        return userCountValue;
    }

    public void setUserCountValue(int userCountValue) {
        this.userCountValue = userCountValue;
    }

    public int getSystemCountValue() {
        return systemCountValue;
    }

    public void setSystemCountValue(int systemCountValue) {
        this.systemCountValue = systemCountValue;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getCountType() {
        return countType;
    }

    public void setCountType(String countType) {
        this.countType = countType;
    }

    public List<CountDetails> getCountDetails() {
        return countDetails;
    }

    public void setCountDetails(List<CountDetails> countDetails) {
        this.countDetails = countDetails;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @NonNull
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(@NonNull String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(Date syncDate) {
        this.syncDate = syncDate;
    }

    public String getCountRecordedBy() {
        return countRecordedBy;
    }

    public void setCountRecordedBy(String countRecordedBy) {
        this.countRecordedBy = countRecordedBy;
    }
}
