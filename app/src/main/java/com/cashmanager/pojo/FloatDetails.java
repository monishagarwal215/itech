package com.cashmanager.pojo;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.cashmanager.util.DateConverter;

import java.util.Date;

/**
 * Created by Dinesh Adhikari on 07/06/17.
 */
@TypeConverters(DateConverter.class)
@Entity(tableName = "float_details", indices = {@Index(value = {"serverId"}, unique = true)})
public class FloatDetails {
    @PrimaryKey(autoGenerate = true)
    private int floatDetailsId;
    @NonNull
    private int floatId;
    @NonNull
    @Embedded
    private Denominations denominations;
    @NonNull
    private String deviceId;
    private Date syncDate;
    private int denominationQuantity;
    @NonNull
    private boolean synced;
    private String serverId;

    public int getFloatDetailsId() {
        return floatDetailsId;
    }

    public void setFloatDetailsId(int floatDetailsId) {
        this.floatDetailsId = floatDetailsId;
    }

    public Denominations getDenominations() {
        return denominations;
    }

    public void setDenominations(Denominations denominations) {
        this.denominations = denominations;
    }

    public int getDenominationQuantity() {
        return denominationQuantity;
    }

    public void setDenominationQuantity(int denominationQuantity) {
        this.denominationQuantity = denominationQuantity;
    }

    @NonNull
    public int getFloatId() {
        return floatId;
    }

    public void setFloatId(@NonNull int floatId) {
        this.floatId = floatId;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    @NonNull
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(@NonNull String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(Date syncDate) {
        this.syncDate = syncDate;
    }
}
