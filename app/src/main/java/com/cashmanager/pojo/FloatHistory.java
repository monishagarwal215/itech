package com.cashmanager.pojo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.cashmanager.util.DateConverter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinesh Adhikari on 07/06/17.
 */

@TypeConverters(DateConverter.class)
@Entity(tableName = "float_history", indices = {@Index(value = {"serverId"}, unique = true)})
public class FloatHistory {
    @PrimaryKey(autoGenerate = true)
    private int floatId;
    private String floatSetBy;
    private Date floatDate;
    @NonNull
    private String registerId;
    @NonNull
    private String deviceId;
    private Date syncDate;
    private int floatValue;
    @NonNull
    private boolean synced = false;
    private String serverId;
    @Ignore
    private List<FloatDetails> floatDetails;

    public int getFloatId() {
        return floatId;
    }

    public void setFloatId(int floatId) {
        this.floatId = floatId;
    }

    public String getFloatSetBy() {
        return floatSetBy;
    }

    public void setFloatSetBy(String floatSetBy) {
        this.floatSetBy = floatSetBy;
    }

    public Date getFloatDate() {
        return floatDate;
    }

    public void setFloatDate(Date floatDate) {
        this.floatDate = floatDate;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public int getFloatValue() {
        return floatValue;
    }

    public void setFloatValue(int floatValue) {
        this.floatValue = floatValue;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public List<FloatDetails> getFloatDetails() {
        return floatDetails;
    }

    public void setFloatDetails(List<FloatDetails> floatDetails) {
        this.floatDetails = floatDetails;
    }

    @NonNull
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(@NonNull String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(Date syncDate) {
        this.syncDate = syncDate;
    }
}
