package com.cashmanager.pojo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "role_actions")
public class RoleActions {
    @PrimaryKey(autoGenerate = true)
    private int roleActionId;

    @NonNull
    private int roleId;

    @NonNull
    private int actionId;

    private boolean isActive = true;

    private int canModify;

    public int getCanModify() {
        return canModify;
    }

    public void setCanModify(int canModify) {
        this.canModify = canModify;
    }

    public int getRoleActionId() {
        return roleActionId;
    }

    public void setRoleActionId(int roleActionId) {
        this.roleActionId = roleActionId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
