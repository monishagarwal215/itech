package com.cashmanager.pojo;

/**
 * Created by Dinesh on 23/7/17.
 */

public class RoleNameResponse {
    private String systemRole;
    private String name;
    private String id;

    public String getSystemRole() {
        return systemRole;
    }

    public void setSystemRole(String systemRole) {
        this.systemRole = systemRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
