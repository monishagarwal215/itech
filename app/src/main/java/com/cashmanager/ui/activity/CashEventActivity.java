package com.cashmanager.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.cashmanager.constants.ApiConstants;
import com.cashmanager.model.CashEventData;
import com.cashmanager.model.response.CashEventResponse;
import com.cashmanager.pojo.CountHistory;
import com.cashmanager.pojo.FloatHistory;
import com.cashmanager.ui.adapter.CashEventAdapter;
import com.cashmanager.util.LookAtMePrefernece;
import com.cashmanager1.R;
import com.clover.sdk.v1.ResultStatus;
import com.clover.sdk.v1.merchant.Merchant;
import com.clover.sdk.v1.merchant.MerchantConnector;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jive on 5/16/2017.
 */

public class CashEventActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    RecyclerView listview;
    private String url;
    private CashEventAdapter cashEventAdapter;
    private Spinner spinner;
    private List<CashEventData> originalAllData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_event);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        listview = (RecyclerView) findViewById(R.id.listview);
        listview.setNestedScrollingEnabled(false);
        listview.setFocusable(false);
        listview.setLayoutManager(linearLayoutManager);

        cashEventAdapter = new CashEventAdapter(this);
        listview.setAdapter(cashEventAdapter);

        setUpSpinner();

    }

    private void setUpSpinner() {
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.cash_events, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (account != null) {
            if (merchantConnector != null && LookAtMePrefernece.getInstance().getMerchantId() == null) {
                getMerchant();
            } else {
                hitApiRequest(ApiConstants.REQUEST_GET_CASH_EVENTS, true);
            }
        } else {
            startAccountChooser();
        }
    }

    private void getMerchant() {
        merchantConnector.getMerchant(new MerchantConnector.MerchantCallback<Merchant>() {
            @Override
            public void onServiceSuccess(Merchant result, ResultStatus status) {
                super.onServiceSuccess(result, status);
                LookAtMePrefernece.getInstance().setMerchantId(result.getId());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hitApiRequest(ApiConstants.REQUEST_GET_CASH_EVENTS, true);
                    }
                }, 500);

            }

            @Override
            public void onServiceFailure(ResultStatus status) {
                super.onServiceFailure(status);
                Log.d("CASH MANAGER", "get merchant bind failure");
            }

            @Override
            public void onServiceConnectionFailure() {
                super.onServiceConnectionFailure();
                Log.d("CASH MANAGER", "get merchant bind failure");
            }
        });
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {

            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        if (showLoader) {
            ((BaseActivity) this).showProgressDialog();
        }
        switch (reqType) {
            case ApiConstants.REQUEST_GET_CASH_EVENTS:
                url = LookAtMePrefernece.getInstance().getBaseUrl() +
                        String.format(ApiConstants.URL_CASH_EVENTS,
                                LookAtMePrefernece.getInstance().getMerchantId(),
                                LookAtMePrefernece.getInstance().getAuthToken());
                className = CashEventResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(ApiConstants.REQUEST_GET_CASH_EVENTS));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                super.hitApiRequest(reqType, showLoader);
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_GET_CASH_EVENTS) {

        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            ((BaseActivity) this).removeProgressDialog();
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_CASH_EVENTS:
                    CashEventResponse cashEventResponse = (CashEventResponse) responseObject;
                    if (cashEventResponse != null && cashEventResponse.getCashEventDataList() != null) {
                        originalAllData = new ArrayList<>();
                        originalAllData.addAll(cashEventResponse.getCashEventDataList());
                        setAdapter(cashEventResponse.getCashEventDataList());
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setAdapter(List<CashEventData> cashEventData) {
        if (cashEventAdapter != null) {
            cashEventAdapter.setListData(cashEventData);
            cashEventAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        DateTime today;
        switch (position) {
            case 0:
                //All
                //10 year
                today = new DateTime();
                System.out.println(today.withDayOfMonth(1).minusYears(1)); //prints 2017-01-01\
                today = today.withDayOfMonth(1).minusYears(10).withTime(0, 0, 0, 0);
                filterCashEventsByTime(today.toDate().getTime());
                break;
            case 1:
                //today
                today = new DateTime().withTimeAtStartOfDay();
                filterCashEventsByTime(today.getMillis());
                break;
            case 2:
                //This week
                today = new DateTime();
                System.out.println(today.withDayOfWeek(DateTimeConstants.MONDAY)); //prints 2011-01-17\
                today = today.withDayOfWeek(DateTimeConstants.MONDAY).withTime(0, 0, 0, 0);
                filterCashEventsByTime(today.toDate().getTime());
                break;
            case 3:
                //This Month
                today = new DateTime();
                System.out.println(today.withDayOfMonth(1)); //prints 2017-01-01\
                today = today.withDayOfMonth(1).withTime(0, 0, 0, 0);
                filterCashEventsByTime(today.toDate().getTime());
                break;
            case 4:
                //3 Month
                today = new DateTime();
                System.out.println(today.withDayOfMonth(1).minusMonths(2)); //prints 2017-01-01\
                today = today.withDayOfMonth(1).minusMonths(2).withTime(0, 0, 0, 0);
                filterCashEventsByTime(today.toDate().getTime());
                break;
            case 5:
                //6 Month
                today = new DateTime();
                System.out.println(today.withDayOfMonth(1).minusMonths(5)); //prints 2017-01-01\
                today = today.withDayOfMonth(1).minusMonths(5).withTime(0, 0, 0, 0);
                filterCashEventsByTime(today.toDate().getTime());
                break;
            case 6:
                //1 year
                today = new DateTime();
                System.out.println(today.withDayOfMonth(1).minusYears(1)); //prints 2017-01-01\
                today = today.withDayOfMonth(1).minusYears(1).withTime(0, 0, 0, 0);
                filterCashEventsByTime(today.toDate().getTime());
                break;
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void filterCashEventsByTime(long millis) {
        if (cashEventAdapter != null && originalAllData != null) {
            List<CashEventData> cashEventData = new ArrayList<>();
            for (CashEventData temp : originalAllData) {
                if (temp.getTimestamp().after(new Date(millis))) {
                    cashEventData.add(temp);
                }
            }
            setAdapter(cashEventData);
        }
    }
}