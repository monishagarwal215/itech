package com.cashmanager.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;

import com.cashmanager.constants.AppDatabase;
import com.cashmanager.ui.fragment.left.SetFloatLeftFragment;
import com.cashmanager.ui.fragment.middle.SetFloatMiddleFragment;
import com.cashmanager.ui.fragment.right.RightMenuFragment;
import com.cashmanager1.R;
import com.krapps.ui.BaseActivity;

public class HomeActivity extends BaseActivity {

    private AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_home);
        //By Dinesh Adhikari comment

        replaceFragment(null, new RightMenuFragment(), null, false, R.id.content_frame_right);
        replaceFragment(null, new SetFloatLeftFragment(), null, false, R.id.content_frame_left);
        replaceFragment(null, new SetFloatMiddleFragment(), null, false, R.id.content_frame_middle);

        InputMethodManager imm = (InputMethodManager) getBaseContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

}
