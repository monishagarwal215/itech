package com.cashmanager.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.cashmanager.constants.ApiConstants;
import com.cashmanager.constants.AppConstants;
import com.cashmanager.constants.AppDatabase;
import com.cashmanager.gateway.*;
import com.cashmanager.model.DeviceData;
import com.cashmanager.model.request.RegisterDeviceRequest;
import com.cashmanager.model.response.DeviceListResponse;
import com.cashmanager.model.response.RegisterDeviceResponse;
import com.cashmanager.pojo.Actions;
import com.cashmanager.pojo.RoleActions;
import com.cashmanager.pojo.RoleNameResponse;
import com.cashmanager.pojo.Roles;
import com.cashmanager.util.DbInitializer;
import com.cashmanager.util.LookAtMePrefernece;
import com.cashmanager.util.SyncDatabase;
import com.cashmanager1.R;
import com.clover.sdk.util.CloverAuth;
import com.clover.sdk.v1.Intents;
import com.clover.sdk.v1.ResultStatus;
import com.clover.sdk.v1.ServiceConnector;
import com.clover.sdk.v1.merchant.Merchant;
import com.clover.sdk.v1.merchant.MerchantConnector;
import com.clover.sdk.v3.employees.Employee;
import com.clover.sdk.v3.employees.EmployeeConnector;
import com.clover.sdk.v3.employees.EmployeeIntent;
import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends BaseActivity implements EmployeeConnector.OnActiveEmployeeChangedListener, ServiceConnector.OnServiceConnectedListener {

    private static final String TAG = "MainActivity";
    private List<Employee> employees;
    private String url;

    private AppDatabase appDatabase;
    private String cloverRoleId;
    private ImageView imgRefresh;

    private boolean synced = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.txtCloseOut).setOnClickListener(this);
        findViewById(R.id.txtSetFloat).setOnClickListener(this);
        findViewById(R.id.txtCheckbalance).setOnClickListener(this);
        findViewById(R.id.txtMakeDrop).setOnClickListener(this);
        findViewById(R.id.txtMainMenu).setOnClickListener(this);
        findViewById(R.id.txtShowDevices).setOnClickListener(this);
        findViewById(R.id.txtCashEvent).setOnClickListener(this);
        findViewById(R.id.txtSyncPull).setOnClickListener(this);
        findViewById(R.id.txtSyncPush).setOnClickListener(this);
        findViewById(R.id.imgRefresh).setOnClickListener(this);

        findViewById(R.id.txtShowDevices).setVisibility(View.GONE);
        findViewById(R.id.txtCashEvent).setVisibility(View.GONE);

        registerReceiver(activeEmployeeChangedReceiver, new IntentFilter(EmployeeIntent.ACTION_ACTIVE_EMPLOYEE_CHANGED));

        appDatabase = BaseApplication.getAppDatabase(this);
        DbInitializer.populateDbAsynTask(appDatabase);

        if (synced == false) {
            new SyncRolesTask(this).execute();
            new SyncActionsTask(this).execute();
            new SyncRolesActionsTask(this).execute();
            new SyncDenominationTask(this).execute();

            /*SyncDatabase.getInstance(this).syncDenomination();
            SyncDatabase.getInstance(this).syncRoles();
            SyncDatabase.getInstance(this).syncFloatHistory();
            SyncDatabase.getInstance(this).syncCountHistory();*/

            synced = true;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (account != null) {
            getActiveEmployee();
            getEmployees();
            getAuthTokenAndBaseUrl();
        } else {
            startAccountChooser();
        }

        if (!StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getMerchantId())
                && !StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getDeviceId())
                && !StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getAuthToken())) {
            if (StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getRegisterId())) {
                hitApiRequest(ApiConstants.REQUEST_REGISTER_DEVICE, true);
            }
        } else {
            if (merchantConnector != null) {
                getMerchant();
            } else {
                connect();
            }
        }
    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        if (showLoader) {
            showProgressDialog();
        }
        VolleyJsonRequest request = null;
        VolleyStringRequest volleyStringRequest;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_REGISTER_DEVICE:
                url = ApiConstants.URL_REGISTER_DEVICE;
                className = RegisterDeviceResponse.class;
                try {
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType) {
                    }, getJsonString(ApiConstants.REQUEST_REGISTER_DEVICE));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_GET_DEVICES:
                url = LookAtMePrefernece.getInstance().getBaseUrl() +
                        String.format(ApiConstants.URL_DEVICES,
                                LookAtMePrefernece.getInstance().getMerchantId(),
                                LookAtMePrefernece.getInstance().getAuthToken());
                className = DeviceListResponse.class;
                volleyStringRequest = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(ApiConstants.REQUEST_GET_DEVICES));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(volleyStringRequest, url);
                break;
            case ApiConstants.REQUEST_GET_CLOVER_ROLE_NAME:
                url = LookAtMePrefernece.getInstance().getBaseUrl() +
                        String.format(ApiConstants.URL_GET_ROLE_NAME,
                                LookAtMePrefernece.getInstance().getMerchantId(),
                                cloverRoleId,
                                LookAtMePrefernece.getInstance().getAuthToken());
                className = RoleNameResponse.class;
                volleyStringRequest = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(ApiConstants.REQUEST_GET_CLOVER_ROLE_NAME));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(volleyStringRequest, url);
                break;
            default:
                super.hitApiRequest(reqType, showLoader);
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_GET_CASH_EVENTS) {

        }
        return params;
    }


    private String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_REGISTER_DEVICE) {
            RegisterDeviceRequest registerDeviceRequest = new RegisterDeviceRequest();
            registerDeviceRequest.setDeviceDescription("South Street Mall - Altamont Station");
            registerDeviceRequest.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
            registerDeviceRequest.setDeviceName("South Street Mall - Altamont Station");
            return new Gson().toJson(registerDeviceRequest);
        }
        return null;
    }


    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {
        try {
            removeProgressDialog();
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }

            switch (reqType) {
                case ApiConstants.REQUEST_REGISTER_DEVICE:
                    RegisterDeviceResponse registerDeviceResponse = new Gson().fromJson(responseString, RegisterDeviceResponse.class);
                    if (registerDeviceResponse.getStatus() == 1) {
                        LookAtMePrefernece.getInstance().setRegisterId(registerDeviceResponse.getRegisterId());
                        //LookAtMePrefernece.getInstance().setDeviceName(registerDeviceResponse.getDeviceName());
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            ((BaseActivity) this).removeProgressDialog();
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_CLOVER_ROLE_NAME:
                    RoleNameResponse roleNameResponse = (RoleNameResponse) responseObject;
                    if (roleNameResponse != null) {
                        if (!StringUtils.isNullOrEmpty(roleNameResponse.getName())) {
                            LookAtMePrefernece.getInstance().setCloverRoleName(roleNameResponse.getName());
                            List<Roles> roles = appDatabase.rolesDao().getRoleByName(roleNameResponse.getName().toUpperCase());
                            List<Actions> actionsList = appDatabase.actionDao().getAllActions();
                            if (roles != null && roles.size() > 0) {
                                List<RoleActions> roleActionsList = appDatabase.rolesActionsDao().getByRoleId(roles.get(0).getRoleId());
                                showMenuAccordingToRole(actionsList, roleActionsList);
                            }
                        }
                    }
                    break;
                case ApiConstants.REQUEST_GET_DEVICES:
                    DeviceListResponse deviceListResponse = (DeviceListResponse) responseObject;
                    if (deviceListResponse != null && deviceListResponse.getDeviceDataList() != null) {
                        StringBuilder builder = new StringBuilder();
                        for (DeviceData temp : deviceListResponse.getDeviceDataList()) {
                            if (temp.getId().equalsIgnoreCase(LookAtMePrefernece.getInstance().getDeviceId())) {
                                LookAtMePrefernece.getInstance().setDeviceName(temp.getName());
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        new SyncDeviceTask(MainActivity.this).execute();
                                    }
                                }, 500);
                            }

                        }
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void getEmployees() {
        employeeConnector.getEmployees(new EmployeeConnector.EmployeeCallback<List<Employee>>() {
            @Override
            public void onServiceSuccess(List<Employee> result, ResultStatus status) {
                super.onServiceSuccess(result, status);
                employees = result;
            }

            @Override
            public void onServiceFailure(ResultStatus status) {
                super.onServiceFailure(status);
            }

            @Override
            public void onServiceConnectionFailure() {
                super.onServiceConnectionFailure();
            }
        });
    }

    private void getActiveEmployee() {
        employeeConnector.getEmployee(new EmployeeConnector.EmployeeCallback<Employee>() {
            @Override
            public void onServiceSuccess(Employee result, ResultStatus status) {
                super.onServiceSuccess(result, status);
                updateActiveEmployee("get active employee success", status, result);
                result.validate();
            }

            @Override
            public void onServiceFailure(ResultStatus status) {
                super.onServiceFailure(status);
                updateActiveEmployee("get active employee failure", status, null);
            }

            @Override
            public void onServiceConnectionFailure() {
                super.onServiceConnectionFailure();
                updateActiveEmployee("get active employee failure", null, null);
            }
        });
    }

    private MerchantConnector.OnMerchantChangedListener merchantListener = new MerchantConnector.OnMerchantChangedListener() {
        @Override
        public void onMerchantChanged(Merchant merchant) {
            getEmployees();
        }
    };

    private final BroadcastReceiver activeEmployeeChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String employeeId = intent.getStringExtra(Intents.EXTRA_EMPLOYEE_ID);
            account = EmployeeIntent.getAccount(intent);
            connect();
            employeeConnector.getEmployee(employeeId, new EmployeeConnector.EmployeeCallback<Employee>() {
                @Override
                public void onServiceSuccess(Employee result, ResultStatus status) {
                    //notifyActiveEmployeeChanged(result);
                }
            });
        }
    };


    private void getMerchant() {
        merchantConnector.getMerchant(new MerchantConnector.MerchantCallback<Merchant>() {
            @Override
            public void onServiceSuccess(Merchant result, ResultStatus status) {
                super.onServiceSuccess(result, status);
                LookAtMePrefernece.getInstance().setMerchantId(result.getId());
                LookAtMePrefernece.getInstance().setDeviceId(result.getDeviceId());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hitApiRequest(ApiConstants.REQUEST_REGISTER_DEVICE, true);
                    }
                }, 500);

            }

            @Override
            public void onServiceFailure(ResultStatus status) {
                super.onServiceFailure(status);
                Log.d("CASH MANAGER", "get merchant bind failure");
            }

            @Override
            public void onServiceConnectionFailure() {
                super.onServiceConnectionFailure();
                Log.d("CASH MANAGER", "get merchant bind failure");
            }
        });
    }

    private void updateActiveEmployee(String status, ResultStatus resultStatus, Employee result) {

        try {
            if (result != null) {
                if (!StringUtils.isNullOrEmpty(result.getName())) {
                    ((TextView) findViewById(R.id.txtActiveUser)).setText(result.getName());
                    LookAtMePrefernece.getInstance().setActiveUsername(result.getName());
                }

                if (result.getRoles() != null && result.getRoles().size() > 0 && !StringUtils.isNullOrEmpty(result.getRoles().get(0).getId())) {
                    cloverRoleId = result.getRoles().get(0).getId();
                    hitApiRequest(ApiConstants.REQUEST_GET_CLOVER_ROLE_NAME, true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showMenuAccordingToRole(List<Actions> actionsList, List<RoleActions> roleActionsList) {

        findViewById(R.id.txtCloseOut).setVisibility(View.GONE);
        findViewById(R.id.txtSetFloat).setVisibility(View.GONE);
        findViewById(R.id.txtCheckbalance).setVisibility(View.GONE);
        findViewById(R.id.txtMakeDrop).setVisibility(View.GONE);

        for (RoleActions temp : roleActionsList) {
            Actions actionToFind = new Actions();
            actionToFind.setActionId(temp.getActionId());
            if (actionsList.contains(actionToFind)) {
                int pos = actionsList.indexOf(actionToFind);
                Actions actions = actionsList.get(pos);
                showMenu(actions);

            }
        }

    }

    private void showMenu(Actions actions) {
        switch (actions.getActionName()) {
            case AppConstants.ACTION_SET_FLOAT:
                findViewById(R.id.txtSetFloat).setVisibility(View.VISIBLE);
                break;
            case AppConstants.ACTION_CHECK_BALANCE:
                findViewById(R.id.txtCheckbalance).setVisibility(View.VISIBLE);
                break;
            case AppConstants.ACTION_MAKE_DROP:
                findViewById(R.id.txtMakeDrop).setVisibility(View.VISIBLE);
                break;
            case AppConstants.ACTION_CLOSE_OUT:
                findViewById(R.id.txtCloseOut).setVisibility(View.VISIBLE);
                break;
        }
    }


    @Override
    public void onActiveEmployeeChanged(Employee employee) {
        updateActiveEmployee("active employee changed", null, employee);
    }

    @Override
    public void onServiceConnected(ServiceConnector connector) {
        Log.i(TAG, "service connected: " + connector);
    }

    @Override
    public void onServiceDisconnected(ServiceConnector connector) {
        Log.i(TAG, "service disconnected: " + connector);
    }

    private void getAuthTokenAndBaseUrl() {
        new AsyncTask<Void, String, Void>() {

            @Override
            protected void onProgressUpdate(String... values) {
                String logString = values[0];
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    publishProgress("Requesting auth token");
                    CloverAuth.AuthResult authResult = CloverAuth.authenticate(MainActivity.this, account);

                    if (authResult.authToken != null && authResult.baseUrl != null) {
                        LookAtMePrefernece.getInstance().setAuthToken(authResult.authToken);
                        LookAtMePrefernece.getInstance().setBaseUrl(authResult.baseUrl);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
            }
        }.execute();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.txtCloseOut:
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(ApiConstants.EXTRA_POSITION, 3);
                startActivity(intent);
                break;
            case R.id.txtSetFloat:
                if (LookAtMePrefernece.getInstance().isSetFloatDisabled() == false) {
                    intent = new Intent(this, HomeActivity.class);
                    intent.putExtra(ApiConstants.EXTRA_POSITION, 0);
                    startActivity(intent);
                } else {
                    AlertDialogUtils.showAlertDialog(this, "Cashmanager", "You must first closeout your current shift before you can set float again.", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {

                        }
                    });
                }
                break;
            case R.id.txtCheckbalance:
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(ApiConstants.EXTRA_POSITION, 1);
                startActivity(intent);
                break;
            case R.id.txtMakeDrop:
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(ApiConstants.EXTRA_POSITION, 2);
                startActivity(intent);
                break;
            case R.id.txtMainMenu:
                ToastUtils.showToast(this, "txtMainMenu");
                finish();
                break;
            case R.id.txtShowDevices:
                intent = new Intent(this, ShowDevicesActivity.class);
                startActivity(intent);
                break;
            case R.id.txtCashEvent:
                intent = new Intent(this, CashEventActivity.class);
                startActivity(intent);
                break;
            case R.id.txtSyncPull:
                SyncDatabase.getInstance(this).syncDenomination();
                SyncDatabase.getInstance(this).syncRoles();
                break;
            case R.id.txtSyncPush:
                SyncDatabase.getInstance(this).syncFloatHistory();
                SyncDatabase.getInstance(this).syncCountHistory();
                break;
            case R.id.imgRefresh:
                refresh();

                new SyncRolesTask(this).execute();
                new SyncActionsTask(this).execute();
                new SyncRolesActionsTask(this).execute();
                new SyncDenominationTask(this).execute();

                new SyncFloatHistoryTask(this).execute();
                new SyncCountHistoryTask(this).execute();

                /*SyncDatabase.getInstance(this).syncDenomination();
                SyncDatabase.getInstance(this).syncRoles();
                SyncDatabase.getInstance(this).syncFloatHistory();
                SyncDatabase.getInstance(this).syncCountHistory();*/
                hitApiRequest(ApiConstants.REQUEST_GET_DEVICES, false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        completeRefresh();
                    }
                }, 4000);
                break;
            default:
                super.onClick(v);
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(activeEmployeeChangedReceiver);
        super.onDestroy();
    }

    public void refresh() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imgRefresh = (ImageView) findViewById(R.id.imgRefresh);

        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_refresh);
        rotation.setRepeatCount(Animation.INFINITE);
        imgRefresh.startAnimation(rotation);
    }

    public void completeRefresh() {
        imgRefresh.clearAnimation();
    }
}
