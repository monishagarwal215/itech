package com.cashmanager.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.cashmanager.constants.ApiConstants;
import com.cashmanager.model.DeviceData;
import com.cashmanager.model.response.DeviceListResponse;
import com.cashmanager.ui.adapter.DeviceAdapter;
import com.cashmanager.util.LookAtMePrefernece;
import com.cashmanager1.R;
import com.clover.sdk.v1.ResultStatus;
import com.clover.sdk.v1.merchant.Merchant;
import com.clover.sdk.v1.merchant.MerchantConnector;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

import java.util.HashMap;
import java.util.List;

/**
 * Created by jive on 5/16/2017.
 */

public class ShowDevicesActivity extends BaseActivity {

    RecyclerView listview;
    private String url;
    private DeviceAdapter deviceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_devices);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        listview = (RecyclerView) findViewById(R.id.listview);
        listview.setNestedScrollingEnabled(false);
        listview.setFocusable(false);
        listview.setLayoutManager(linearLayoutManager);

        deviceAdapter = new DeviceAdapter(this);
        listview.setAdapter(deviceAdapter);

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (account != null) {
            if (merchantConnector != null && LookAtMePrefernece.getInstance().getMerchantId() == null) {
                getMerchant();
            } else {
                hitApiRequest(ApiConstants.REQUEST_GET_DEVICES, true);
            }
        } else {
            startAccountChooser();
        }
    }

    private void getMerchant() {
        merchantConnector.getMerchant(new MerchantConnector.MerchantCallback<Merchant>() {
            @Override
            public void onServiceSuccess(Merchant result, ResultStatus status) {
                super.onServiceSuccess(result, status);
                LookAtMePrefernece.getInstance().setMerchantId(result.getId());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hitApiRequest(ApiConstants.REQUEST_GET_DEVICES, true);
                    }
                }, 500);

            }

            @Override
            public void onServiceFailure(ResultStatus status) {
                super.onServiceFailure(status);
                Log.d("CASH MANAGER", "get merchant bind failure");
            }

            @Override
            public void onServiceConnectionFailure() {
                super.onServiceConnectionFailure();
                Log.d("CASH MANAGER", "get merchant bind failure");
            }
        });
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {

            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        if (showLoader) {
            ((BaseActivity) this).showProgressDialog();
        }
        switch (reqType) {
            case ApiConstants.REQUEST_GET_DEVICES:
                url = LookAtMePrefernece.getInstance().getBaseUrl() +
                        String.format(ApiConstants.URL_DEVICES,
                                LookAtMePrefernece.getInstance().getMerchantId(),
                                LookAtMePrefernece.getInstance().getAuthToken());
                className = DeviceListResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(ApiConstants.REQUEST_GET_DEVICES));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                super.hitApiRequest(reqType, showLoader);
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_GET_DEVICES) {

        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            ((BaseActivity) this).removeProgressDialog();
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_DEVICES:
                    DeviceListResponse deviceListResponse = (DeviceListResponse) responseObject;
                    if (deviceListResponse != null && deviceListResponse.getDeviceDataList() != null) {
                        setAdapter(deviceListResponse.getDeviceDataList());
                        /*StringBuilder builder = new StringBuilder();
                        for (DeviceData temp : deviceListResponse.getDeviceDataList()) {
                            builder.append(temp.getName())
                                    .append("\n");
                        }
                        ((TextView) findViewById(R.id.devices)).setText(builder.toString());*/
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setAdapter(List<DeviceData> elements) {
        if (deviceAdapter != null) {
            deviceAdapter.setListData(elements);
            deviceAdapter.notifyDataSetChanged();
        }
    }


}