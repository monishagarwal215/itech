package com.cashmanager.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cashmanager.model.CashEventData;
import com.cashmanager1.R;
import com.krapps.utils.StringUtils;

import java.util.List;

public class CashEventAdapter extends RecyclerView.Adapter<CashEventAdapter.ViewHolder> {

    private Context mContext;
    private List<CashEventData> mlistData;
    private OnClickListener mClickListener;

    public CashEventAdapter(Context context) {
        mContext = context;
    }


    public void setListData(List<CashEventData> cashEventData) {
        this.mlistData = cashEventData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_cash_event, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {

        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getNote())) {
            viewholder.txtOrderName.setText(mlistData.get(position).getNote());
        }


        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getDevice().getId())) {
            viewholder.txtRegister.setText(mlistData.get(position).getDevice().getId());
        }


        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getType())) {
            viewholder.txtTransactionType.setText(mlistData.get(position).getType());
        }


        viewholder.txtAmount.setText(mlistData.get(position).getAmountChange() + "");

        if (mlistData.get(position).getTimestamp() != null) {
            viewholder.txtDate.setText(mlistData.get(position).getTimestamp() + "");
        }


    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtOrderName;
        private final TextView txtRegister;
        private final TextView txtAmount;
        private final TextView txtTransactionType;
        private final TextView txtDate;


        public ViewHolder(View itemView) {
            super(itemView);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtOrderName = (TextView) itemView.findViewById(R.id.txtOrderName);
            txtRegister = (TextView) itemView.findViewById(R.id.txtRegister);
            txtTransactionType = (TextView) itemView.findViewById(R.id.txtTransactionType);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
        }
    }


}
