package com.cashmanager.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cashmanager.model.DeviceData;
import com.cashmanager1.R;
import com.krapps.utils.StringUtils;

import java.util.List;

public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.ViewHolder> {

    private Context mContext;
    private List<DeviceData> mlistData;
    private OnClickListener mClickListener;

    public DeviceAdapter(Context context) {
        mContext = context;
    }


    public void setListData(List<DeviceData> searchSections) {
        this.mlistData = searchSections;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_devices, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {

        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getName())) {
            viewholder.txtDeviceName.setText(mlistData.get(position).getName());
        }


        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getId())) {
            viewholder.txtDeviceId.setText(mlistData.get(position).getId());
        }


        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getDeviceTypeName())) {
            viewholder.txtDeviceType.setText(mlistData.get(position).getDeviceTypeName());
        }


    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtDeviceName;
        private final TextView txtDeviceId;
        private TextView txtDeviceType;

        public ViewHolder(View itemView) {
            super(itemView);
            txtDeviceName = (TextView) itemView.findViewById(R.id.txtDeviceName);
            txtDeviceId = (TextView) itemView.findViewById(R.id.txtDeviceId);
            txtDeviceType = (TextView) itemView.findViewById(R.id.txtDeviceType);

        }
    }


}
