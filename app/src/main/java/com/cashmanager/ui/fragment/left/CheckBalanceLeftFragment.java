package com.cashmanager.ui.fragment.left;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cashmanager.constants.ApiConstants;
import com.cashmanager.constants.AppConstants;
import com.cashmanager.constants.AppDatabase;
import com.cashmanager.listener.CurrencyEvent;
import com.cashmanager.listener.MessageEvent;
import com.cashmanager.listener.SaveCheckBalanceEvent;
import com.cashmanager.model.CashEventData;
import com.cashmanager.model.response.CashEventResponse;
import com.cashmanager.pojo.CountDetails;
import com.cashmanager.pojo.CountHistory;
import com.cashmanager.pojo.Denominations;
import com.cashmanager.pojo.FloatHistory;
import com.cashmanager.util.LookAtMePrefernece;
import com.cashmanager1.R;
import com.clover.sdk.v1.ResultStatus;
import com.clover.sdk.v1.merchant.Merchant;
import com.clover.sdk.v1.merchant.MerchantConnector;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.cashmanager1.R.id.txtDifference;

/**
 * Created by Dinesh Adhikari on 25/04/16.
 */
public class CheckBalanceLeftFragment extends BaseFragment implements View.OnFocusChangeListener {

    private View mView;
    private LayoutInflater mInflater;
    private EditText focusedEditText;
    private AppDatabase appDatabase;
    private List<Denominations> mDenominations;
    private String url;
    private int floatTotal = 0;
    private int netSales = 0;
    int currencyTotal = 0;
    int denominationposition = 0;
    int checkbalancedifference = 0;

    DecimalFormat dollarformat = new DecimalFormat("#0.00");
    private String message;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_check_balance_left, null);
        mInflater = LayoutInflater.from(getActivity().getBaseContext());

        appDatabase = BaseApplication.getAppDatabase(getActivity());
        LiveData<List<Denominations>> denominations = appDatabase.denominationDao().getAllDenominations();
        denominations.observe(this, new Observer<List<Denominations>>() {
            @Override
            public void onChanged(@Nullable List<Denominations> denominations) {
                mDenominations = denominations;
                addCurrentyToLayout();

                FloatHistory latestfloat = appDatabase.floatHistoryDao().getLatestFloatHistory();
                if (latestfloat != null) {
                    ((TextView) mView.findViewById(R.id.txtFloat)).setText("$" + dollarformat.format((float) latestfloat.getFloatValue() / 100));
                    floatTotal = latestfloat.getFloatValue();
                }
            }
        });


        return mView;
    }

    public int getCurrencyTotal() {
        return currencyTotal;
    }

    public int getDifference() {
        return netSales + floatTotal - currencyTotal;
    }


    private int getDenominationposition() {
        return denominationposition;
    }

    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        super.onResume();

        if (!StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getMerchantId())
                && !StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getDeviceId())
                && !StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getAuthToken())) {
            hitApiRequest(ApiConstants.REQUEST_GET_CASH_EVENTS, true);
        } else {
            getMerchant();
        }
    }

    private void getMerchant() {
        merchantConnector.getMerchant(new MerchantConnector.MerchantCallback<Merchant>() {
            @Override
            public void onServiceSuccess(Merchant result, ResultStatus status) {
                super.onServiceSuccess(result, status);
                LookAtMePrefernece.getInstance().setMerchantId(result.getId());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hitApiRequest(ApiConstants.REQUEST_GET_CASH_EVENTS, true);
                    }
                }, 500);

            }

            @Override
            public void onServiceFailure(ResultStatus status) {
                super.onServiceFailure(status);
                Log.d("CASH MANAGER", "get merchant bind failure");
            }

            @Override
            public void onServiceConnectionFailure() {
                super.onServiceConnectionFailure();
                Log.d("CASH MANAGER", "get merchant bind failure");
            }
        });
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {

            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        if (showLoader) {
            ((BaseActivity) getActivity()).showProgressDialog();
        }
        switch (reqType) {
            case ApiConstants.REQUEST_GET_CASH_EVENTS:
                url = LookAtMePrefernece.getInstance().getBaseUrl() +
                        String.format(ApiConstants.URL_CASH_EVENTS,
                                LookAtMePrefernece.getInstance().getMerchantId(),
                                LookAtMePrefernece.getInstance().getDeviceId(),
                                LookAtMePrefernece.getInstance().getAuthToken(),
                                LookAtMePrefernece.getInstance().getFloatTime() + "");
                className = CashEventResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                }, getParams(ApiConstants.REQUEST_GET_CASH_EVENTS));
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                //super.hitApiRequest(reqType, showLoader);
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_GET_CASH_EVENTS) {

        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            ((BaseActivity) getActivity()).removeProgressDialog();
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_CASH_EVENTS:
                    CashEventResponse cashEventResponse = (CashEventResponse) responseObject;
                    if (cashEventResponse != null && cashEventResponse.getCashEventDataList() != null) {
                        getTotalOfCashEvents(cashEventResponse.getCashEventDataList());
                        /*originalAllData = new ArrayList<>();
                        originalAllData.addAll(cashEventResponse.getCashEventDataList());
                        setAdapter(cashEventResponse.getCashEventDataList());*/
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void getTotalOfCashEvents(List<CashEventData> cashEventDataList) {
        netSales = 0;
        if (cashEventDataList != null) {
            for (CashEventData temp : cashEventDataList) {
                netSales = netSales + temp.getAmountChange();
            }

            netSales = netSales - LookAtMePrefernece.getInstance().getMakeDropAmount();

            ((TextView) mView.findViewById(R.id.txtTotalCash)).setTag((netSales + floatTotal));
            ((TextView) mView.findViewById(R.id.txtTotalCash)).setText("$" + dollarformat.format((float) (netSales + floatTotal) / 100) + "");
            ((TextView) mView.findViewById(R.id.txtNetSales)).setText("$" + dollarformat.format((float) netSales / 100) + "");

            updateDifference();
        }
    }


    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void addCurrentyToLayout() {
        LinearLayout lnrCurreny = (LinearLayout) mView.findViewById(R.id.lnrCurreny);

        int pos = lnrCurreny.getChildCount();
        if (mDenominations == null) {
            return;
        }
        if (mDenominations.size() == pos) {
            return;
        }

        final View rowView = mInflater.inflate(R.layout.layout_currency, null);

        if (mDenominations.get(pos).getDenominationValue() < 100) {
            ((TextView) rowView.findViewById(R.id.txtCurrencyCent)).setText(mDenominations.get(pos).getDenominationValue() + "\u00A2");
            ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setTag(pos);
            ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setText("0" + "");
            ((TextView) rowView.findViewById(R.id.txtCurrencyCent)).setVisibility(View.VISIBLE);
            ((TextView) rowView.findViewById(R.id.txtCurrencyNote)).setVisibility(View.GONE);
        } else {
            ((TextView) rowView.findViewById(R.id.txtCurrencyNote)).setText("$" + mDenominations.get(pos).getDenominationValue() / 100 + "");
            ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setTag(pos);
            ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setText("0" + "");
            ((TextView) rowView.findViewById(R.id.txtCurrencyCent)).setVisibility(View.GONE);
            ((TextView) rowView.findViewById(R.id.txtCurrencyNote)).setVisibility(View.VISIBLE);
        }

        rowView.findViewById(R.id.txtCurrencyValue).setTag(0.0f);
        rowView.findViewById(R.id.txtCurrencyNote).setTag(mDenominations.get(pos).getDenominationValue());
        rowView.findViewById(R.id.edtCurencyCount).setTag(pos);
        ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setOnFocusChangeListener(this);
        ((EditText) rowView.findViewById(R.id.edtCurencyCount)).requestFocus();
        lnrCurreny.addView(rowView);

        addDenomination(pos);
    }


    @Subscribe
    public void saveCheckBalance(SaveCheckBalanceEvent saveCheckBalanceEvent) throws Exception {
        if (saveCheckBalanceEvent.isSave() == false) {
            ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
            addCurrentyToLayout();
            updateDifference();
            return;
        }
        CountHistory countHistory = new CountHistory();
        countHistory.setCountDate(new Date(System.currentTimeMillis()));
        countHistory.setCountBy(LookAtMePrefernece.getInstance().getActiveUsername());
        countHistory.setCountRecordedBy(LookAtMePrefernece.getInstance().getActiveUsername());
        if (mView.findViewById(txtDifference).getTag() instanceof Integer) {
            countHistory.setUserCountValue(currencyTotal);
            countHistory.setSystemCountValue((int) mView.findViewById(R.id.txtTotalCash).getTag());
            countHistory.setCountType(AppConstants.COUNT_TYPE_CHECK_BALANCE);
            countHistory.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
            long id = appDatabase.countHistoryDao().insertAll(countHistory);
            countHistory.setCountId((int) id);

            List<CountDetails> floatDetailList = new ArrayList<>();
            for (Denominations temp : mDenominations) {
                CountDetails countDetails = new CountDetails();
                countDetails.setDenominationQuantity(temp.getTempCount());
                temp.setDefaultQuantity(temp.getTempCount());
                countDetails.setDenominations(temp);
                countDetails.setCountId(countHistory.getCountId());
                countDetails.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
                floatDetailList.add(countDetails);
            }
            appDatabase.countDetailDao().insertAll(floatDetailList);

            //after successfully save
            ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
            addCurrentyToLayout();
            updateDifference();
            AlertDialogUtils.showAlertDialog(getActivity(), "Cash Manager", "Check Balance successfully saved.", new AlertDialogUtils.OnButtonClickListener() {
                @Override
                public void onButtonClick(int buttonId) {
                    getActivity().finish();
                }
            });
        } else {
            // ToastUtils.showToast(getActivity(), "Unable to save check balance");

            AlertDialogUtils.showAlertDialog(getActivity(), "Cash Manager", "Unable to save check balance", new AlertDialogUtils.OnButtonClickListener() {
                @Override
                public void onButtonClick(int buttonId) {
                    getActivity().finish();
                }
            });
        }
    }

    @Subscribe
    public void addCurrency(CurrencyEvent currencyEvent) {
        //NEXT
        if (currencyEvent.getNum() == -1) {
            addCurrentyToLayout();
        } else if (currencyEvent.getNum() == -2) {//BACKSPACE
            String temp = focusedEditText.getText().toString();
            if (StringUtils.isNullOrEmpty(temp)) {
                return;
            }

            focusedEditText.setText(temp.substring(0, temp.length() - 1));
            focusedEditText.setSelection(temp.length() - 1);

            if (focusedEditText.getText().toString().equals("")) {
                //addDenomination((int) focusedEditText.getTag());
            } else {
                addDenomination((int) focusedEditText.getTag());
            }
        } else {
            if (focusedEditText.getText().toString().equals("0")) {
                focusedEditText.setText("");
            }
            String temp = focusedEditText.getText().toString();
            temp = temp + currencyEvent.getNum();
            focusedEditText.setText(temp + "");
            focusedEditText.setSelection(temp.length());

            addDenomination((int) focusedEditText.getTag());
        }

    }


    private void addDenomination(int pos) {
        LinearLayout lnrCurreny = (LinearLayout) mView.findViewById(R.id.lnrCurreny);
        View view = lnrCurreny.getChildAt(pos);
        EditText edtCurencyCount = (EditText) view.findViewById(R.id.edtCurencyCount);
        int currencyAmount = (int) view.findViewById(R.id.txtCurrencyNote).getTag() * Integer.parseInt(edtCurencyCount.getText().toString().equals("") ? "0" : edtCurencyCount.getText().toString());
        int currencyCount = Integer.parseInt(edtCurencyCount.getText().toString().equals("") ? "0" : edtCurencyCount.getText().toString());
        mDenominations.get(pos).setTempCount(currencyCount);
        ((TextView) view.findViewById(R.id.txtCurrencyValue)).setText("$" + dollarformat.format((float) currencyAmount / 100) + "");
        view.findViewById(R.id.txtCurrencyValue).setTag(currencyAmount);
        //updateTotalFloat();
        updateDifference();

        String quantity = ((EditText) view.findViewById(R.id.edtCurencyCount)).getText().toString();

        String initialmessage = "";
        denominationposition = pos;

        /*if (pos == 0) {
            initialmessage = AppConstants.CURRENCY_CHECKBALANCE_MESSAGE_1;
            String message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName() + "");
            EventBus.getDefault().post(new MessageEvent(message));
        } else if (mDenominations != null && pos == mDenominations.size() - 1) {
            if (currencyTotal > 0) {
                EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CHECKBALANCE_UNMATCH));
            } else {
                EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CHECKBALANCE_MATCH));
            }
        } else {
            initialmessage = AppConstants.CURRENCY_CHECKBALANCE_MESSAGE_MIDDLE;
            /*if (mDenominations.get(pos).getDenominationValue() > 100) {
                message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName(), "bills" + "");
            } else {
                message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName(), "coins" + "");
            }
            message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName(), "" + "");
            EventBus.getDefault().post(new MessageEvent(message));
        }*/

        if (pos == 0 && ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() != (mDenominations.size())) {
            initialmessage = AppConstants.CURRENCY_CHECKBALANCE_MESSAGE_1;
            String message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName() + "");
            EventBus.getDefault().post(new MessageEvent(message));
        }

        if (pos != 0 && pos < mDenominations.size() - 1 && ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() != (mDenominations.size())) {
            initialmessage = AppConstants.CURRENCY_CHECKBALANCE_MESSAGE_MIDDLE;
            //String message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName() + "");
            if (mDenominations.get(pos).getDenominationValue() > 100) {
                message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName(), "bills" + "");
            } else {
                message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName(), "coins" + "");
            }
            EventBus.getDefault().post(new MessageEvent(message));
        }

        if (((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() == (mDenominations.size())) {
            if (checkbalancedifference == 0) {
                EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CHECKBALANCE_MATCH));
            } else {
                EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CHECKBALANCE_UNMATCH));
            }
        }

        /*if (currencyTotal > 0) {
            if (checkbalancedifference == 0) {
                EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CHECKBALANCE_MATCH));
            } else {
                EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CHECKBALANCE_UNMATCH));
            }
        }*/


    }

    private void updateDifference() {
        LinearLayout lnrCurreny = (LinearLayout) mView.findViewById(R.id.lnrCurreny);
        currencyTotal = 0;
        for (int i = 0; i <= lnrCurreny.getChildCount() - 1; i++) {
            View view = lnrCurreny.getChildAt(i);
            int currencyAmount = (int) view.findViewById(R.id.txtCurrencyValue).getTag();
            currencyTotal = currencyTotal + currencyAmount;
        }

        float differencevalue = ((float) ((netSales + floatTotal) - currencyTotal)) / 100;
        checkbalancedifference = ((netSales + floatTotal) - currencyTotal);

        if (differencevalue > 0) {
            ((TextView) mView.findViewById(R.id.txtDifference)).setTextColor(getResources().getColor(R.color.red));
        } else {
            ((TextView) mView.findViewById(R.id.txtDifference)).setTextColor(getResources().getColor(R.color.black));
        }
        ((TextView) mView.findViewById(txtDifference)).setTag(((netSales + floatTotal) - currencyTotal));
        ((TextView) mView.findViewById(txtDifference)).setText("$" + dollarformat.format((float) (netSales + floatTotal - currencyTotal) / 100) + "");

        if (mDenominations != null && mDenominations.size() - 1 == lnrCurreny.getChildCount() - 1) {
            if (differencevalue > 0) {
                EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CHECKBALANCE_UNMATCH));
            } else {
                EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CHECKBALANCE_MATCH));
            }
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v instanceof EditText && hasFocus) {
            focusedEditText = (EditText) v;
        }
    }
}
