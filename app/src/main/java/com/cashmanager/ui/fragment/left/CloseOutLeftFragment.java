package com.cashmanager.ui.fragment.left;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cashmanager.constants.ApiConstants;
import com.cashmanager.constants.AppConstants;
import com.cashmanager.constants.AppDatabase;
import com.cashmanager.listener.CLoseoutUpdateMiddleUIEvent;
import com.cashmanager.listener.CurrencyEvent;
import com.cashmanager.listener.MessageEvent;
import com.cashmanager.listener.SaveCloseOutCBEvent;
import com.cashmanager.listener.SaveCloseOutMDEvent;
import com.cashmanager.listener.SaveCloseOutSFEvent;
import com.cashmanager.model.CashEventData;
import com.cashmanager.model.response.CashEventResponse;
import com.cashmanager.pojo.CountDetails;
import com.cashmanager.pojo.CountHistory;
import com.cashmanager.pojo.Denominations;
import com.cashmanager.pojo.FloatDetails;
import com.cashmanager.pojo.FloatHistory;
import com.cashmanager.util.LookAtMePrefernece;
import com.cashmanager1.R;
import com.clover.sdk.v1.ResultStatus;
import com.clover.sdk.v1.merchant.Merchant;
import com.clover.sdk.v1.merchant.MerchantConnector;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.cashmanager1.R.id.txtCounted;
import static com.cashmanager1.R.id.txtDifference;
import static com.cashmanager1.R.id.txtFloatamount;

/**
 * Created by Dinesh Adhikari on 25/06/17.
 */
public class CloseOutLeftFragment extends BaseFragment implements View.OnFocusChangeListener {

    private View mView;
    private LayoutInflater mInflater;
    private EditText focusedEditText;
    private AppDatabase appDatabase;
    private List<Denominations> mDenominations;
    private String url;
    private int totalcash = 0;
    private int floatTotal = 0;
    private int netSales = 0;
    int currencyTotal = 0;
    int dropAmount = 0;

    int checkbalancedifference = 0;
    int dropdifference = 0;
    int closeout_phase = 1;  //1= check balance, 2 = makedrop, 3 = set float.

    // set float
    int denominationposition = 0;
    float floatdifference = 0;

    DecimalFormat dollarformat = new DecimalFormat("#0.00");
    private String message;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_close_out_left, null);
        mInflater = LayoutInflater.from(getActivity().getBaseContext());

        appDatabase = BaseApplication.getAppDatabase(getActivity());
        LiveData<List<Denominations>> denominations = appDatabase.denominationDao().getAllDenominations();
        denominations.observe(this, new Observer<List<Denominations>>() {
            @Override
            public void onChanged(@Nullable List<Denominations> denominations) {
                mDenominations = denominations;
                addCurrentyToLayout();

                FloatHistory latestfloat = appDatabase.floatHistoryDao().getLatestFloatHistory();
                if (latestfloat != null) {
                    ((TextView) mView.findViewById(R.id.txtFloat)).setText("$" + dollarformat.format((float) latestfloat.getFloatValue() / 100));
                    floatTotal = latestfloat.getFloatValue();
                }
            }
        });


        return mView;
    }

    public int getCurrencyTotal() {
        return currencyTotal;
    }

    public int getDenominationposition() {
        return denominationposition;
    }

    public float getFloatdifference() {
        return floatdifference;
    }

    public LinearLayout getLinearCurrency() {
        return (LinearLayout) mView.findViewById(R.id.lnrCurreny);
    }

    public List<Denominations> getmDenominations() {
        return mDenominations;
    }

    public int getdropdifference() {
        return dropdifference;
    }

    public int getDropAmount() {
        return dropAmount;
    }

    public int getCheckbalancedifference() {
        return checkbalancedifference;
    }

    @Override
    public void onResume() {

        closeout_phase = 1;

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();

        try {
            if (!StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getMerchantId())
                    && !StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getDeviceId())
                    && !StringUtils.isNullOrEmpty(LookAtMePrefernece.getInstance().getAuthToken())) {
                hitApiRequest(ApiConstants.REQUEST_GET_CASH_EVENTS, true);
            } else {
                getMerchant();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    private void getMerchant() {
        merchantConnector.getMerchant(new MerchantConnector.MerchantCallback<Merchant>() {
            @Override
            public void onServiceSuccess(Merchant result, ResultStatus status) {
                super.onServiceSuccess(result, status);
                LookAtMePrefernece.getInstance().setMerchantId(result.getId());
                LookAtMePrefernece.getInstance().setDeviceId(result.getDeviceId());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hitApiRequest(ApiConstants.REQUEST_GET_CASH_EVENTS, true);
                    }
                }, 500);

            }

            @Override
            public void onServiceFailure(ResultStatus status) {
                super.onServiceFailure(status);
                Log.d("CASH MANAGER", "get merchant bind failure");
            }

            @Override
            public void onServiceConnectionFailure() {
                super.onServiceConnectionFailure();
                Log.d("CASH MANAGER", "get merchant bind failure");
            }
        });
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {

            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        if (showLoader) {
            ((BaseActivity) getActivity()).showProgressDialog();
        }

        switch (reqType) {
            case ApiConstants.REQUEST_GET_CASH_EVENTS:
                url = LookAtMePrefernece.getInstance().getBaseUrl() +
                        String.format(ApiConstants.URL_CASH_EVENTS,
                                LookAtMePrefernece.getInstance().getMerchantId(),
                                LookAtMePrefernece.getInstance().getDeviceId(),
                                LookAtMePrefernece.getInstance().getAuthToken(),
                                LookAtMePrefernece.getInstance().getFloatTime() + "");
                className = CashEventResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                }, getParams(ApiConstants.REQUEST_GET_CASH_EVENTS));
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                //super.hitApiRequest(reqType, showLoader);
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_GET_CASH_EVENTS) {

        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            ((BaseActivity) getActivity()).removeProgressDialog();
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_CASH_EVENTS:
                    CashEventResponse cashEventResponse = (CashEventResponse) responseObject;
                    if (cashEventResponse != null && cashEventResponse.getCashEventDataList() != null) {
                        getTotalOfCashEvents(cashEventResponse.getCashEventDataList());
                        /*originalAllData = new ArrayList<>();
                        originalAllData.addAll(cashEventResponse.getCashEventDataList());
                        setAdapter(cashEventResponse.getCashEventDataList());*/
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void getTotalOfCashEvents(List<CashEventData> cashEventDataList) {
        netSales = 0;
        if (cashEventDataList != null) {
            for (CashEventData temp : cashEventDataList) {
                netSales = netSales + temp.getAmountChange();
            }

            totalcash = netSales + floatTotal - LookAtMePrefernece.getInstance().getMakeDropAmount();

            ((TextView) mView.findViewById(R.id.txtTotalCash)).setTag(totalcash);
            ((TextView) mView.findViewById(R.id.txtTotalCash)).setText("$" + dollarformat.format((float) totalcash / 100) + "");
            ((TextView) mView.findViewById(R.id.txtNetSales)).setText("$" + dollarformat.format((float) (netSales - LookAtMePrefernece.getInstance().getMakeDropAmount()) / 100) + "");

            ((TextView) mView.findViewById(R.id.txtTotalCashvalue)).setText("$" + dollarformat.format((float) totalcash / 100) + "");

            dropAmount = netSales - LookAtMePrefernece.getInstance().getMakeDropAmount();

            updateCountandDifference();
        }
    }

    private void addCurrentyToLayout() {
        LinearLayout lnrCurreny = (LinearLayout) mView.findViewById(R.id.lnrCurreny);

        int pos = lnrCurreny.getChildCount();
        if (mDenominations == null) {
            return;
        }

        if (pos > mDenominations.size() - 1) {
            if (closeout_phase == 1) // check balance
            {
                if (((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() == (mDenominations.size())) {
                    if (checkbalancedifference == 0) {
                        EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CLOSEOUT_CB_MATCH));
                    } else {
                        EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CLOSEOUT_CB_UNMATCH));
                    }
                }
            } else if (closeout_phase == 2)//make drop
            {

            } else if (closeout_phase == 3) {

            }
            return;
        }

        final View rowView = mInflater.inflate(R.layout.layout_currency, null);

        if (mDenominations.get(pos).getDenominationValue() < 100) {
            ((TextView) rowView.findViewById(R.id.txtCurrencyCent)).setText(mDenominations.get(pos).getDenominationValue() + "\u00A2");
            ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setTag(pos);
            ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setText(0 + "");
            ((TextView) rowView.findViewById(R.id.txtCurrencyCent)).setVisibility(View.VISIBLE);
            ((TextView) rowView.findViewById(R.id.txtCurrencyNote)).setVisibility(View.GONE);
        } else {
            ((TextView) rowView.findViewById(R.id.txtCurrencyNote)).setText("$" + mDenominations.get(pos).getDenominationValue() / 100 + "");
            ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setTag(pos);
            ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setText(0 + "");
            ((TextView) rowView.findViewById(R.id.txtCurrencyCent)).setVisibility(View.GONE);
            ((TextView) rowView.findViewById(R.id.txtCurrencyNote)).setVisibility(View.VISIBLE);
        }

        rowView.findViewById(R.id.txtCurrencyValue).setTag(0.0f);
        rowView.findViewById(R.id.txtCurrencyNote).setTag(mDenominations.get(pos).getDenominationValue());
        rowView.findViewById(R.id.edtCurencyCount).setTag(pos);
        ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setOnFocusChangeListener(this);
        ((EditText) rowView.findViewById(R.id.edtCurencyCount)).requestFocus();
        lnrCurreny.addView(rowView);

        if (closeout_phase == 1) // check balance
        {
            ((LinearLayout) mView.findViewById(R.id.li_checkbalance)).setVisibility(View.VISIBLE);
            ((LinearLayout) mView.findViewById(R.id.li_dropamount)).setVisibility(View.GONE);
            ((RelativeLayout) mView.findViewById(R.id.rtlDiffernce)).setVisibility(View.VISIBLE);
            ((LinearLayout) mView.findViewById(R.id.lilfloatset)).setVisibility(View.GONE);

            ((TextView) mView.findViewById(R.id.txtTotalCashvalue)).setText("$" + dollarformat.format((float) totalcash / 100) + "");

        } else if (closeout_phase == 2) // make drop
        {
            ((LinearLayout) mView.findViewById(R.id.li_checkbalance)).setVisibility(View.GONE);
            ((LinearLayout) mView.findViewById(R.id.li_dropamount)).setVisibility(View.VISIBLE);
            ((RelativeLayout) mView.findViewById(R.id.rtlDiffernce)).setVisibility(View.VISIBLE);
            ((LinearLayout) mView.findViewById(R.id.lilfloatset)).setVisibility(View.GONE);

            ((TextView) mView.findViewById(R.id.txt1)).setText("Cash on Hand:");
            ((TextView) mView.findViewById(R.id.txtTotalCash)).setText("$" + dollarformat.format((float) (totalcash - checkbalancedifference) / 100) + "");

            ((TextView) mView.findViewById(R.id.txtDropAmount)).setText("$" + dollarformat.format((float) (netSales - LookAtMePrefernece.getInstance().getMakeDropAmount() - checkbalancedifference) / 100) + "");

            dropAmount = netSales - LookAtMePrefernece.getInstance().getMakeDropAmount() - checkbalancedifference;


        } else if (closeout_phase == 3) // cset float
        {
            ((LinearLayout) mView.findViewById(R.id.li_checkbalance)).setVisibility(View.GONE);
            ((LinearLayout) mView.findViewById(R.id.li_dropamount)).setVisibility(View.GONE);
            ((RelativeLayout) mView.findViewById(R.id.rtlDiffernce)).setVisibility(View.GONE);
            ((LinearLayout) mView.findViewById(R.id.lilfloatset)).setVisibility(View.VISIBLE);

            ((RelativeLayout) mView.findViewById(R.id.rtlSuggestedTotal)).setVisibility(View.INVISIBLE);
            addSuggestedTotal();
        }

        addDenomination(pos);
    }

  /*  @Subscribe
    public void addDropAmount(MakeDropEvent makeDropEvent) {
        //NEXT
        if (makeDropEvent.getDropAmount() >= 0.0) {

            dropAmount = makeDropEvent.getDropAmount();
            ((TextView) mView.findViewById(R.id.txtDropAmount)).setText("$" + dollarformat.format(makeDropEvent.getDropAmount()));

            //updateCountandDifference();
            addDenomination((int) focusedEditText.getTag());
        }
    }*/

    @Subscribe
    public void saveCheckBalance(SaveCloseOutCBEvent saveCloseOutEvent) throws Exception {
        if (saveCloseOutEvent.isSave() == false) {
            ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
            addCurrentyToLayout();
            //updateDifference();

            // update ui in middle fragment
            EventBus.getDefault().post(new CLoseoutUpdateMiddleUIEvent(closeout_phase));

            return;
        }

        CountHistory countHistory = new CountHistory();
        countHistory.setCountDate(new Date(System.currentTimeMillis()));
        countHistory.setCountBy(LookAtMePrefernece.getInstance().getActiveUsername());
        countHistory.setCountRecordedBy(LookAtMePrefernece.getInstance().getActiveUsername());
        if (mView.findViewById(txtDifference).getTag() instanceof Integer) {
            countHistory.setUserCountValue(currencyTotal);
            countHistory.setSystemCountValue((int) mView.findViewById(R.id.txtTotalCash).getTag());
            countHistory.setCountType(AppConstants.COUNT_TYPE_CHECK_BALANCE);
            countHistory.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
            long id = appDatabase.countHistoryDao().insertAll(countHistory);
            countHistory.setCountId((int) id);

            List<CountDetails> floatDetailList = new ArrayList<>();
            for (Denominations temp : mDenominations) {
                CountDetails countDetails = new CountDetails();
                countDetails.setDenominationQuantity(temp.getTempCount());
                temp.setDefaultQuantity(temp.getTempCount());
                countDetails.setDenominations(temp);
                countDetails.setCountId(countHistory.getCountId());
                countDetails.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
                floatDetailList.add(countDetails);
            }

            appDatabase.countDetailDao().insertAll(floatDetailList);

            AlertDialogUtils.showAlertDialog(getActivity(), "Cash Manager", "Closeout Balance check is successfully saved.", new AlertDialogUtils.OnButtonClickListener() {
                @Override
                public void onButtonClick(int buttonId) {
                    //getActivity().finish();

                    //after successfully save
                    // change UI for make drop
                    closeout_phase = 2;
                    ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
                    addCurrentyToLayout();

                }
            });
        } else {
            AlertDialogUtils.showAlertDialog(getActivity(), "Cash Manager", "Unable to save balance check", new AlertDialogUtils.OnButtonClickListener() {
                @Override
                public void onButtonClick(int buttonId) {
                    //getActivity().finish();
                }
            });
        }
    }

    @Subscribe
    public void saveMakeDrop(SaveCloseOutMDEvent saveMakeDropEvent) throws Exception {

        if (saveMakeDropEvent.isSave() == false) {

            if (saveMakeDropEvent.getFlag() == 3) {
                closeout_phase = 3;
                ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
                ((LinearLayout) mView.findViewById(R.id.li_topfieldlayout)).setVisibility(View.GONE);

                addCurrentyToLayout();
                return;
            } else {
                ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
                // addCurrentyToLayout();
                dropAmount = 0;
                ((TextView) mView.findViewById(R.id.txtDropAmount)).setText("$0.00");
                updateCountandDifference();
                return;
            }

        }

        CountHistory countHistory = new CountHistory();
        countHistory.setCountDate(new Date(System.currentTimeMillis()));
        countHistory.setCountBy(LookAtMePrefernece.getInstance().getActiveUsername());
        countHistory.setCountRecordedBy(LookAtMePrefernece.getInstance().getActiveUsername());
        countHistory.setCountRecordedBy(LookAtMePrefernece.getInstance().getActiveUsername());
        if (mView.findViewById(txtDifference).getTag() instanceof Integer) {
            countHistory.setUserCountValue(currencyTotal);
            countHistory.setSystemCountValue((int) mView.findViewById(R.id.txtTotalCash).getTag());
            countHistory.setCountType(AppConstants.COUNT_TYPE_MAKE_DROP);
            countHistory.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
            long id = appDatabase.countHistoryDao().insertAll(countHistory);
            countHistory.setCountId((int) id);

            List<CountDetails> floatDetailList = new ArrayList<>();
            for (Denominations temp : mDenominations) {
                CountDetails countDetails = new CountDetails();
                countDetails.setDenominationQuantity(temp.getTempCount());
                temp.setDefaultQuantity(temp.getTempCount());
                countDetails.setDenominations(temp);
                countDetails.setCountId(countHistory.getCountId());
                countDetails.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
                floatDetailList.add(countDetails);
            }

            appDatabase.countDetailDao().insertAll(floatDetailList);

            /*//after successfully save
            ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
            addCurrentyToLayout();
            updateCountandDifference();*/

            AlertDialogUtils.showAlertDialog(getActivity(), "Cash Manager", "Closeout Drop is successfully saved.", new AlertDialogUtils.OnButtonClickListener() {
                @Override
                public void onButtonClick(int buttonId) {
                    //getActivity().finish();

                    //after successfully save
                    // change UI for make drop
                    LookAtMePrefernece.getInstance().setMakeDropAmount(LookAtMePrefernece.getInstance().getMakeDropAmount() + dropAmount);
                    closeout_phase = 3;
                    ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
                    ((LinearLayout) mView.findViewById(R.id.li_topfieldlayout)).setVisibility(View.GONE);

                    addCurrentyToLayout();
                }
            });

        } else {
            AlertDialogUtils.showAlertDialog(getActivity(), "Cash Manager", "Unable to save Closeout Drop", new AlertDialogUtils.OnButtonClickListener() {
                @Override
                public void onButtonClick(int buttonId) {
                    //getActivity().finish();
                }
            });
        }
    }

    @Subscribe
    public void saveFloat(SaveCloseOutSFEvent saveSetFloatEvent) {

        if (saveSetFloatEvent.isSave() == false) {
            ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
            addCurrentyToLayout();

            mView.findViewById(R.id.rtlSuggestedTotal).setVisibility(View.GONE);
            ((TextView) mView.findViewById(R.id.txtFloatamount)).setTextColor(getResources().getColor(R.color.black));
            return;
        }

        /*we have to save data to server and then save to our local data base.
        * If internet connection is not available the we will save data but sync will be false and
        * if we press sync button then we have syn with server and chnage the primary key ids with that.
        * and do sync true*/

        FloatHistory floatHistory = new FloatHistory();
        floatHistory.setFloatDate(new Date(System.currentTimeMillis()));
        floatHistory.setFloatSetBy(LookAtMePrefernece.getInstance().getActiveUsername());
        floatHistory.setSynced(false);
        floatHistory.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
        if (mView.findViewById(R.id.txtFloatamount).getTag() instanceof Integer) {
            floatHistory.setFloatValue((int) mView.findViewById(R.id.txtFloatamount).getTag());

            try {
                long id = appDatabase.floatHistoryDao().insertAll(floatHistory);
                Log.d("Saved set float id : ", id + "");
                floatHistory.setFloatId((int) id);

                List<FloatDetails> floatDetailList = new ArrayList<>();
                for (Denominations temp : mDenominations) {
                    FloatDetails floatDetails = new FloatDetails();
                    floatDetails.setDenominationQuantity(temp.getTempCount());
                    temp.setDefaultQuantity(temp.getTempCount());
                    floatDetails.setDenominations(temp);
                    floatDetails.setFloatId(floatHistory.getFloatId());
                    floatDetails.setSynced(false);
                    floatDetails.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
                    floatDetailList.add(floatDetails);
                }
                appDatabase.floatDetailDao().insertAll(floatDetailList);

                //after successfully save
                ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
                addCurrentyToLayout();
                LookAtMePrefernece.getInstance().setFloatTime(new Date(System.currentTimeMillis()));
                LookAtMePrefernece.getInstance().setMakeDropAmount(0);
                LookAtMePrefernece.getInstance().setFloatDisabled(false);
                AlertDialogUtils.showAlertDialog(getActivity(), "Cash Manager", "Closeout Float set is successfully saved for next shift. CloseOut is completed now.", new AlertDialogUtils.OnButtonClickListener() {
                    @Override
                    public void onButtonClick(int buttonId) {
                        getActivity().finish();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AlertDialogUtils.showAlertDialog(getActivity(), "Cash Manager", "Unable to Set Float.", new AlertDialogUtils.OnButtonClickListener() {
                @Override
                public void onButtonClick(int buttonId) {
                    //getActivity().finish();
                }
            });
        }

    }


    private void addSuggestedTotal() {
        List<Denominations> mDenominations = appDatabase.denominationDao().getAllDenominationFromDatabase();

        int suggestedTotal = 0;
        for (int i = 0; i < mDenominations.size(); i++) {
            int currencyQuantity = mDenominations.get(i).getDefaultQuantity();
            int currencyValue = mDenominations.get(i).getDenominationValue();
            int total_cal = currencyValue * currencyQuantity;
            suggestedTotal = suggestedTotal + total_cal;
        }

        ((TextView) mView.findViewById(R.id.txtSuggestedTotal)).setText("$" + dollarformat.format((float) suggestedTotal / 100) + "");
        ((TextView) mView.findViewById(R.id.txtSuggestedTotal)).setTag(suggestedTotal);
    }

    @Subscribe
    public void addCurrency(CurrencyEvent currencyEvent) {
        //NEXT
        if (currencyEvent.getNum() == -1) {
            addCurrentyToLayout();
        } else if (currencyEvent.getNum() == -2) {//BACKSPACE
            String temp = focusedEditText.getText().toString();
            if (StringUtils.isNullOrEmpty(temp)) {
                return;
            }
            if (focusedEditText.getText().equals("0")) {
                focusedEditText.setText("");
            }
            focusedEditText.setText(temp.substring(0, temp.length() - 1));
            focusedEditText.setSelection(temp.length() - 1);

            if (focusedEditText.getText().toString().equals("")) {
                //addDenomination((int) focusedEditText.getTag());
            } else {
                addDenomination((int) focusedEditText.getTag());
            }
        } else {
            if (focusedEditText.getText().toString().equals("0")) {
                focusedEditText.setText("");
            }
            String temp = focusedEditText.getText().toString();
            temp = temp + currencyEvent.getNum();
            focusedEditText.setText(temp + "");
            focusedEditText.setSelection(temp.length());

            addDenomination((int) focusedEditText.getTag());
        }
    }


    private void addDenomination(int pos) {

        LinearLayout lnrCurreny = (LinearLayout) mView.findViewById(R.id.lnrCurreny);
        View view = lnrCurreny.getChildAt(pos);
        EditText edtCurencyCount = (EditText) view.findViewById(R.id.edtCurencyCount);
        int currencyAmount = (int) view.findViewById(R.id.txtCurrencyNote).getTag() * Integer.parseInt(edtCurencyCount.getText().toString().equals("") ? "0" : edtCurencyCount.getText().toString());

        ((TextView) view.findViewById(R.id.txtCurrencyValue)).setText("$" + dollarformat.format((float) currencyAmount / 100) + "");
        view.findViewById(R.id.txtCurrencyValue).setTag(currencyAmount);

        updateCountandDifference();

        String quantity = ((EditText) view.findViewById(R.id.edtCurencyCount)).getText().toString();

        String initialmessage = "";

        if (mDenominations != null) {

            if (closeout_phase == 1) // check balance
            {
                if (pos == 0 && ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() != (mDenominations.size())) {
                    initialmessage = AppConstants.CURRENCY_CLOSEOUT_CB_1;
                    String message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName() + "");
                    EventBus.getDefault().post(new MessageEvent(message));
                }

                if (pos != 0 && pos <= mDenominations.size() - 1) {
                    initialmessage = AppConstants.CURRENCY_CLOSEOUT_CB_2;
                    String message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName() + "");
                    EventBus.getDefault().post(new MessageEvent(message));
                }

                /*if (((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() == (mDenominations.size())) {
                    if (checkbalancedifference == 0) {
                        EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CLOSEOUT_CB_MATCH));
                    } else {
                        EventBus.getDefault().post(new MessageEvent(AppConstants.CURRENCY_CLOSEOUT_CB_UNMATCH));
                    }
                }*/

            } else if (closeout_phase == 2) // make drop
            {
                int difference = currencyTotal - dropAmount;

                if (currencyTotal > 0 && (difference) != 0) {
                    initialmessage = AppConstants.CURRENCY_CLOSEOUT_MD_1;
                    String message = String.format(initialmessage, "", "" + mDenominations.get(pos).getDenominationName() + "");
                    EventBus.getDefault().post(new MessageEvent(message));
                } else if (currencyTotal > 0 && difference == 0) {
                    initialmessage = AppConstants.CURRENCY_CLOSEOUT_MD_PRINTRECEIPT;
                    String message = String.format(initialmessage, "" + mDenominations.get(pos).getDenominationName() + "");
                    EventBus.getDefault().post(new MessageEvent(message));
                } else {
                    initialmessage = AppConstants.CURRENCY_CLOSEOUT_MD_1;
                    String message = String.format(initialmessage, "", "" + mDenominations.get(pos).getDenominationName() + "");
                    EventBus.getDefault().post(new MessageEvent(message));
                }

            } else if (closeout_phase == 3) // set float
            {

                if (pos == mDenominations.size() - 1) {
                    addSuggestedTotal();
                    mView.findViewById(R.id.rtlSuggestedTotal).setVisibility(View.VISIBLE);
                }

                denominationposition = pos;

                int totalfloatvalue = (int) ((TextView) mView.findViewById(R.id.txtFloatamount)).getTag();
                int totalsugestedvalue = (int) ((TextView) mView.findViewById(R.id.txtSuggestedTotal)).getTag();

                if (((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() != mDenominations.size()) {
                    initialmessage = AppConstants.CURRENCY_CLOSEOUT_SETFLOAT;
                }

                floatdifference = (float) (totalfloatvalue - totalsugestedvalue) / 100;

                if (floatdifference == 0) {
                    initialmessage = AppConstants.CURRENCY_FLOAT_MESSAGE_FINAL;
                    ((TextView) mView.findViewById(R.id.txtFloatamount)).setTextColor(getResources().getColor(R.color.black));
                }

                if (floatdifference != 0 && ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() == mDenominations.size()) {

                    floatdifference = (float) (totalfloatvalue - totalsugestedvalue) / 100;

                    ((TextView) mView.findViewById(R.id.txtFloatamount)).setTextColor(getResources().getColor(R.color.red));
                    initialmessage = AppConstants.CURRENCY_FLOAT_MESSAGE_DIFFERENCE;
                }

                /*if (mDenominations.get(pos).getDenominationValue() >= 100) {
                    message = String.format(initialmessage, "", "" + mDenominations.get(pos).getDenominationValue() + " dollar bills");
                } else {
                    message = String.format(initialmessage, "", "" + mDenominations.get(pos).getDenominationValue() + " cent coins");
                }*/
                message = String.format(initialmessage, "", "" + mDenominations.get(pos).getDenominationName());

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(new MessageEvent(message));
                    }
                }, 100);
            }
        }

        // update ui in middle fragment
        EventBus.getDefault().post(new CLoseoutUpdateMiddleUIEvent(closeout_phase));
    }

    String getdenomationvalue(int denominationvalue) {
        String value = "";

        float name = ((float) denominationvalue) / 100;

        //value = "$" + dollarformat.format(name);
        value = dollarformat.format(name);

        return value;
    }

    private void updateCountandDifference() {
        LinearLayout lnrCurreny = (LinearLayout) mView.findViewById(R.id.lnrCurreny);
        currencyTotal = 0;

        for (int i = 0; i <= lnrCurreny.getChildCount() - 1; i++) {
            View view = lnrCurreny.getChildAt(i);

            int currencyAmount = (int) view.findViewById(R.id.txtCurrencyValue).getTag();
            currencyTotal = currencyTotal + currencyAmount;
        }


        // total counted amount
        if (closeout_phase == 1) {
            // check balance
            ((TextView) mView.findViewById(R.id.txtCashCounted)).setTag((currencyTotal));
            ((TextView) mView.findViewById(R.id.txtCashCounted)).setText("$" + dollarformat.format((float) currencyTotal / 100) + "");

            // update difference between drop amount and counted amount
            ((TextView) mView.findViewById(txtDifference)).setTag((totalcash - currencyTotal));

            checkbalancedifference = totalcash - currencyTotal;

            ((TextView) mView.findViewById(txtDifference)).setText("$" + dollarformat.format((float) checkbalancedifference / 100) + "");

            if (checkbalancedifference != 0) {
                ((TextView) mView.findViewById(txtDifference)).setTextColor(getResources().getColor(R.color.red));
            } else {
                ((TextView) mView.findViewById(txtDifference)).setTextColor(getResources().getColor(R.color.black));
            }

        } else if (closeout_phase == 2) {
            // make drop
            ((TextView) mView.findViewById(txtCounted)).setTag((currencyTotal));
            ((TextView) mView.findViewById(txtCounted)).setText("$" + dollarformat.format((float) currencyTotal / 100) + "");

            // update difference between drop amount and counted amount
            ((TextView) mView.findViewById(txtDifference)).setTag((dropAmount - currencyTotal));
            ((TextView) mView.findViewById(txtDifference)).setText("$" + dollarformat.format((float) (dropAmount - currencyTotal) / 100) + "");

            dropdifference = dropAmount - currencyTotal;

            if (currencyTotal > 0 && (dropdifference) != 0) {
                ((TextView) mView.findViewById(txtDifference)).setTextColor(getResources().getColor(R.color.red));
            } else if (currencyTotal > 0 && (dropdifference) == 0) {
                ((TextView) mView.findViewById(txtDifference)).setTextColor(getResources().getColor(R.color.black));
            }

            if (dropAmount <= 0) {
                AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Closeout Drop", "It appears there is no extra cash to make a drop. Tap OK to continue to Set Float.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                    @Override
                    public void onButtonClick(int buttonId) {
                        switch (buttonId) {
                            case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                EventBus.getDefault().post(new SaveCloseOutMDEvent(false, 3));
                                break;
                            case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                EventBus.getDefault().post(new SaveCloseOutMDEvent(false, 3));
                                break;
                        }
                    }
                });
            }


        } else if (closeout_phase == 3) {
            // set float

            ((TextView) mView.findViewById(txtFloatamount)).setTag((currencyTotal));
            ((TextView) mView.findViewById(txtFloatamount)).setText("$" + dollarformat.format((float) currencyTotal / 100) + "");

        }

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v instanceof EditText && hasFocus) {
            focusedEditText = (EditText) v;
        }
    }
}
