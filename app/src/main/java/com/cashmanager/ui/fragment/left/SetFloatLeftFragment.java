package com.cashmanager.ui.fragment.left;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cashmanager.constants.AppConstants;
import com.cashmanager.constants.AppDatabase;
import com.cashmanager.listener.CurrencyEvent;
import com.cashmanager.listener.MessageEvent;
import com.cashmanager.listener.SaveSetFloatEvent;
import com.cashmanager.pojo.Denominations;
import com.cashmanager.pojo.FloatDetails;
import com.cashmanager.pojo.FloatHistory;
import com.cashmanager.util.LookAtMePrefernece;
import com.cashmanager1.R;
import com.krapps.application.BaseApplication;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.StringUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinesh Adhikari on 25/04/16.
 */
public class SetFloatLeftFragment extends BaseFragment implements View.OnFocusChangeListener {

    private View mView;
    private LayoutInflater mInflater;
    //float a[] = {50, 20, 10, 1, 0.25f, 0.10f, 0.05f, 0.01f};
    private EditText focusedEditText;
    private AppDatabase appDatabase;
    List<Denominations> mDenominations;
    int floatTotal = 0;
    DecimalFormat dollarformat = new DecimalFormat("#0.00");

    int denominationposition = 0;

    float floatdifference = 0;
    private String message;
    private int totalfloatvalue;
    private int totalsugestedvalue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_set_float_left, null);
        mInflater = LayoutInflater.from(getActivity().getBaseContext());

        appDatabase = BaseApplication.getAppDatabase(getActivity());

        LiveData<List<Denominations>> denominations = appDatabase.denominationDao().getAllDenominations();
        FloatHistory floatHistory = appDatabase.floatHistoryDao().getLatestFloatHistory();

        denominations.observe(this, new Observer<List<Denominations>>() {
            @Override
            public void onChanged(@Nullable List<Denominations> denominations) {
                mDenominations = denominations;
                addSuggestedTotal();
                mView.findViewById(R.id.rtlSuggestedTotal).setVisibility(View.INVISIBLE);
                addCurrentyToLayout();
            }
        });

        return mView;
    }

    public float getCurrencyTotal() {
        return floatTotal;
    }

    public int getDenominationposition() {
        return denominationposition;
    }

    public float getFloatdifference() {
        return floatdifference;
    }

    public LinearLayout getLinearCurrency() {
        return (LinearLayout) mView.findViewById(R.id.lnrCurreny);
    }

    public List<Denominations> getmDenominations() {
        return mDenominations;
    }

    private void addSuggestedTotal() {
        int suggestedtTotal = 0;
        for (int i = 0; i < mDenominations.size(); i++) {
            int currencyQuantity = mDenominations.get(i).getDefaultQuantity();
            int currencyValue = mDenominations.get(i).getDenominationValue();
            int total_cal = currencyValue * currencyQuantity;
            suggestedtTotal = suggestedtTotal + total_cal;
        }
        ((TextView) mView.findViewById(R.id.txtSuggestedTotal)).setText("$" + dollarformat.format((float) suggestedtTotal / 100) + "");
        ((TextView) mView.findViewById(R.id.txtSuggestedTotal)).setTag(suggestedtTotal);
    }


    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void addCurrentyToLayout() {
        LinearLayout lnrCurreny = (LinearLayout) mView.findViewById(R.id.lnrCurreny);

        int pos = lnrCurreny.getChildCount();
        if (mDenominations == null) {
            return;
        }

        if (pos > mDenominations.size() - 1) {
            String initialmessage = "";
            if (floatdifference == 0) {
                initialmessage = AppConstants.CURRENCY_FLOAT_MESSAGE_FINAL;
                ((TextView) mView.findViewById(R.id.txtFloatTotal)).setTextColor(getResources().getColor(R.color.black));
            }

            if (floatdifference != 0 && ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() == (mDenominations.size())) {
                floatdifference = (float) (totalfloatvalue - totalsugestedvalue) / 100;
                ((TextView) mView.findViewById(R.id.txtFloatTotal)).setTextColor(getResources().getColor(R.color.red));
                initialmessage = AppConstants.CURRENCY_FLOAT_MESSAGE_DIFFERENCE;
            }

            message = String.format(initialmessage, "" + "", "", "" + "");

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    MessageEvent messageEvent = new MessageEvent(message);
                    messageEvent.setHideTip(true);
                    EventBus.getDefault().post(messageEvent);
                }
            }, 100);
        }

        final View rowView = mInflater.inflate(R.layout.layout_currency, null);

        if (mDenominations != null && mDenominations.size() > 0) {
            if (mDenominations.get(pos).getDenominationValue() < 100) {
                ((TextView) rowView.findViewById(R.id.txtCurrencyCent)).setText(mDenominations.get(pos).getDenominationValue() + "\u00A2");
                ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setTag(pos);
                ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setText("0");
                ((TextView) rowView.findViewById(R.id.txtCurrencyCent)).setVisibility(View.VISIBLE);
                ((TextView) rowView.findViewById(R.id.txtCurrencyNote)).setVisibility(View.GONE);
            } else {
                ((TextView) rowView.findViewById(R.id.txtCurrencyNote)).setText("$" + mDenominations.get(pos).getDenominationValue() / 100 + "");
                ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setTag(pos);
                ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setText("0");
                ((TextView) rowView.findViewById(R.id.txtCurrencyCent)).setVisibility(View.GONE);
                ((TextView) rowView.findViewById(R.id.txtCurrencyNote)).setVisibility(View.VISIBLE);

            }

            rowView.findViewById(R.id.txtCurrencyValue).setTag(0.0f);
            rowView.findViewById(R.id.txtCurrencyNote).setTag(mDenominations.get(pos).getDenominationValue());
            rowView.findViewById(R.id.edtCurencyCount).setTag(pos);
            ((EditText) rowView.findViewById(R.id.edtCurencyCount)).setOnFocusChangeListener(this);
            ((EditText) rowView.findViewById(R.id.edtCurencyCount)).requestFocus();

            lnrCurreny.addView(rowView);

            addDenomination(pos);
        }

    }

    @Subscribe
    public void saveFloat(SaveSetFloatEvent saveSetFloatEvent) {
        if (saveSetFloatEvent.isSave() == false) {
            ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
            addCurrentyToLayout();
            updateTotalFloat();
            mView.findViewById(R.id.rtlSuggestedTotal).setVisibility(View.GONE);
            ((TextView) mView.findViewById(R.id.txtFloatTotal)).setTextColor(getResources().getColor(R.color.black));
            return;
        }

        /*we have to save data to server and then save to our local data base.
        * If internet connection is not available the we will save data but sync will be false and
        * if we press sync button then we have syn with server and chnage the primary key ids with that.
        * and do sync true*/

        FloatHistory floatHistory = new FloatHistory();
        floatHistory.setFloatDate(new Date(System.currentTimeMillis()));
        floatHistory.setFloatSetBy(LookAtMePrefernece.getInstance().getActiveUsername());
        floatHistory.setFloatSetBy(LookAtMePrefernece.getInstance().getActiveUsername());
        floatHistory.setSynced(false);
        floatHistory.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
        if (mView.findViewById(R.id.txtFloatTotal).getTag() instanceof Integer) {
            floatHistory.setFloatValue((int) mView.findViewById(R.id.txtFloatTotal).getTag());

            try {
                long id = appDatabase.floatHistoryDao().insertAll(floatHistory);
                Log.d("Saved set float id : ", id + "");
                floatHistory.setFloatId((int) id);

                List<FloatDetails> floatDetailList = new ArrayList<>();
                for (Denominations temp : mDenominations) {
                    FloatDetails floatDetails = new FloatDetails();
                    floatDetails.setDenominationQuantity(temp.getTempCount());
                    temp.setDefaultQuantity(temp.getTempCount());
                    floatDetails.setDenominations(temp);
                    floatDetails.setFloatId(floatHistory.getFloatId());
                    floatDetails.setSynced(false);
                    floatDetails.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
                    floatDetailList.add(floatDetails);
                }
                appDatabase.floatDetailDao().insertAll(floatDetailList);

                //after successfully save
                ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).removeAllViews();
                addCurrentyToLayout();
                updateTotalFloat();
                LookAtMePrefernece.getInstance().setFloatTime(new Date(System.currentTimeMillis()));
                LookAtMePrefernece.getInstance().setFloatDisabled(true);
                AlertDialogUtils.showAlertDialog(getActivity(), "Cash Manager", "Set Float successfully saved.", new AlertDialogUtils.OnButtonClickListener() {
                    @Override
                    public void onButtonClick(int buttonId) {
                        getActivity().finish();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // ToastUtils.showToast(getActivity(), "Unable to Set Float");

            AlertDialogUtils.showAlertDialog(getActivity(), "Cash Manager", "Unable to Set Float.", new AlertDialogUtils.OnButtonClickListener() {
                @Override
                public void onButtonClick(int buttonId) {
                    //getActivity().finish();
                }
            });
        }

    }

    @Subscribe
    public void addCurrency(CurrencyEvent currencyEvent) {
        //NEXT
        if (currencyEvent.getNum() == -1) {
            addCurrentyToLayout();
        } else if (currencyEvent.getNum() == -2) {//BACKSPACE
            String temp = focusedEditText.getText().toString();
            if (StringUtils.isNullOrEmpty(temp)) {
                return;
            }
            focusedEditText.setText(temp.substring(0, temp.length() - 1));
            focusedEditText.setSelection(temp.length() - 1);

            if (focusedEditText.getText().toString().equals("")) {
                //addDenomination((int) focusedEditText.getTag());
            } else {
                addDenomination((int) focusedEditText.getTag());
            }

        } else {
            if (focusedEditText.getText().toString().equals("0")) {
                focusedEditText.setText("");
            }
            String temp = focusedEditText.getText().toString();
            temp = temp + currencyEvent.getNum();
            focusedEditText.setText(temp + "");
            focusedEditText.setSelection(temp.length());

            addDenomination((int) focusedEditText.getTag());
        }
    }


    private void addDenomination(int pos) {
        LinearLayout lnrCurreny = (LinearLayout) mView.findViewById(R.id.lnrCurreny);
        View view = lnrCurreny.getChildAt(pos);
        EditText edtCurencyCount = (EditText) view.findViewById(R.id.edtCurencyCount);
        int currencyCount = Integer.parseInt(edtCurencyCount.getText().toString().equals("") ? "0" : edtCurencyCount.getText().toString());
        mDenominations.get(pos).setTempCount(currencyCount);
        int currencyAmount = (int) view.findViewById(R.id.txtCurrencyNote).getTag() * currencyCount;
        ((TextView) view.findViewById(R.id.txtCurrencyValue)).setText("$" + dollarformat.format((float) currencyAmount / 100) + "");
        view.findViewById(R.id.txtCurrencyValue).setTag(currencyAmount);
        updateTotalFloat();

        String quantity = ((EditText) view.findViewById(R.id.edtCurencyCount)).getText().toString();

        String initialmessage = "";
        denominationposition = pos;

        totalfloatvalue = (int) ((TextView) mView.findViewById(R.id.txtFloatTotal)).getTag();
        totalsugestedvalue = (int) ((TextView) mView.findViewById(R.id.txtSuggestedTotal)).getTag();

        if (pos == 0 && ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() != (mDenominations.size())) {
            initialmessage = AppConstants.CURRENCY_FLOAT_MESSAGE_1;
        }

        if (pos != 0 && pos <= mDenominations.size() - 1) {
            initialmessage = AppConstants.CURRENCY_FLOAT_MESSAGE_MIDDLE;
        }

        floatdifference = (float) (totalfloatvalue - totalsugestedvalue) / 100;

        /*if (floatdifference == 0) {

            initialmessage = AppConstants.CURRENCY_FLOAT_MESSAGE_FINAL;
            ((TextView) mView.findViewById(R.id.txtFloatTotal)).setTextColor(getResources().getColor(R.color.black));
        }

        if (floatdifference != 0 && ((LinearLayout) mView.findViewById(R.id.lnrCurreny)).getChildCount() == (mDenominations.size())) {

            floatdifference = (float) (totalfloatvalue - totalsugestedvalue) / 100;

            ((TextView) mView.findViewById(R.id.txtFloatTotal)).setTextColor(getResources().getColor(R.color.red));
            initialmessage = AppConstants.CURRENCY_FLOAT_MESSAGE_DIFFERENCE;
        }*/

        if (pos == mDenominations.size() - 1) {
            mView.findViewById(R.id.rtlSuggestedTotal).setVisibility(View.VISIBLE);
        }


        /*if (mDenominations.get(pos).getDenominationValue() >= 100) {
            message = String.format(initialmessage, "" + "", "" + mDenominations.get(pos).getDenominationName(), "bills" + "");
        } else {
            message = String.format(initialmessage, "" + "", "" + mDenominations.get(pos).getDenominationName(), "coins" + "");
        }*/
        message = String.format(initialmessage, "" + "", "" + mDenominations.get(pos).getDenominationName(), "" + "");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new MessageEvent(message));
            }
        }, 100);

    }

    private void updateTotalFloat() {
        LinearLayout lnrCurreny = (LinearLayout) mView.findViewById(R.id.lnrCurreny);
        floatTotal = 0;
        for (int i = 0; i <= lnrCurreny.getChildCount() - 1; i++) {
            View view = lnrCurreny.getChildAt(i);
            //String txtCurrencyValue = (String) view.findViewById(R.id.txtCurrencyValue)).getTag();
            int currencyAmount = (int) view.findViewById(R.id.txtCurrencyValue).getTag();
            floatTotal = floatTotal + currencyAmount;
        }

        ((TextView) mView.findViewById(R.id.txtFloatTotal)).setTag(floatTotal);
        ((TextView) mView.findViewById(R.id.txtFloatTotal)).setText("$" + dollarformat.format((float) floatTotal / 100) + "");

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v instanceof EditText && hasFocus) {
            focusedEditText = (EditText) v;
        }
    }
}
