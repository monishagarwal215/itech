package com.cashmanager.ui.fragment.middle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.cashmanager.listener.CurrencyEvent;
import com.cashmanager.listener.MessageEvent;
import com.cashmanager.listener.SaveCheckBalanceEvent;
import com.cashmanager.numpad.NumPadButton;
import com.cashmanager.numpad.NumPadView;
import com.cashmanager.numpad.OnNumPadClickListener;
import com.cashmanager.ui.fragment.left.CheckBalanceLeftFragment;
import com.cashmanager1.R;
import com.cashmanager.util.LookAtMePrefernece;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.cashmanager1.R.id.numpad;

/**
 * Created by Dinesh Adhikari on 25/04/16.
 */
public class CheckBalanceMiddleFragment extends BaseFragment implements OnNumPadClickListener {

    private View mView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_check_balance_middle, null);
        ((TextView) mView.findViewById(R.id.titleText)).setText(LookAtMePrefernece.getInstance().getDeviceName());
        NumPadView customNumberPad = (NumPadView) mView.findViewById(numpad);
        customNumberPad.setNumberPadClickListener(this);

        mView.findViewById(R.id.txtSave).setOnClickListener(this);
        mView.findViewById(R.id.txtCancel).setOnClickListener(this);

        return mView;
    }

    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void
    onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSave:

                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(CheckBalanceLeftFragment.class.getSimpleName());
                if (fragment != null && fragment instanceof CheckBalanceLeftFragment) {
                    CheckBalanceLeftFragment checkBalanceLeftFragment = (CheckBalanceLeftFragment) fragment;
                    if (checkBalanceLeftFragment.getDifference() != 0) {
                        if (mView.findViewById(R.id.lnrNote).getVisibility() == View.VISIBLE) {
                            String note = ((EditText) mView.findViewById(R.id.edtNote)).getText().toString();
                            if (StringUtils.isNullOrEmpty(note)) {
                                ToastUtils.showToast(getActivity(), "Please enter note");
                                return;
                            } else {
                                AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Check Balance", "Do you really want to save.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                                    @Override
                                    public void onButtonClick(int buttonId) {
                                        switch (buttonId) {
                                            case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                                break;
                                            case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                                EventBus.getDefault().post(new SaveCheckBalanceEvent(true, ((EditText) mView.findViewById(R.id.edtNote)).getText().toString()));
                                                break;
                                        }
                                    }
                                });
                            }
                        } else {
                            mView.findViewById(R.id.lnrNote).setVisibility(View.VISIBLE);
                            mView.findViewById(R.id.txtMessage).setVisibility(View.GONE);
                            mView.findViewById(R.id.numpad).setVisibility(View.GONE);

                            return;
                        }
                    } else {
                        AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Check Balance", "Tap 'OK' to confirm and save.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                switch (buttonId) {
                                    case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                        break;
                                    case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                        EventBus.getDefault().post(new SaveCheckBalanceEvent(true, ((EditText) mView.findViewById(R.id.edtNote)).getText().toString()));
                                        break;
                                }
                            }
                        });
                    }
                }


                break;
            case R.id.txtCancel:
                AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Check Balance", "Do you really want to cancel.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                    @Override
                    public void onButtonClick(int buttonId) {
                        switch (buttonId) {
                            case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                break;
                            case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                mView.findViewById(R.id.lnrNote).setVisibility(View.GONE);
                                mView.findViewById(R.id.txtMessage).setVisibility(View.VISIBLE);
                                mView.findViewById(R.id.numpad).setVisibility(View.VISIBLE);
                                EventBus.getDefault().post(new SaveCheckBalanceEvent(false, ""));
                                break;
                        }
                    }
                });
                break;
            default:
                super.onClick(view);
        }
    }

    @Override
    public void onPadClicked(NumPadButton button) {
        switch (button) {
            case NUM_0:
                EventBus.getDefault().post(new CurrencyEvent(0));
                break;
            case NUM_1:
                EventBus.getDefault().post(new CurrencyEvent(1));
                break;
            case NUM_2:
                EventBus.getDefault().post(new CurrencyEvent(2));
                break;
            case NUM_3:
                EventBus.getDefault().post(new CurrencyEvent(3));
                break;
            case NUM_4:
                EventBus.getDefault().post(new CurrencyEvent(4));
                break;
            case NUM_5:
                EventBus.getDefault().post(new CurrencyEvent(5));
                break;
            case NUM_6:
                EventBus.getDefault().post(new CurrencyEvent(6));
                break;
            case NUM_7:
                EventBus.getDefault().post(new CurrencyEvent(7));
                break;
            case NUM_8:
                EventBus.getDefault().post(new CurrencyEvent(8));
                break;
            case NUM_9:
                EventBus.getDefault().post(new CurrencyEvent(9));
                break;
            case NEXT:
                EventBus.getDefault().post(new CurrencyEvent(-1));
                break;
            case BACKSPACE:
                EventBus.getDefault().post(new CurrencyEvent(-2));
                break;
            default:
                break;
        }
    }

    @Subscribe
    public void getMessage(MessageEvent messageEvent) {
        ((TextView) mView.findViewById(R.id.txtMessage)).setText(messageEvent.getMessage());
    }

}
