package com.cashmanager.ui.fragment.middle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.cashmanager.listener.CLoseoutUpdateMiddleUIEvent;
import com.cashmanager.listener.CurrencyEvent;
import com.cashmanager.listener.MessageEvent;
import com.cashmanager.listener.SaveCloseOutCBEvent;
import com.cashmanager.listener.SaveCloseOutMDEvent;
import com.cashmanager.listener.SaveCloseOutSFEvent;
import com.cashmanager.numpad.NumPadButton;
import com.cashmanager.numpad.NumPadView;
import com.cashmanager.numpad.OnNumPadClickListener;
import com.cashmanager.ui.fragment.left.CloseOutLeftFragment;
import com.cashmanager1.R;
import com.cashmanager.util.LookAtMePrefernece;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.cashmanager1.R.id.numpad;

/**
 * Created by Dinesh Adhikari on 25/06/17.
 */
public class CloseOutMiddleFragment extends BaseFragment implements OnNumPadClickListener {

    private View mView;
    int dropdifference = 0;
    int dropAmount = 0;
    int closeout_phase = 1;  // 1= check balance, 2 = makedrop, 3 = set float.

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_close_out_middle, null);
        ((TextView) mView.findViewById(R.id.titleText)).setText(LookAtMePrefernece.getInstance().getDeviceName());
        NumPadView customNumberPad = (NumPadView) mView.findViewById(numpad);
        customNumberPad.setNumberPadClickListener(this);

        mView.findViewById(R.id.txtSave).setOnClickListener(this);
        mView.findViewById(R.id.txtCancel).setOnClickListener(this);

        return mView;
    }

    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSave:

                if (closeout_phase == 1) // save check balance
                {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(CloseOutLeftFragment.class.getSimpleName());
                    if (fragment != null && fragment instanceof CloseOutLeftFragment) {
                        CloseOutLeftFragment closeoutLeftFragment = (CloseOutLeftFragment) fragment;

                        int cbdifference = closeoutLeftFragment.getCheckbalancedifference();
                        if (cbdifference != 0) {
                            if (mView.findViewById(R.id.lnrNote).getVisibility() == View.VISIBLE) {
                                String note = ((EditText) mView.findViewById(R.id.edtNote)).getText().toString();
                                if (StringUtils.isNullOrEmpty(note)) {
                                    ToastUtils.showToast(getActivity(), "Please enter note");
                                    return;
                                }
                            } else {
                                mView.findViewById(R.id.lnrNote).setVisibility(View.VISIBLE);
                                mView.findViewById(R.id.txtMessage).setVisibility(View.GONE);
                                mView.findViewById(R.id.numpad).setVisibility(View.GONE);
                                ((EditText) mView.findViewById(R.id.edtNote)).requestFocus();
                                return;
                            }
                        }
                    }

                    AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Check Balance", "Do you really want to save current changes.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {
                            switch (buttonId) {
                                case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                    break;
                                case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                    EventBus.getDefault().post(new SaveCloseOutCBEvent(true, ((EditText) mView.findViewById(R.id.edtNote)).getText().toString()));
                                    break;
                            }
                        }
                    });
                }

                if (closeout_phase == 2) // save drop amount
                {
                    if (dropAmount != 0) {
                        if (dropdifference != 0) {
                            AlertDialogUtils.showAlertDialog(getActivity(), "Closeout Drop", "You cannot save until your difference is equal to zero.", new AlertDialogUtils.OnButtonClickListener() {
                                @Override
                                public void onButtonClick(int buttonId) {

                                }
                            });
                        } else {

                            try {
                                printMessage(dropAmount);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Closeout Drop", "Please place the drop receipt together with the cash for the drop.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                                @Override
                                public void onButtonClick(int buttonId) {
                                    switch (buttonId) {
                                        case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                            break;
                                        case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                            EventBus.getDefault().post(new SaveCloseOutMDEvent(true, 1));
                                            break;
                                    }
                                }
                            });
                        }
                    } else {
                        AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Closeout Drop", "It appears there is no extra cash to make a drop. Tap OK to continue to Set Float.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                switch (buttonId) {
                                    case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                        break;
                                    case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                        EventBus.getDefault().post(new SaveCloseOutMDEvent(false, 3));
                                        break;
                                }
                            }
                        });
                    }
                }
                if (closeout_phase == 3) // save set float
                {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(CloseOutLeftFragment.class.getSimpleName());
                    if (fragment != null && fragment instanceof CloseOutLeftFragment) {
                        CloseOutLeftFragment closeoutLeftFragment = (CloseOutLeftFragment) fragment;
                        if (closeoutLeftFragment.getLinearCurrency().getChildCount() != closeoutLeftFragment.getmDenominations().size()) {
                            AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Closeout Set Float", "Are you sure want to save without adding all denominations?", "Yes", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                                @Override
                                public void onButtonClick(int buttonId) {
                                    switch (buttonId) {
                                        case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                            break;
                                        case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                            EventBus.getDefault().post(new SaveCloseOutSFEvent(true));
                                            break;
                                    }
                                }
                            });
                        } else {
                            if (closeoutLeftFragment.getFloatdifference() == 0) {
                                AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Closeout Set Float", "Tap 'OK' to confirm and save.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                                    @Override
                                    public void onButtonClick(int buttonId) {
                                        switch (buttonId) {
                                            case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                                break;
                                            case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                                EventBus.getDefault().post(new SaveCloseOutSFEvent(true));
                                                break;
                                        }
                                    }
                                });
                            } else {
                                AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Closeout Set Float", "Your float amount is different from the suggested amount.  Do you want to save float with this amount?", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                                    @Override
                                    public void onButtonClick(int buttonId) {
                                        switch (buttonId) {
                                            case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                                break;
                                            case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                                EventBus.getDefault().post(new SaveCloseOutSFEvent(true));
                                                break;
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
                break;
            case R.id.txtCancel:
                if (closeout_phase == 1) // save check balance
                {
                    AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Cash Manager", "Do you really want to cancel current changes", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {
                            switch (buttonId) {
                                case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                    break;
                                case AlertDialogUtils.BUTTON_CLICK_SUCCESS:

                                    // EventBus.getDefault().post(new MessageEvent("Please choose the amount of cash you would like to withdraw from the till:"));
                                    EventBus.getDefault().post(new SaveCloseOutCBEvent(false, ""));
                                    break;
                            }
                        }
                    });
                } else if (closeout_phase == 2) // save make drop
                {
                    AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Cash Manager", "Do you really want to cancel current changes", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {
                            switch (buttonId) {
                                case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                    break;
                                case AlertDialogUtils.BUTTON_CLICK_SUCCESS:

                                    // EventBus.getDefault().post(new MessageEvent("Please choose the amount of cash you would like to withdraw from the till:"));
                                    EventBus.getDefault().post(new SaveCloseOutMDEvent(false, 1));
                                    break;
                            }
                        }
                    });
                } else if (closeout_phase == 3) // save set float
                {
                    AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Cash Manager", "Do you really want to cancel current changes", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {
                            switch (buttonId) {
                                case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                    break;
                                case AlertDialogUtils.BUTTON_CLICK_SUCCESS:

                                    // EventBus.getDefault().post(new MessageEvent("Please choose the amount of cash you would like to withdraw from the till:"));
                                    EventBus.getDefault().post(new SaveCloseOutCBEvent(false, ""));
                                    break;
                            }
                        }
                    });
                }

                break;
            default:
                super.onClick(view);
        }
    }

    @Override
    public void onPadClicked(NumPadButton button) {
        switch (button) {
            case NUM_0:
                EventBus.getDefault().post(new CurrencyEvent(0));
                break;
            case NUM_1:
                EventBus.getDefault().post(new CurrencyEvent(1));
                break;
            case NUM_2:
                EventBus.getDefault().post(new CurrencyEvent(2));
                break;
            case NUM_3:
                EventBus.getDefault().post(new CurrencyEvent(3));
                break;
            case NUM_4:
                EventBus.getDefault().post(new CurrencyEvent(4));
                break;
            case NUM_5:
                EventBus.getDefault().post(new CurrencyEvent(5));
                break;
            case NUM_6:
                EventBus.getDefault().post(new CurrencyEvent(6));
                break;
            case NUM_7:
                EventBus.getDefault().post(new CurrencyEvent(7));
                break;
            case NUM_8:
                EventBus.getDefault().post(new CurrencyEvent(8));
                break;
            case NUM_9:
                EventBus.getDefault().post(new CurrencyEvent(9));
                break;
            case NEXT:
                EventBus.getDefault().post(new CurrencyEvent(-1));
                break;
            case BACKSPACE:
                EventBus.getDefault().post(new CurrencyEvent(-2));
                break;
            default:
                break;
        }
    }

    @Subscribe
    public void getMessage(MessageEvent messageEvent) {

        String finalmessage = "";
        finalmessage = messageEvent.getMessage();
        if (closeout_phase == 3) {
            Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(CloseOutLeftFragment.class.getSimpleName());
            if (fragment != null && fragment instanceof CloseOutLeftFragment) {
                CloseOutLeftFragment closeoutLeftFragment = (CloseOutLeftFragment) fragment;

                int count = closeoutLeftFragment.getmDenominations().get(closeoutLeftFragment.getDenominationposition()).getDefaultQuantity();
               // Log.d("quantity count: ",""+count);

                if (count > 0) {
                   // int count = closeoutLeftFragment.getmDenominations().get(closeoutLeftFragment.getDenominationposition()).getDefaultQuantity();
                    String value = closeoutLeftFragment.getmDenominations().get(closeoutLeftFragment.getDenominationposition()).getDenominationName();

                    finalmessage = finalmessage + "\n\nTip: For the recommended float, we suggest counting out " + count + " " + value;
                }
            }
        }
        ((TextView) mView.findViewById(R.id.txtMessage)).setText(finalmessage);
    }

    @Subscribe
    public void middleUIUpdate(CLoseoutUpdateMiddleUIEvent middleUIEvent) {

        if (middleUIEvent.getCloseout_phase() == 1) // cancel
        {
            closeout_phase = 1;
            mView.findViewById(R.id.lnrNote).setVisibility(View.GONE);
            mView.findViewById(R.id.txtMessage).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.numpad).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.rel_changeprint).setVisibility(View.GONE);

        }
        if (middleUIEvent.getCloseout_phase() == 2) {
            closeout_phase = 2;
            mView.findViewById(R.id.lnrNote).setVisibility(View.GONE);
            mView.findViewById(R.id.txtMessage).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.numpad).setVisibility(View.VISIBLE);

            Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(CloseOutLeftFragment.class.getSimpleName());
            if (fragment != null && fragment instanceof CloseOutLeftFragment) {

                CloseOutLeftFragment closeoutLeftFragment = (CloseOutLeftFragment) fragment;
                dropdifference = closeoutLeftFragment.getdropdifference();

                dropAmount = closeoutLeftFragment.getDropAmount();

                mView.findViewById(R.id.rel_changeprint).setVisibility(View.GONE);
                /*if (dropAmount != 0 && dropdifference == 0) {

                    mView.findViewById(R.id.rel_changeprint).setVisibility(View.VISIBLE);

                } else {
                    mView.findViewById(R.id.rel_changeprint).setVisibility(View.GONE);
                }*/
            }
        } else if (middleUIEvent.getCloseout_phase() == 3) {
            closeout_phase = 3;

            mView.findViewById(R.id.lnrNote).setVisibility(View.GONE);
            mView.findViewById(R.id.txtMessage).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.numpad).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.rel_changeprint).setVisibility(View.GONE);
        }
    }
}
