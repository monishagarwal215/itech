package com.cashmanager.ui.fragment.middle;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cashmanager.constants.AppDatabase;
import com.cashmanager.listener.CurrencyEvent;
import com.cashmanager.listener.MakeDropEvent;
import com.cashmanager.listener.MessageEvent;
import com.cashmanager.listener.NetSaleEvent;
import com.cashmanager.listener.OtherAmountEvent;
import com.cashmanager.listener.SaveMakeDropEvent;
import com.cashmanager.numpad.NumPadButton;
import com.cashmanager.numpad.NumPadView;
import com.cashmanager.numpad.OnNumPadClickListener;
import com.cashmanager.pojo.FloatHistory;
import com.cashmanager.ui.fragment.left.MakeDropLeftFragment;
import com.cashmanager1.R;
import com.cashmanager.util.LookAtMePrefernece;
import com.clover.sdk.v1.printer.job.TextPrintJob;
import com.krapps.application.BaseApplication;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DecimalFormat;

/**
 * Created by Dinesh Adhikari on 25/04/16.
 */
public class MakeDropMiddleFragment extends BaseFragment implements OnNumPadClickListener, RadioGroup.OnCheckedChangeListener {

    private View mView;
    DecimalFormat dollarformat = new DecimalFormat("#0.00");
    EditText otherAmountEditText;
    int dropAmount = 0;
    float difference = 0;
    private int netSales;
    private AppDatabase appDatabase;
    private int floatTotal;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_make_drop_middle, null);
        ((TextView) mView.findViewById(R.id.titleText)).setText(LookAtMePrefernece.getInstance().getDeviceName());
        NumPadView customNumberPad = (NumPadView) mView.findViewById(R.id.numpad);
        customNumberPad.setNumberPadClickListener(this);

        mView.findViewById(R.id.txtSave).setOnClickListener(this);
        mView.findViewById(R.id.txtCancel).setOnClickListener(this);

        otherAmountEditText = ((EditText) mView.findViewById(R.id.et_OtherAmount));
        otherAmountEditText.setEnabled(false);

        appDatabase = BaseApplication.getAppDatabase(getActivity());
        FloatHistory latestfloat = appDatabase.floatHistoryDao().getLatestFloatHistory();
        if (latestfloat != null) {
            floatTotal = latestfloat.getFloatValue();
        }

        ((RadioGroup) mView.findViewById(R.id.rg_makedrop)).setOnCheckedChangeListener(this);

        return mView;
    }

    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe
    public void addNetSale(NetSaleEvent netsaleEvent) {
        //NEXT
       /* if (netsaleEvent.getNetsale() >= 0) {
            ((TextView) mView.findViewById(R.id.tv_netsale)).setText("$" + dollarformat.format((float) (netsaleEvent.getNetsale()) / 100));
            netSales = netsaleEvent.getNetsale();
            dropAmount = netsaleEvent.getNetsale();
        }*/

        ((TextView) mView.findViewById(R.id.tv_netsale)).setText("$" + dollarformat.format((float) (netsaleEvent.getNetsale()) / 100));
        netSales = netsaleEvent.getNetsale();
        dropAmount = netsaleEvent.getNetsale();
    }

   /* public boolean isNetSalesChecked() {
        return ((RadioButton) mView.findViewById(R.id.rdBtnNetSales)).isChecked();
    }*/

    @Override
    public void onClick(View view) {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(MakeDropLeftFragment.class.getSimpleName());
        MakeDropLeftFragment makeDropLeftFragment = (MakeDropLeftFragment) fragment;
        switch (view.getId()) {
            case R.id.txtSave:
                int rdBtn = ((RadioGroup) mView.findViewById(R.id.rg_makedrop)).getCheckedRadioButtonId();
                if (rdBtn <= 0) {
                    showAlertMessage("You haven’t selected an amount. If you wish to exit without making a drop, press OK. Otherwise, tap cancel to continue.");
                    return;
                }

                if (makeDropLeftFragment.getLnrCurreny().getChildCount() == 0) {
                    showAlertMessage("You have not completed your drop. Press 'OK' to exit or 'Cancel' to continue you count.");
                    return;
                }

                if (!StringUtils.isNullOrEmpty(otherAmountEditText.getText().toString()) && Float.parseFloat(otherAmountEditText.getText().toString()) == 0) {
                    showAlertMessage("Please enter a valid amount.");
                    otherAmountEditText.setText("");
                    return;
                }

                if (!StringUtils.isNullOrEmpty(otherAmountEditText.getText().toString()) && Float.parseFloat(otherAmountEditText.getText().toString()) > (float) (netSales + floatTotal) / 100) {
                    showAlertMessage("The amount you entered is greater than the total cash available. Please select an amount less than total cash.");
                    return;
                }

                if (fragment != null && fragment instanceof MakeDropLeftFragment) {
                    MakeDropLeftFragment makedropLeftFragment = (MakeDropLeftFragment) fragment;
                    difference = makedropLeftFragment.getDifference();
                    if (difference != 0) {
                        showAlertMessage("You cannot save until your difference is equal to zero");
                        return;
                    }

                    try {
                        printMessage(dropAmount);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Cash Manager", "Please place the drop receipt together with the cash for the drop.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {
                            switch (buttonId) {
                                case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                    break;
                                case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                    //printMessage(dropAmount);
                                    EventBus.getDefault().post(new SaveMakeDropEvent(true));
                                    break;
                            }
                        }
                    });
                }
                break;
            case R.id.txtCancel:
                int radioselected = ((RadioGroup) mView.findViewById(R.id.rg_makedrop)).getCheckedRadioButtonId();
                if (radioselected <= 0) {
                    getActivity().finish();
                } else {
                    AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Cash Manager", "Do you really want to cancel current changes", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {
                            switch (buttonId) {
                                case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                    break;
                                case AlertDialogUtils.BUTTON_CLICK_SUCCESS:

                                    ((RadioGroup) mView.findViewById(R.id.rg_makedrop)).clearCheck();
                                    ((RelativeLayout) mView.findViewById(R.id.rel_dropamountlayout)).setVisibility(View.VISIBLE);
                                    ((EditText) mView.findViewById(R.id.et_OtherAmount)).setEnabled(false);
                                    ((EditText) mView.findViewById(R.id.et_OtherAmount)).setText("");

                                    EventBus.getDefault().post(new MessageEvent("Please choose the amount of cash you would like to withdraw from the till:"));
                                    EventBus.getDefault().post(new SaveMakeDropEvent(false));
                                    break;
                            }
                        }
                    });
                }
                break;
            default:
                super.onClick(view);
        }
    }


    private void showAlertMessage(final String s) {
        AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Cash Manager", s, "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
            @Override
            public void onButtonClick(int buttonId) {
                switch (buttonId) {
                    case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                        break;
                    case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                        if (s.contains("exit")) {
                            getActivity().finish();
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void onPadClicked(NumPadButton button) {
        int numpadvalue = 0;
        switch (button) {
            case NUM_0:
                numpadvalue = 0;
                break;
            case NUM_1:
                numpadvalue = 1;
                break;
            case NUM_2:
                numpadvalue = 2;
                break;
            case NUM_3:
                numpadvalue = 3;
                break;
            case NUM_4:
                numpadvalue = 4;
                break;
            case NUM_5:
                numpadvalue = 5;
                break;
            case NUM_6:
                numpadvalue = 6;
                break;
            case NUM_7:
                numpadvalue = 7;
                break;
            case NUM_8:
                numpadvalue = 8;
                break;
            case NUM_9:
                numpadvalue = 9;
                break;
            case NEXT:
                numpadvalue = -1;
                break;
            case BACKSPACE:
                numpadvalue = -2;
                break;
            default:
                break;

        }

        int rdBtn = ((RadioGroup) mView.findViewById(R.id.rg_makedrop)).getCheckedRadioButtonId();
        if (rdBtn <= 0) {
            showAlertMessage("Please select a valid drop amount.");
            dropAmount = 0;
            return;
        }

        if (rdBtn == R.id.rdBtnOtherAmount && otherAmountEditText.isFocused()
                && numpadvalue == -1 && otherAmountEditText.getText().toString().isEmpty()) {
            showAlertMessage("Please enter a valid drop amount.");
            dropAmount = 0;
            return;
        }

        if (!StringUtils.isNullOrEmpty(otherAmountEditText.getText().toString()) && Float.parseFloat(otherAmountEditText.getText().toString()) == 0) {
            showAlertMessage("Please enter a valid amount.");
            otherAmountEditText.setText("");
            return;
        }


        if (rdBtn == R.id.rdBtnOtherAmount && otherAmountEditText.isFocused()
                && numpadvalue == -1 && !StringUtils.isNullOrEmpty(otherAmountEditText.getText().toString()) && Float.parseFloat(otherAmountEditText.getText().toString()) > (float) (netSales + floatTotal) / 100) {
            showAlertMessage("The amount you entered is greater than the total cash available. Please select an amount less than total cash.");
            return;
        }

        if (rdBtn == R.id.rdBtnOtherAmount && otherAmountEditText.isFocused()
                && numpadvalue == -1 && !StringUtils.isNullOrEmpty(otherAmountEditText.getText().toString()) && Float.parseFloat(otherAmountEditText.getText().toString()) < (float) (netSales + floatTotal) / 100) {
            EventBus.getDefault().post(new OtherAmountEvent(numpadvalue));
            try {
                dropAmount = Integer.parseInt(otherAmountEditText.getText().toString().replace("$", "")) * 100;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            return;
        }

        if (rdBtn == R.id.rdBtnOtherAmount && otherAmountEditText.isFocused()) {
            EventBus.getDefault().post(new OtherAmountEvent(numpadvalue));
        } else {
            EventBus.getDefault().post(new CurrencyEvent(numpadvalue));
        }
    }


    @Subscribe
    public void otherAmountListner(OtherAmountEvent otherAmountEvent) {
        //NEXT
        if (otherAmountEvent.getNum() == -1) {
            otherAmountEditText.clearFocus();
            EventBus.getDefault().post(new CurrencyEvent(-1));

            if (((RadioButton) mView.findViewById(R.id.rdBtnNetSales)).isChecked()) {
                dropAmount = netSales;
            } else {
                try {
                    dropAmount = Integer.parseInt(otherAmountEditText.getText().toString().replace("$", "")) * 100;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    dropAmount = 0;
                }
            }

            EventBus.getDefault().post(new MakeDropEvent(dropAmount));
            ((RelativeLayout) mView.findViewById(R.id.rel_dropamountlayout)).setVisibility(View.GONE);
        } else if (otherAmountEvent.getNum() == -2) {//BACKSPACE
            String temp = otherAmountEditText.getText().toString();
            if (StringUtils.isNullOrEmpty(temp)) {
                return;
            }
            otherAmountEditText.setText(temp.substring(0, temp.length() - 1));
            otherAmountEditText.setSelection(temp.length() - 1);

        } else {
            String temp = otherAmountEditText.getText().toString();
            temp = temp + otherAmountEvent.getNum();
            otherAmountEditText.setText(temp + "");
            otherAmountEditText.setSelection(temp.length());
        }
    }

    @Subscribe
    public void getMessage(MessageEvent messageEvent) {
        ((TextView) mView.findViewById(R.id.txtMessage)).setText(messageEvent.getMessage());

    }

    public void printmsg(String content) {
        //print String text
        new TextPrintJob.Builder().text(content).build().print(getActivity(), account);
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.rdBtnNetSales:
                mView.findViewById(R.id.txtTapGreen).setVisibility(View.GONE);
                if (netSales > 0) {
                    ToastUtils.showToast(getActivity(), "net sales");
                    ((EditText) mView.findViewById(R.id.et_OtherAmount)).setEnabled(false);
                    ((EditText) mView.findViewById(R.id.et_OtherAmount)).setText("");
                    // dropAmount = netSales;
                    EventBus.getDefault().post(new OtherAmountEvent(-1));
                } else {
                    showAlertMessage("It appears there is no extra cash to make a drop.");
                    ((RadioGroup) mView.findViewById(R.id.rg_makedrop)).clearCheck();
                    ((EditText) mView.findViewById(R.id.et_OtherAmount)).setText("");
                }

                break;
            case R.id.rdBtnOtherAmount:
                mView.findViewById(R.id.txtTapGreen).setVisibility(View.VISIBLE);
                ToastUtils.showToast(getActivity(), "other amount");
                ((EditText) mView.findViewById(R.id.et_OtherAmount)).setEnabled(true);
                ((EditText) mView.findViewById(R.id.et_OtherAmount)).requestFocus();

                break;

        }
    }
}
