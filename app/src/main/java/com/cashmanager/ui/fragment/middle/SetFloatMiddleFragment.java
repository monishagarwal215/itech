package com.cashmanager.ui.fragment.middle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cashmanager.listener.CurrencyEvent;
import com.cashmanager.listener.MessageEvent;
import com.cashmanager.listener.SaveSetFloatEvent;
import com.cashmanager.numpad.NumPadButton;
import com.cashmanager.numpad.NumPadView;
import com.cashmanager.numpad.OnNumPadClickListener;
import com.cashmanager.ui.fragment.left.SetFloatLeftFragment;
import com.cashmanager1.R;
import com.cashmanager.util.LookAtMePrefernece;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Dinesh Adhikari on 25/04/16.
 */
public class SetFloatMiddleFragment extends BaseFragment implements OnNumPadClickListener {

    private View mView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_set_float_middle, null);

        ((TextView) mView.findViewById(R.id.titleText)).setText(LookAtMePrefernece.getInstance().getDeviceName());
        NumPadView customNumberPad = (NumPadView) mView.findViewById(R.id.numpad);
        customNumberPad.setNumberPadClickListener(this);

        mView.findViewById(R.id.txtSave).setOnClickListener(this);
        mView.findViewById(R.id.txtCancel).setOnClickListener(this);

        return mView;
    }

    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSave:

                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(SetFloatLeftFragment.class.getSimpleName());
                if (fragment != null && fragment instanceof SetFloatLeftFragment) {
                    SetFloatLeftFragment setfloatLeftFragment = (SetFloatLeftFragment) fragment;
                    if (setfloatLeftFragment.getLinearCurrency().getChildCount() != setfloatLeftFragment.getmDenominations().size()) {
                        AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Set Float", "Are you sure want to save without adding all denominations?", "Yes", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                switch (buttonId) {
                                    case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                        break;
                                    case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                        EventBus.getDefault().post(new SaveSetFloatEvent(true));
                                        break;
                                }
                            }
                        });
                    } else {

                        if (setfloatLeftFragment.getFloatdifference() == 0) {
                            AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Set Float", "Tap 'OK' to confirm and save.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                                @Override
                                public void onButtonClick(int buttonId) {
                                    switch (buttonId) {
                                        case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                            break;
                                        case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                            EventBus.getDefault().post(new SaveSetFloatEvent(true));
                                            break;
                                    }
                                }
                            });
                        } else {
                            AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Set Float", "Your float amount is different from the suggested amount. Do you want to save float with this amount?", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                                @Override
                                public void onButtonClick(int buttonId) {
                                    switch (buttonId) {
                                        case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                            break;
                                        case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                            EventBus.getDefault().post(new SaveSetFloatEvent(true));
                                            break;
                                    }
                                }
                            });
                        }

                    }

                }
                break;
            case R.id.txtCancel:
                AlertDialogUtils.showAlertDialogWithTwoButtons(getActivity(), "Set Float", "Do you really want to cancel.", "OK", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                    @Override
                    public void onButtonClick(int buttonId) {
                        switch (buttonId) {
                            case AlertDialogUtils.BUTTON_CLICK_FAILURE:
                                break;
                            case AlertDialogUtils.BUTTON_CLICK_SUCCESS:
                                EventBus.getDefault().post(new SaveSetFloatEvent(false));
                                break;
                        }
                    }
                });
                break;
            default:
                super.onClick(view);
        }
    }

    @Override
    public void onPadClicked(NumPadButton button) {
        switch (button) {
            case NUM_0:
                EventBus.getDefault().post(new CurrencyEvent(0));
                break;
            case NUM_1:
                EventBus.getDefault().post(new CurrencyEvent(1));
                break;
            case NUM_2:
                EventBus.getDefault().post(new CurrencyEvent(2));
                break;
            case NUM_3:
                EventBus.getDefault().post(new CurrencyEvent(3));
                break;
            case NUM_4:
                EventBus.getDefault().post(new CurrencyEvent(4));
                break;
            case NUM_5:
                EventBus.getDefault().post(new CurrencyEvent(5));
                break;
            case NUM_6:
                EventBus.getDefault().post(new CurrencyEvent(6));
                break;
            case NUM_7:
                EventBus.getDefault().post(new CurrencyEvent(7));
                break;
            case NUM_8:
                EventBus.getDefault().post(new CurrencyEvent(8));
                break;
            case NUM_9:
                EventBus.getDefault().post(new CurrencyEvent(9));
                break;
            case NEXT:
                EventBus.getDefault().post(new CurrencyEvent(-1));
                break;
            case BACKSPACE:
                EventBus.getDefault().post(new CurrencyEvent(-2));
                break;
            default:
                break;
        }
    }

    @Subscribe
    public void getMessage(MessageEvent messageEvent) {

        String finalmessage = "";

        finalmessage = messageEvent.getMessage();

        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(SetFloatLeftFragment.class.getSimpleName());
        if (fragment != null && fragment instanceof SetFloatLeftFragment) {
            SetFloatLeftFragment setfloatLeftFragment = (SetFloatLeftFragment) fragment;

            int count = setfloatLeftFragment.getmDenominations().get(setfloatLeftFragment.getDenominationposition()).getDefaultQuantity();

            if (count > 0) {
                // int count = setfloatLeftFragment.getmDenominations().get(setfloatLeftFragment.getDenominationposition()).getDefaultQuantity();
                String value = setfloatLeftFragment.getmDenominations().get(setfloatLeftFragment.getDenominationposition()).getDenominationName();

                /*if (setfloatLeftFragment.getmDenominations().get(setfloatLeftFragment.getDenominationposition()).getDenominationValue() >= 100) {
                    finalmessage = finalmessage + "\n\nTip: For the recommended float, we suggest counting out " + value + " bills.";
                } else {
                    finalmessage = finalmessage + "\n\nTip: For the recommended float, we suggest counting out " + value + " coins.";
                }*/
                //  finalmessage = finalmessage + "\n\nTip: For the recommended float, we suggest counting out " + value;

                if (messageEvent.isHideTip()) {
                } else {
                    finalmessage = finalmessage + "\n\nTip: For the recommended float, we suggest counting out " + count + " " + value;
                }

            }
        }

        ((TextView) mView.findViewById(R.id.txtMessage)).setText(finalmessage);
    }

}
