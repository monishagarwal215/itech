package com.cashmanager.ui.fragment.right;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cashmanager.constants.ApiConstants;
import com.cashmanager.constants.AppConstants;
import com.cashmanager.constants.AppDatabase;
import com.cashmanager.pojo.Actions;
import com.cashmanager.pojo.RoleActions;
import com.cashmanager.pojo.Roles;
import com.cashmanager.ui.fragment.left.CheckBalanceLeftFragment;
import com.cashmanager.ui.fragment.left.CloseOutLeftFragment;
import com.cashmanager.ui.fragment.left.MakeDropLeftFragment;
import com.cashmanager.ui.fragment.left.SetFloatLeftFragment;
import com.cashmanager.ui.fragment.middle.CheckBalanceMiddleFragment;
import com.cashmanager.ui.fragment.middle.CloseOutMiddleFragment;
import com.cashmanager.ui.fragment.middle.MakeDropMiddleFragment;
import com.cashmanager.ui.fragment.middle.SetFloatMiddleFragment;
import com.cashmanager.util.LookAtMePrefernece;
import com.cashmanager1.R;
import com.krapps.application.BaseApplication;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;

import java.util.List;

/**
 * Created by Dinesh Adhikari on 25/04/16.
 */
public class RightMenuFragment extends BaseFragment {

    private View mView;
    private AppDatabase appDatabase;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_right_menu, null);

        mView.findViewById(R.id.txtCloseOut).setOnClickListener(this);
        mView.findViewById(R.id.txtSetFloat).setOnClickListener(this);
        mView.findViewById(R.id.txtCheckbalance).setOnClickListener(this);
        mView.findViewById(R.id.txtMakeDrop).setOnClickListener(this);
        mView.findViewById(R.id.txtMainMenu).setOnClickListener(this);

        getTabSelected();

        appDatabase = BaseApplication.getAppDatabase(getActivity());
        List<Roles> roles = appDatabase.rolesDao().getRoleByName(LookAtMePrefernece.getInstance().getCloverRoleName().toUpperCase());
        List<Actions> actionsList = appDatabase.actionDao().getAllActions();
        if (roles != null && roles.size() > 0) {
            List<RoleActions> roleActionsList = appDatabase.rolesActionsDao().getByRoleId(roles.get(0).getRoleId());
            showMenuAccordingToRole(actionsList, roleActionsList);
        }

        return mView;
    }

    private void showMenuAccordingToRole(List<Actions> actionsList, List<RoleActions> roleActionsList) {

        mView.findViewById(R.id.txtCloseOut).setVisibility(View.GONE);
        mView.findViewById(R.id.txtSetFloat).setVisibility(View.GONE);
        mView.findViewById(R.id.txtCheckbalance).setVisibility(View.GONE);
        mView.findViewById(R.id.txtMakeDrop).setVisibility(View.GONE);

        for (RoleActions temp : roleActionsList) {
            Actions actionToFind = new Actions();
            actionToFind.setActionId(temp.getActionId());
            if (actionsList.contains(actionToFind)) {
                int pos = actionsList.indexOf(actionToFind);
                Actions actions = actionsList.get(pos);
                showMenu(actions);

            }
        }

    }

    private void showMenu(Actions actions) {
        switch (actions.getActionName()) {
            case AppConstants.ACTION_SET_FLOAT:
                mView.findViewById(R.id.txtSetFloat).setVisibility(View.VISIBLE);
                break;
            case AppConstants.ACTION_CHECK_BALANCE:
                mView.findViewById(R.id.txtCheckbalance).setVisibility(View.VISIBLE);
                break;
            case AppConstants.ACTION_MAKE_DROP:
                mView.findViewById(R.id.txtMakeDrop).setVisibility(View.VISIBLE);
                break;
            case AppConstants.ACTION_CLOSE_OUT:
                mView.findViewById(R.id.txtCloseOut).setVisibility(View.VISIBLE);
                break;
        }
    }

    private void getTabSelected() {
        if (getActivity().getIntent() != null) {
            Intent intent = getActivity().getIntent();
            int pos = intent.getIntExtra(ApiConstants.EXTRA_POSITION, 0);
            switch (pos) {
                case 0:
                    mView.findViewById(R.id.txtSetFloat).performClick();
                    break;
                case 1:
                    mView.findViewById(R.id.txtCheckbalance).performClick();
                    break;
                case 2:
                    mView.findViewById(R.id.txtMakeDrop).performClick();
                    break;
                case 3:
                    mView.findViewById(R.id.txtCloseOut).performClick();
                    break;
            }
        }
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.txtCloseOut:
                makeViewSelected(v.getId());
                ((BaseActivity) getActivity()).replaceFragment(null, new CloseOutLeftFragment(), null, false, R.id.content_frame_left);
                ((BaseActivity) getActivity()).replaceFragment(null, new CloseOutMiddleFragment(), null, false, R.id.content_frame_middle);
                break;
            case R.id.txtSetFloat:
                if (LookAtMePrefernece.getInstance().isSetFloatDisabled() == false) {
                    makeViewSelected(v.getId());
                    ((BaseActivity) getActivity()).replaceFragment(null, new SetFloatLeftFragment(), null, false, R.id.content_frame_left);
                    ((BaseActivity) getActivity()).replaceFragment(null, new SetFloatMiddleFragment(), null, false, R.id.content_frame_middle);
                } else {
                    AlertDialogUtils.showAlertDialog(getActivity(), "Cashmanager", "You must first closeout your current shift before you can set float again.", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {

                        }
                    });
                }
                break;
            case R.id.txtCheckbalance:
                makeViewSelected(v.getId());
                ((BaseActivity) getActivity()).replaceFragment(null, new CheckBalanceLeftFragment(), null, false, R.id.content_frame_left);
                ((BaseActivity) getActivity()).replaceFragment(null, new CheckBalanceMiddleFragment(), null, false, R.id.content_frame_middle);
                break;
            case R.id.txtMakeDrop:
                makeViewSelected(v.getId());
                ((BaseActivity) getActivity()).replaceFragment(null, new MakeDropLeftFragment(), null, false, R.id.content_frame_left);
                ((BaseActivity) getActivity()).replaceFragment(null, new MakeDropMiddleFragment(), null, false, R.id.content_frame_middle);
                break;
            case R.id.txtMainMenu:
                makeViewSelected(v.getId());
                getActivity().finish();
                getActivity().finish();
                break;
            default:
                super.onClick(v);
        }
    }

    private void makeViewSelected(int viewId) {
        mView.findViewById(R.id.txtCloseOut).setBackground(null);
        mView.findViewById(R.id.txtSetFloat).setBackground(null);
        mView.findViewById(R.id.txtCheckbalance).setBackground(null);
        mView.findViewById(R.id.txtMakeDrop).setBackground(null);
        mView.findViewById(R.id.txtMainMenu).setBackground(null);

        mView.findViewById(viewId).setBackgroundColor(getResources().getColor(R.color.primary_grey));
    }


}
