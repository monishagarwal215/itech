package com.cashmanager.util;

import android.os.AsyncTask;

import com.cashmanager.constants.AppDatabase;

/**
 * Created by Dinesh Adhikari on 07/06/17.
 */

public class DbInitializer {

    public static void populateDbAsynTask(final AppDatabase db) {
        PopulateDbAsync task = new PopulateDbAsync(db);
        task.execute();
    }


    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;

        PopulateDbAsync(AppDatabase db) {
            mDb = db;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            //populateWithTestData(mDb);
            return null;
        }

    }

    /*private static void populateWithTestData(AppDatabase mDb) {
        ArrayList<Denominations> denominationsList = new ArrayList<>();

        Denominations denomination100 = new Denominations();
        denomination100.setDenominationId(1);
        denomination100.setActive(true);
        denomination100.setDefault(false);
        denomination100.setDenominationName("hundred dollar");
        denomination100.setDenominationValue(100);
        denomination100.setDefaultQuantity(5);
        denominationsList.add(denomination100);

        Denominations denomination50 = new Denominations();
        denomination50.setDenominationId(2);
        denomination50.setActive(true);
        denomination50.setDefault(false);
        denomination50.setDenominationName("fifty dollar");
        denomination50.setDenominationValue(50);
        denomination50.setDefaultQuantity(5);
        denominationsList.add(denomination50);

        Denominations denomination20 = new Denominations();
        denomination20.setDenominationId(3);
        denomination20.setActive(true);
        denomination20.setDefault(false);
        denomination20.setDenominationName("twenty dollar");
        denomination20.setDenominationValue(20);
        denomination20.setDefaultQuantity(10);
        denominationsList.add(denomination20);

        Denominations denomination10 = new Denominations();
        denomination10.setDenominationId(4);
        denomination10.setActive(true);
        denomination10.setDefault(false);
        denomination10.setDenominationName("ten dollar");
        denomination10.setDenominationValue(10);
        denomination10.setDefaultQuantity(10);
        denominationsList.add(denomination10);

        Denominations denomination5 = new Denominations();
        denomination5.setDenominationId(5);
        denomination5.setActive(true);
        denomination5.setDefault(false);
        denomination5.setDenominationName("five dollar");
        denomination5.setDenominationValue(5);
        denomination5.setDefaultQuantity(10);
        denominationsList.add(denomination5);

        Denominations denomination1 = new Denominations();
        denomination1.setDenominationId(6);
        denomination1.setActive(true);
        denomination1.setDefault(false);
        denomination1.setDenominationName("one dollar");
        denomination1.setDenominationValue(1);
        denomination1.setDefaultQuantity(20);
        denominationsList.add(denomination1);

        Denominations denominationCent50 = new Denominations();
        denominationCent50.setDenominationId(7);
        denominationCent50.setActive(true);
        denominationCent50.setDefault(false);
        denominationCent50.setDenominationName("fifty cent");
        denominationCent50.setDenominationValue(0.50f);
        denominationCent50.setDefaultQuantity(20);
        denominationsList.add(denominationCent50);

        Denominations denominationCent25 = new Denominations();
        denominationCent25.setDenominationId(8);
        denominationCent25.setActive(true);
        denominationCent25.setDefault(false);
        denominationCent25.setDenominationName("twenty five cent");
        denominationCent25.setDenominationValue(0.25f);
        denominationCent25.setDefaultQuantity(25);
        denominationsList.add(denominationCent25);

        Denominations denominationCent5 = new Denominations();
        denominationCent5.setDenominationId(9);
        denominationCent5.setActive(true);
        denominationCent5.setDefault(false);
        denominationCent5.setDenominationName("five cent");
        denominationCent5.setDenominationValue(0.05f);
        denominationCent5.setDefaultQuantity(25);
        denominationsList.add(denominationCent5);

        Device device = new Device();
        device.setId("1");
        device.setType("type");
        device.setName("name");

        mDb.deviceDao().insertAll(device);
        mDb.denominationDao().insertAll(denominationsList);
    }*/

}
