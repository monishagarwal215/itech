package com.cashmanager.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.cashmanager.constants.AppConstants;
import com.krapps.application.BaseApplication;

import java.util.Date;


public class LookAtMePrefernece {

    private static SharedPreferences mPreferences;
    private static LookAtMePrefernece mInstance;
    private static Editor mEditor;

    private static String IS_LOGGED_IN = "is_logged_in_new";
    private static String APP_VERSION = "app_version";
    private static String MERCHANT_ID = "merchant_id";
    private static String DEVICE_ID = "deviceid";
    private static String AUTH_TOKEN = "auth_token";
    private static String BASE_URL = "base_url";
    private static String CLOVER_ROLE_NAME = "clover_role_name";
    private static String ACTIVE_USERNAME = "active_username";
    private static String SYSTEM_ROLE = "system_role";
    private static String DEVICE_NAME = "device_name";
    private static String REGISTER_ID = "register_id";
    private static String FLOAT_TIME = "float_time";
    private static String LAST_MAKE_DROP_AMOUNT = "last_make_drop_amount";
    private static String IS_SET_FLOAT_DISABLED = "is_set_float_disabled";
    private static String PRINTER_ID = "printer_id";

    private LookAtMePrefernece() {
    }

    public static LookAtMePrefernece getInstance() {
        if (mInstance == null) {
            Context context = BaseApplication.mContext;
            mInstance = new LookAtMePrefernece();
            mPreferences = context.getSharedPreferences(AppConstants.NIGHT_ADVISOR_PREFS, Context.MODE_PRIVATE);
            mEditor = mPreferences.edit();
        }
        return mInstance;
    }

    public void setActiveUsername(String value) {
        mEditor.putString(ACTIVE_USERNAME, value).apply();
    }


    public String getActiveUsername() {
        return mPreferences.getString(ACTIVE_USERNAME, null);
    }

    public void setMakeDropAmount(int value) {
        mEditor.putInt(LAST_MAKE_DROP_AMOUNT, value).apply();
    }


    public int getMakeDropAmount() {
        return mPreferences.getInt(LAST_MAKE_DROP_AMOUNT, 0);
    }


    public void setAppVersion(int value) {
        mEditor.putInt(APP_VERSION, value).apply();
    }


    public int getAppVersion() {
        return mPreferences.getInt(APP_VERSION, 1);
    }

    public void setMerchantId(String value) {
        mEditor.putString(MERCHANT_ID, value).apply();
    }


    public String getMerchantId() {
        return mPreferences.getString(MERCHANT_ID, null);
    }

    public void setDeviceId(String value) {
        mEditor.putString(DEVICE_ID, value).apply();
    }


    public String getDeviceName() {
        return mPreferences.getString(DEVICE_NAME, null);
    }

    public void setDeviceName(String value) {
        mEditor.putString(DEVICE_NAME, value).apply();
    }

    public String getRegisterId() {
        return mPreferences.getString(REGISTER_ID, null);
    }

    public void setRegisterId(String value) {
        mEditor.putString(REGISTER_ID, value).apply();
    }


    public String getDeviceId() {
        return mPreferences.getString(DEVICE_ID, null);
    }

    public void setAuthToken(String value) {
        mEditor.putString(AUTH_TOKEN, value).apply();
    }


    public String getAuthToken() {
        return mPreferences.getString(AUTH_TOKEN, null);
    }

    public void setBaseUrl(String value) {
        mEditor.putString(BASE_URL, value).apply();
    }


    public String getBaseUrl() {
        return mPreferences.getString(BASE_URL, null);
    }


    public void setLoggedIn(boolean value) {
        mEditor.putBoolean(IS_LOGGED_IN, value).apply();
    }

    public boolean getLoggedIn() {
        return mPreferences.getBoolean(IS_LOGGED_IN, false);
    }

    public void setFloatDisabled(boolean value) {
        mEditor.putBoolean(IS_SET_FLOAT_DISABLED, value).apply();
    }

    public boolean isSetFloatDisabled() {
        return mPreferences.getBoolean(IS_SET_FLOAT_DISABLED, false);
    }

    public void setFloatTime(Date date) {
        mEditor.putLong(FLOAT_TIME, date.getTime()).apply();
    }

    public Long getFloatTime() {
        return mPreferences.getLong(FLOAT_TIME, new Date().getTime());
    }

    public String getPrinterId() {
        return mPreferences.getString(PRINTER_ID, null);
    }

    public void setPrinterId(String value) {
        mEditor.putString(PRINTER_ID, value).apply();
    }

    public void setCloverRoleName(String name) {
        mEditor.putString(CLOVER_ROLE_NAME, name).apply();
    }

    public String getCloverRoleName() {
        return mPreferences.getString(CLOVER_ROLE_NAME, "");
    }
}