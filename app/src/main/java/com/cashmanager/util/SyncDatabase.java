package com.cashmanager.util;

import android.content.Context;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.mobileconnectors.apigateway.ApiClientFactory;
import com.cashmanager.CashManagerClient;
import com.cashmanager.constants.ApiConstants;
import com.cashmanager.constants.AppDatabase;
import com.cashmanager.model.Actions;
import com.cashmanager.model.ActionsItem;
import com.cashmanager.model.Counts;
import com.cashmanager.model.CountsCountDetailsItem;
import com.cashmanager.model.CountsCountHistory;
import com.cashmanager.model.Denominations;
import com.cashmanager.model.DenominationsItem;
import com.cashmanager.model.Device;
import com.cashmanager.model.Floats;
import com.cashmanager.model.FloatsFloatDetailsItem;
import com.cashmanager.model.FloatsFloatHistory;
import com.cashmanager.model.Register;
import com.cashmanager.model.Roleactions;
import com.cashmanager.model.RoleactionsItem;
import com.cashmanager.model.Roles;
import com.cashmanager.model.RolesItem;
import com.cashmanager.model.request.RegisterDeviceRequest;
import com.cashmanager.pojo.CountDetails;
import com.cashmanager.pojo.CountHistory;
import com.cashmanager.pojo.FloatDetails;
import com.cashmanager.pojo.FloatHistory;
import com.cashmanager.pojo.RoleActions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.utils.ToastUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinesh on 29/06/17.
 */

public class SyncDatabase implements UpdateJsonListener.onUpdateViewJsonListener {

    private static SyncDatabase mInstance;
    private static AppDatabase appDatabase;
    private static Context mContext;
    private static String url;
    private static CashManagerClient client;
    private GsonBuilder builder;
    private Gson gson;

    private SyncDatabase() {

    }

    public static SyncDatabase getInstance(Context context) {

        if (mInstance == null) {
            mInstance = new SyncDatabase();
        }
        mContext = context;
        appDatabase = BaseApplication.getAppDatabase(mContext);

        AWSCredentialsProvider awsCredentialsProvider = new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return new AWSCredentials() {
                    @Override
                    public String getAWSAccessKeyId() {
                        return "AKIAI2CATDSUYQNCTKCQ";
                    }

                    @Override
                    public String getAWSSecretKey() {
                        return "RZJ0ajilHVt5zF3uwfGKks5D/iw6ThhPjfZKfTzA";
                    }
                };
            }

            @Override
            public void refresh() {

            }
        };

        ApiClientFactory factory = new ApiClientFactory()
                .credentialsProvider(awsCredentialsProvider);

        client = factory.build(CashManagerClient.class);

        return mInstance;

    }


    public void syncRoles() {
        Roles roles = client.rolesGet();
        List<com.cashmanager.pojo.Roles> rolesPojoList = new ArrayList<>();
        for (RolesItem temp : roles) {
            com.cashmanager.pojo.Roles rolesPojo = new com.cashmanager.pojo.Roles();
            rolesPojo.setActive(temp.getIsActive() == 1 ? true : false);
            rolesPojo.setRoleDescription(temp.getRoleDescription());
            rolesPojo.setRoleId(temp.getRoleId());
            rolesPojo.setRoleName(temp.getRoleName() + "");
            rolesPojoList.add(rolesPojo);
        }
        appDatabase.rolesDao().insertAll(rolesPojoList);
    }

    public void syncActions() {
        Actions actions = client.actionsGet();
        List<com.cashmanager.pojo.Actions> actionsPojoList = new ArrayList<>();
        for (ActionsItem temp : actions) {
            com.cashmanager.pojo.Actions actionsPojo = new com.cashmanager.pojo.Actions();
            actionsPojo.setActionId(temp.getActionId());
            actionsPojo.setActive(temp.getIsActive() == 1 ? true : false);
            actionsPojo.setActionDescription(temp.getActionDescription());
            actionsPojo.setActionName(temp.getActionName());
            actionsPojoList.add(actionsPojo);
        }

        appDatabase.actionDao().insertAll(actionsPojoList);
    }

    public void syncRolesActions() {
        Roleactions roleactions = client.roleactionsGet();
        List<RoleActions> roleActionsPojoList = new ArrayList<>();
        for (RoleactionsItem temp : roleactions) {
            RoleActions roleActionPojo = new RoleActions();
            roleActionPojo.setRoleActionId(temp.getRoleActionId());
            roleActionPojo.setActionId(temp.getActionId());
            roleActionPojo.setActive(temp.getIsActive() == 1 ? true : false);
            roleActionPojo.setRoleId(temp.getRoleId());
            roleActionPojo.setCanModify(temp.getCanModify());
            roleActionsPojoList.add(roleActionPojo);
        }

        appDatabase.rolesActionsDao().insertAll(roleActionsPojoList);
    }

    public void syncDenomination() {
        Denominations denominations = client.denominationsGet();

        List<com.cashmanager.pojo.Denominations> denominationPojoList = new ArrayList<>();
        for (DenominationsItem temp : denominations) {
            com.cashmanager.pojo.Denominations denominationsPojo = new com.cashmanager.pojo.Denominations();
            denominationsPojo.setDenominationId(temp.getDenominationId());
            denominationsPojo.setDenominationName(temp.getDenominationName());
            denominationsPojo.setDenominationValue(temp.getDenominationValue());
            denominationsPojo.setSortOrder(temp.getSortOrder());
            denominationsPojo.setActive(temp.getIsActive() == 1 ? true : false);
            denominationsPojo.setDefaultQuantity(temp.getDefaultQuantity());
            denominationPojoList.add(denominationsPojo);
        }
        try {
            appDatabase.denominationDao().insertAll(denominationPojoList);
        } catch (Exception e) {
            ToastUtils.showToast(mContext, "Some error in denomination api gateway");
            e.printStackTrace();
        }
    }

    public void syncFloatHistory() {
        List<FloatHistory> floatHistoryList = null;
        try {
            floatHistoryList = appDatabase.floatHistoryDao().findAllUnSyncedFloatHistory();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (floatHistoryList != null && floatHistoryList.size() > 0) {
            for (final FloatHistory temp : floatHistoryList) {
                try {
                    List<FloatDetails> floatDetailsList = appDatabase.floatDetailDao().getAllFloatDetailByFloatId(temp.getFloatId());
                    //temp.setFloatDate(null);
                    temp.setRegisterId(LookAtMePrefernece.getInstance().getRegisterId());
                    temp.setFloatDetails(floatDetailsList);

                    FloatsFloatHistory floatsFloatHistory = new FloatsFloatHistory();
                    floatsFloatHistory.setDeviceId(temp.getDeviceId());
                    floatsFloatHistory.setFloatDate(temp.getFloatDate().toString());
                    floatsFloatHistory.setFloatId(temp.getFloatId());
                    floatsFloatHistory.setFloatSetBy(temp.getFloatSetBy());
                    floatsFloatHistory.setFloatValue(temp.getFloatValue());
                    if (temp.getRegisterId() != null) {
                        floatsFloatHistory.setRegisterId(Integer.parseInt(temp.getRegisterId()));
                    }

                    List<FloatsFloatDetailsItem> floatsFloatDetailsItemList = new ArrayList<FloatsFloatDetailsItem>();
                    for (FloatDetails temp1 : floatDetailsList) {
                        FloatsFloatDetailsItem floatsFloatDetailsItem = new FloatsFloatDetailsItem();
                        floatsFloatDetailsItem.setFloatId(temp1.getFloatId());
                        floatsFloatDetailsItem.setDeviceId(temp1.getDeviceId());
                        floatsFloatDetailsItem.setDenominationId(temp1.getDenominations().getDenominationId());
                        floatsFloatDetailsItem.setDenominationName(temp1.getDenominations().getDenominationName());
                        floatsFloatDetailsItem.setDenominationQuantity(temp1.getDenominationQuantity());
                        floatsFloatDetailsItem.setDenominationTotal(temp1.getDenominationQuantity() * temp1.getDenominations().getDenominationValue());
                        floatsFloatDetailsItem.setDenominationValue(temp1.getDenominations().getDenominationValue());
                        floatsFloatDetailsItem.setFloatDetailsId(temp1.getFloatDetailsId());
                        floatsFloatDetailsItemList.add(floatsFloatDetailsItem);
                    }
                    Floats floats = new Floats();
                    floats.setFloatHistory(floatsFloatHistory);
                    floats.setFloatDetails(floatsFloatDetailsItemList);

                    client.floatsPost(floats);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void syncCountHistory() {
        List<CountHistory> countHistoryList = null;
        try {
            countHistoryList = appDatabase.countHistoryDao().findAllUnSyncedCountHistory();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (countHistoryList != null && countHistoryList.size() > 0) {
            for (final CountHistory temp : countHistoryList) {
                try {
                    List<CountDetails> countDetails = appDatabase.countDetailDao().getAllCountDetailByCountId(temp.getCountId());
                    temp.setRegisterId(LookAtMePrefernece.getInstance().getRegisterId());
                    temp.setCountDetails(countDetails);

                    CountsCountHistory countsCountHistory = new CountsCountHistory();
                    countsCountHistory.setCountId(temp.getCountId());
                    countsCountHistory.setCountDate(temp.getCountDate().toString());
                    countsCountHistory.setCountRecordedBy(temp.getCountRecordedBy());
                    countsCountHistory.setCountType(temp.getCountType());
                    countsCountHistory.setDeviceId(temp.getDeviceId());
                    countsCountHistory.setNotes(temp.getNotes());
                    if (temp.getRegisterId() != null) {
                        countsCountHistory.setRegisterId(Integer.parseInt(temp.getRegisterId()));
                    }
                    countsCountHistory.setSystemCountValue(temp.getSystemCountValue());
                    countsCountHistory.setUserCountValue(temp.getUserCountValue());

                    List<CountsCountDetailsItem> countsCountDetailsItems = new ArrayList<CountsCountDetailsItem>();
                    for (CountDetails temp1 : countDetails) {
                        CountsCountDetailsItem countsCountDetailsItem = new CountsCountDetailsItem();
                        countsCountDetailsItem.setDeviceId(temp1.getDeviceId());
                        countsCountDetailsItem.setCountDetailsId(temp1.getCountDetailsId());
                        countsCountDetailsItem.setCountId(temp1.getCountId());
                        countsCountDetailsItem.setDenominationId(temp1.getDenominations().getDenominationId());
                        countsCountDetailsItem.setDenominationName(temp1.getDenominations().getDenominationName());
                        countsCountDetailsItem.setDenominationQuantity(temp1.getDenominationQuantity());
                        countsCountDetailsItem.setDenominationTotal(temp1.getDenominationQuantity() * temp1.getDenominations().getDenominationValue());
                        countsCountDetailsItem.setDenominationValue(temp1.getDenominations().getDenominationValue());
                        countsCountDetailsItems.add(countsCountDetailsItem);
                    }

                    Counts counts = new Counts();
                    counts.setCountHistory(countsCountHistory);
                    counts.setCountDetails(countsCountDetailsItems);
                    client.countsPost(counts);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(mContext, "Some error occured.");
                return;
            }

            switch (reqType) {

                case ApiConstants.REQUEST_POST_FLOAT_DETAIL:
                    builder = new GsonBuilder();
                    builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(json.getAsJsonPrimitive().getAsLong());
                        }
                    });
                    gson = builder.create();
                    FloatHistory floatHistory = gson.fromJson(responseString, FloatHistory.class);
                    if (floatHistory != null) {
                        appDatabase.floatHistoryDao().update(floatHistory);
                        if (floatHistory.getFloatDetails() != null) {
                            appDatabase.floatDetailDao().updateAll(floatHistory.getFloatDetails());
                        }
                    }
                    break;
                case ApiConstants.REQUEST_POST_COUNT_DETAIL:
                    builder = new GsonBuilder();
                    builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(json.getAsJsonPrimitive().getAsLong());
                        }
                    });
                    gson = builder.create();
                    CountHistory countHistory = gson.fromJson(responseString, CountHistory.class);
                    if (countHistory != null) {
                        appDatabase.countHistoryDao().update(countHistory);
                        if (countHistory.getCountDetails() != null) {
                            appDatabase.countDetailDao().updateAll(countHistory.getCountDetails());
                        }
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void syncDevice() {
        RegisterDeviceRequest registerDeviceRequest = new RegisterDeviceRequest();
        registerDeviceRequest.setDeviceDescription(LookAtMePrefernece.getInstance().getDeviceName());
        registerDeviceRequest.setDeviceId(LookAtMePrefernece.getInstance().getDeviceId());
        registerDeviceRequest.setDeviceName(LookAtMePrefernece.getInstance().getDeviceName());

        Device device = new Device();
        Register registers = new Register();
        registers.setDeviceId(registerDeviceRequest.getDeviceId());
        registers.setDeviceName(registerDeviceRequest.getDeviceName());
        registers.setDeviceDescription(registerDeviceRequest.getDeviceDescription());

        client.registersPost(registers);

    }
}
