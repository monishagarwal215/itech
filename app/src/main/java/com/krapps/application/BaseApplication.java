package com.krapps.application;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.Handler;

import com.cashmanager.constants.AppConstants;
import com.cashmanager.constants.AppDatabase;
import com.cashmanager1.R;
import com.krapps.database.BaseSqliteOpenHelper;
import com.krapps.network.VolleyManager;
import com.krapps.utils.FontsOverride;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * @author Dinesh Adhikari
 */
public class BaseApplication extends Application {

    private static final String TAG = BaseApplication.class.getSimpleName();
    public static Handler mHandler = null;

    private static BaseSqliteOpenHelper dbHelper;
    public static Context mContext = null;
    private static AppDatabase appDatabase;


    @Override
    public void onCreate() {

        super.onCreate();
        mContext = getApplicationContext();
        mHandler = new Handler();

        getAppDatabase(this);
        initializeFont();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
               // .setDefaultFontPath("fonts/Roboto-RobotoRegular.ttf")
                .setDefaultFontPath("fonts/opensans_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    private void initializeFont() {
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/opensans_regular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/opensans_regular.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/opensans_regular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/opensans_regular.ttf");
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    public VolleyManager getVolleyManagerInstance() {
        return VolleyManager.getInstance(getApplicationContext());
    }

    public static BaseSqliteOpenHelper getDbHelperInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new BaseSqliteOpenHelper(context);
        }
        return dbHelper;
    }

    public static AppDatabase getAppDatabase(Context context) {
        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(context, AppDatabase.class, AppConstants.DB_NAME).
                    allowMainThreadQueries()
                    .build();
        }
        return appDatabase;
    }

    @Override
    public void onTerminate() {
        dbHelper.close();
        super.onTerminate();
    }

}
