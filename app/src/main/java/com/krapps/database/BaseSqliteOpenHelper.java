package com.krapps.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author Dinesh Adhikari
 */
public class BaseSqliteOpenHelper extends SQLiteOpenHelper {

    private String LOG_TAG = "BaseSqliteOpenHelper";

    /**
     * Common constants for database and for all tables
     */
    public final static String DB_NAME = "db_chapp";
    public final static int DB_VERSION = 1;

    public final static String DB_COLUMN_ID = "id";
    public static final int DB_OPERATION_FAILURE = -1;
    public static final int DB_OPERATION_SUCCESS = 1;

    /**
     * Array of Table create queries...
     */
    public final static String[] DB_SQL_CREATE_TABLE_QUERIES = new String[]
            {
            };

    /**
     * Array of Table drop queries...
     */
    public final static String[] DB_SQL_DROP_TABLE_QUERIES = new String[]
            {
            };

    public BaseSqliteOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(LOG_TAG, "Tables creation start.");
        int size = DB_SQL_CREATE_TABLE_QUERIES.length;
        for (int i = 0; i < size; i++) {
            db.execSQL(DB_SQL_CREATE_TABLE_QUERIES[i]);
        }
        Log.i(LOG_TAG, "Tables creation end.");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
        Log.i(LOG_TAG, "DB upgrade.");
        int size = DB_SQL_DROP_TABLE_QUERIES.length;
        for (int i = 0; i < size; i++) {
            db.execSQL(DB_SQL_DROP_TABLE_QUERIES[i]);
        }
        onCreate(db);
    }
}
