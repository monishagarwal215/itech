package com.krapps.listener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.cashmanager1.BuildConfig;;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.krapps.model.CommonJsonResponse;
import com.krapps.network.VolleyExceptionUtil;
import com.krapps.network.VolleyStringRequest;
import com.krapps.utils.StringUtils;

import java.lang.reflect.Type;
import java.util.Date;

public class UpdateListener<T> implements Listener<String>, ErrorListener {
    private int reqType;
    private onUpdateViewListener onUpdateViewListener;
    private Activity mActivity;
    private Class<T> classObject;

    public Activity getmActivity() {
        return mActivity;
    }

    public interface onUpdateViewListener {
        void updateView(Object responseObject, boolean isSuccess, int reqType);
    }

    public UpdateListener(Activity activity, onUpdateViewListener onUpdateView, int reqType, Class<T> classObject) {
        this.reqType = reqType;
        this.onUpdateViewListener = onUpdateView;
        mActivity = activity;
        this.classObject = classObject;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        onUpdateViewListener.updateView(VolleyExceptionUtil.getErrorMessage(error), false, reqType);
    }

    @Override
    public void onResponse(final String responseStr) {
        if (StringUtils.isNullOrEmpty(responseStr)) {
            return;
        }
        if (BuildConfig.DEBUG) {
            Log.i(VolleyStringRequest.mNetworkTag, responseStr + "");
        }
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    /*Gson gson = new GsonBuilder()
                            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                            .create();*/

                    GsonBuilder builder = new GsonBuilder();
                    builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(json.getAsJsonPrimitive().getAsLong());
                        }
                    });
                    Gson gson = builder.create();

                    final Object responseObject = gson.fromJson(responseStr, classObject);
                    mActivity.runOnUiThread(new Runnable() {
                        @SuppressLint("InlinedApi")
                        @Override
                        public void run() {
                            if (responseObject instanceof CommonJsonResponse) {

                            }
                            onUpdateViewListener.updateView(responseObject, true, reqType);
                        }
                    });
                }
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
            onUpdateViewListener.updateView(responseStr, false, reqType);
        }
    }

}
