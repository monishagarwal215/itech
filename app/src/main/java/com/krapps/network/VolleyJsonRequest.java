package com.krapps.network;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cashmanager1.BuildConfig;;
import com.krapps.listener.UpdateJsonListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * @author Dinesh Adhikari
 */
public class VolleyJsonRequest extends JsonObjectRequest {

    private VolleyJsonRequest(int method, String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {
        super(method, url, requestJson == null ? new JSONObject() : new JSONObject(requestJson), updateListener, updateListener);
    }

    public static VolleyJsonRequest doPost(String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url--->", url);
            Log.i("Request Json-->", requestJson);
        }
        return new VolleyJsonRequest(Method.POST, url, updateListener, requestJson);
    }

    public static VolleyJsonRequest doPut(String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url--->", url);
            Log.i("Request Json-->", requestJson);
        }
        return new VolleyJsonRequest(Method.PUT, url, updateListener, requestJson);
    }

    public static VolleyJsonRequest doget(String url, UpdateJsonListener updateListener) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url-->", url);
        }
        return new VolleyJsonRequest(Method.GET, url, updateListener, null);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return super.getHeaders();
    }
}