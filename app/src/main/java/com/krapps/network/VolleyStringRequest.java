package com.krapps.network;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.cashmanager1.BuildConfig;;
import com.krapps.listener.UpdateListener;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dinesh Adhikari
 */
public class VolleyStringRequest extends StringRequest {

    private static Activity mActivity;
    private Map<String, String> mRequestparams = new HashMap<String, String>();
    private String mJsonBody;
    public static final String mNetworkTag = "Network";

    private VolleyStringRequest(int method, String url, UpdateListener updateListener, Map<String, String> params) {
        super(method, url, updateListener, updateListener);
        mRequestparams = params;
    }

    private VolleyStringRequest(int method, String url, UpdateListener updateListener, String json) {
        super(method, url, updateListener, updateListener);
        mJsonBody = json;
    }

    public static VolleyStringRequest doPost(String url, UpdateListener updateListener, Map<String, String> params) {
        if (BuildConfig.DEBUG) {
            Log.i(mNetworkTag, url);
            Log.i(mNetworkTag, params.toString());
        }
        mActivity = updateListener.getmActivity();
        return new VolleyStringRequest(Method.POST, url, updateListener, params);
    }

    public static VolleyStringRequest doGet(String url, UpdateListener updateListener, Map<String, String> params) {
        if (BuildConfig.DEBUG) {
            Log.i(mNetworkTag, url);
        }
        mActivity = updateListener.getmActivity();
        return new VolleyStringRequest(Method.GET, url, updateListener, params);
    }

    public static VolleyStringRequest doPost(String url, UpdateListener updateListener, String json) {
        if (BuildConfig.DEBUG) {
            Log.i(mNetworkTag, url);
            Log.i(mNetworkTag, json.toString());
        }
        mActivity = updateListener.getmActivity();
        return new VolleyStringRequest(Method.POST, url, updateListener, json);
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (mJsonBody == null) {
            return super.getBody();
        } else {
            return mJsonBody.getBytes();
        }
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return mRequestparams;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        super.getHeaders();

        Map<String, String> params = new HashMap<String, String>();
        params.put("Content-Type", "application/json");


        return params;
    }
}
