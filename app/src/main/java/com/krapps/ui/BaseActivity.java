package com.krapps.ui;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.LifecycleActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import com.cashmanager.constants.AppConstants;
import com.cashmanager.util.LookAtMePrefernece;
import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.v1.ResultStatus;
import com.clover.sdk.v1.ServiceConnector;
import com.clover.sdk.v1.merchant.Merchant;
import com.clover.sdk.v1.merchant.MerchantConnector;
import com.clover.sdk.v3.employees.EmployeeConnector;
import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.listener.UpdateListener.onUpdateViewListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class BaseActivity extends LifecycleActivity implements onUpdateViewListener, OnClickListener, UpdateJsonListener.onUpdateViewJsonListener, ServiceConnector.OnServiceConnectedListener {

    private static AlertDialog alertDialog;
    private BaseApplication mChappApplication;
    protected EmployeeConnector employeeConnector;
    protected MerchantConnector merchantConnector;
    protected Account account;
    private ProgressDialog mProgressDialog;
    private String mDeviceId;

    private LayoutInflater mInflater;
    private Picasso picasso;

    int imgSize = 0;


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        mInflater = LayoutInflater.from(getBaseContext());
        mChappApplication = (BaseApplication) getApplication();

        picasso = Picasso.with(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void connect() {
        disconnect();
        if (account != null) {
            employeeConnector = new EmployeeConnector(this, account, this);
            employeeConnector.connect();
            merchantConnector = new MerchantConnector(this, account, this);
            merchantConnector.connect();
        }
    }

    private void disconnect() {
        if (employeeConnector != null) {
            employeeConnector.disconnect();
            employeeConnector = null;
        }
        if (merchantConnector != null) {
            merchantConnector.disconnect();
            merchantConnector = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (account != null) {
            connect();
            if (merchantConnector != null && LookAtMePrefernece.getInstance().getMerchantId() == null) {
                getMerchant();
            } else {
            }
        } else {
            startAccountChooser();
        }
    }

    private void getMerchant() {
        merchantConnector.getMerchant(new MerchantConnector.MerchantCallback<Merchant>() {
            @Override
            public void onServiceSuccess(Merchant result, ResultStatus status) {
                super.onServiceSuccess(result, status);
                LookAtMePrefernece.getInstance().setMerchantId(result.getId());
                LookAtMePrefernece.getInstance().setDeviceId(result.getDeviceId());
            }

            @Override
            public void onServiceFailure(ResultStatus status) {
                super.onServiceFailure(status);
                Log.d("CASH MANAGER", "get merchant bind failure");
            }

            @Override
            public void onServiceConnectionFailure() {
                super.onServiceConnectionFailure();
                Log.d("CASH MANAGER", "get merchant bind failure");
            }
        });
    }

    public void startAccountChooser() {
        Intent intent = AccountManager.newChooseAccountIntent(null, null, new String[]{CloverAccount.CLOVER_ACCOUNT_TYPE}, false, null, null, null, null);
        startActivityForResult(intent, AppConstants.REQUEST_ACCOUNT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstants.REQUEST_ACCOUNT) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                String type = data.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);

                account = new Account(name, type);
            } else {
                if (account == null) {
                    finish();
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        disconnect();
        super.onDestroy();
    }


    public void showProgressDialog(String... args) {
        if (isFinishing()) {
            return;
        }

        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mProgressDialog.setCancelable(false);
            }
            if (args != null && args.length > 0) {
                mProgressDialog.setMessage(args[0]);
            } else {
                mProgressDialog.setMessage("Loading...");
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes the progress dialog
     */
    public void removeProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.hide();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        if (showLoader) {
            showProgressDialog();
        }
        String url = "";
        switch (reqType) {
            default:
                url = "";
                className = null;
                break;
        }
    }


    private String getJsonRequest(int reqType) {
        Gson gson = new Gson();
        return "";
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();

        return params;
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {

    }


    public void replaceFragment(String currentFragmentTag, Fragment fragment, Bundle bundle, boolean isAddToBackStack, int frameId) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragmentLocal = getSupportFragmentManager().findFragmentById(frameId);
        if (fragmentLocal != null && fragmentLocal.getTag().equalsIgnoreCase(tag)) {
            ((BaseFragment) fragmentLocal).refreshFragment(bundle);
            return;
        }
        if (!StringUtils.isNullOrEmpty(currentFragmentTag)) {
            ft.add(frameId, fragment, tag);
            Fragment fragmentToHide = getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
            if (fragmentToHide != null) {
                ft.hide(fragmentToHide);
            }
        } else {
            ft.replace(frameId, fragment, tag);
        }

        fragment.setRetainInstance(true);
        if (isAddToBackStack) {
            ft.addToBackStack(tag);
        }
        try {
            ft.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            ft.commitAllowingStateLoss();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public EmployeeConnector getEmployeeConnector() {
        return employeeConnector;
    }

    public MerchantConnector getMerchantConnector() {
        return merchantConnector;
    }

    public Account getAccount() {
        return account;
    }

    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {

    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onServiceConnected(ServiceConnector<? extends IInterface> connector) {

    }

    @Override
    public void onServiceDisconnected(ServiceConnector<? extends IInterface> connector) {

    }
}
