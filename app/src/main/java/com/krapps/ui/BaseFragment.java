package com.krapps.ui;

import android.accounts.Account;
import android.app.Activity;
import android.arch.lifecycle.LifecycleFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.cashmanager.util.LookAtMePrefernece;
import com.clover.sdk.v1.merchant.MerchantConnector;
import com.clover.sdk.v1.printer.Category;
import com.clover.sdk.v1.printer.Printer;
import com.clover.sdk.v1.printer.PrinterConnector;
import com.clover.sdk.v1.printer.job.PrintJob;
import com.clover.sdk.v1.printer.job.PrintJobsConnector;
import com.clover.sdk.v1.printer.job.TextPrintJob;
import com.clover.sdk.v3.employees.EmployeeConnector;
import com.krapps.listener.UpdateListener.onUpdateViewListener;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

public class BaseFragment extends LifecycleFragment implements OnClickListener, onUpdateViewListener {
    private String frgamentName;
    public BaseActivity mActivity;
    private float density;
    public DecimalFormat dollarformat = new DecimalFormat("#0.00");
    protected EmployeeConnector employeeConnector;
    protected MerchantConnector merchantConnector;
    protected PrinterConnector printerConnector;
    protected Account account;


    public BaseFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        density = mActivity.getResources().getDisplayMetrics().density;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() == null) {
            return;
        }

        account = ((BaseActivity) getActivity()).getAccount();
        merchantConnector = ((BaseActivity) getActivity()).getMerchantConnector();
        employeeConnector = ((BaseActivity) getActivity()).getEmployeeConnector();

        try {
            printerConnector = new PrinterConnector((BaseActivity) getActivity(), account, null);
            fillPrinterId();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (printerConnector != null) {
            printerConnector.disconnect();
            printerConnector = null;
        }
        hideKeyboard();
    }

    public float getDensity() {
        return density;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (BaseActivity) activity;
    }


    private void hideKeyboard() {
        // Check if no view has focus:
        View view = mActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void setFragmentName(String frgamentName) {
        this.frgamentName = frgamentName;
    }

    public String getFragmentName() {
        return frgamentName;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
        // ((DashboardActivity) mActivity).removeProgressView();
    }

    /**
     * Override this method when refresh page is required in your fragment
     *
     * @param bundle
     */
    protected void refreshFragment(Bundle bundle) {

    }

    public void updateView(Object object) {

    }

    @Override
    public void onClick(View view) {
    }

    private void fillPrinterId() {

        new AsyncTask<Void, Void, List<Printer>>() {
            @Override
            protected List<Printer> doInBackground(Void... params) {
                try {
                    return printerConnector.getPrinters(Category.RECEIPT);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<Printer> printers) {
                if (printers != null && !printers.isEmpty()) {

                    // set first printerID
                    if (printers != null && printers.size() > 0) {
                        LookAtMePrefernece.getInstance().setPrinterId(printers.get(0).getUuid());
                    }

                }
            }
        }.execute();
    }

    public void printmsg(final String content) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                try {
                    // print text
                    Printer p = getPrinter();
                    PrintJob printJob = new TextPrintJob.Builder().text(content).build();
                    return new PrintJobsConnector(getActivity()).print(p, printJob);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String id) {
                if (id != null) {
                    Toast.makeText(getActivity(), "Print success, print job ID: " + id, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Print failure, please check your printer connection", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    private Printer getPrinter() {
        String printerID = LookAtMePrefernece.getInstance().getPrinterId();
        if (TextUtils.isEmpty(printerID)) {
            try {
                List<Printer> printers = printerConnector.getPrinters(Category.RECEIPT);
                if (printers != null && !printers.isEmpty()) {
                    return printers.get(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        try {
            return printerConnector.getPrinter(printerID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void printMessage(int dropAmount) {
        try {
            StringBuilder sb = new StringBuilder("Cash Manager - Drop Receipt");
            sb.append("\n");
            sb.append("Merchant ID: " + LookAtMePrefernece.getInstance().getMerchantId());
            sb.append("\n");
            sb.append("Address: " + "South Street Mall");
            sb.append("\n");
            sb.append("Device Name: " + LookAtMePrefernece.getInstance().getDeviceName());
            sb.append("\n");
            sb.append("User Name: " + LookAtMePrefernece.getInstance().getActiveUsername());
            sb.append("\n");
            sb.append("Drop Amount: " + "$" + dollarformat.format(((float) dropAmount) / 100));
            sb.append("\n");
            sb.append("Date and Time: " + new Date());
            Log.d("printer msg", sb.toString());
            printmsg(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
