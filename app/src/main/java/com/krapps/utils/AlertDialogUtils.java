package com.krapps.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.krapps.ui.BaseActivity;

public class AlertDialogUtils {

    public static final int BUTTON_CLICK_FAILURE = 0;
    public static final int BUTTON_CLICK_SUCCESS = 1;
    private static AlertDialog alertDialog;

    public static void showAlertDialogWithTwoButtons(final Context context, String headerMessage, String messageInfo, String btnPositiveText, String btnNegativeText, final OnButtonClickListener onButtonClick) {
        if (context == null) {
            return;
        }
        if (((BaseActivity) context).isFinishing()) {
            return;
        }
        final AlertDialog alertDialog = new AlertDialog.Builder(context).setTitle(headerMessage).setMessage(messageInfo).setPositiveButton(btnPositiveText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                onButtonClick.onButtonClick(BUTTON_CLICK_SUCCESS);
            }
        }).setNegativeButton(btnNegativeText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onButtonClick.onButtonClick(BUTTON_CLICK_FAILURE);
            }
        }).setCancelable(false).show();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                alertDialog.getButton(Dialog.BUTTON_POSITIVE).setTextSize(context.getResources().getDisplayMetrics().density * 20);
                alertDialog.getButton(Dialog.BUTTON_NEGATIVE).setTextSize(context.getResources().getDisplayMetrics().density * 20);
            }
        });
    }

    public static void showAlertDialog(final Context context, String headerMessage, String messageInfo, final OnButtonClickListener onButtonClick) {
        if (context == null) {
            return;
        }
        if (((BaseActivity) context).isFinishing()) {
            return;
        }
        if (StringUtils.isNullOrEmpty(headerMessage)) {
            alertDialog = new AlertDialog.Builder(context).setMessage(messageInfo).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    onButtonClick.onButtonClick(BUTTON_CLICK_SUCCESS);
                }
            }).show();
        } else {
            alertDialog = new AlertDialog.Builder(context).setTitle(headerMessage).setMessage(messageInfo).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    onButtonClick.onButtonClick(BUTTON_CLICK_SUCCESS);
                }
            }).setCancelable(false).show();
        }

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                alertDialog.getButton(Dialog.BUTTON_POSITIVE).setTextSize(context.getResources().getDisplayMetrics().density * 20);
            }
        });
    }


    public interface OnButtonClickListener {
        void onButtonClick(int buttonId);
    }

}
