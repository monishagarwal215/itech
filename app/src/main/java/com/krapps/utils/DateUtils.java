package com.krapps.utils;


import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateUtils {

    public static String getRelativeDateTimeDiff(Date taskDate) {
        if (taskDate == null) {
            return "";
        }
        long commentDateInMillis = taskDate.getTime();

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        long currentDateInMillis = DateUtils.getDateInMillis(sdf.format(new Date()), "yyyy-MM-dd HH:mm:ss");

        return TimeUtils.millisToLongDHMS(currentDateInMillis - commentDateInMillis);
    }

    public static String getNextDate(String curDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date date = sdf.parse(curDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            return sdf.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static long getDateInMillis(String curDate, String dateFormat) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
            Date date = sdf.parse(curDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
            e.printStackTrace();
            return System.currentTimeMillis();
        }
    }

    public static String getWeekDay(String dateFormat, String curDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
            Date date = sdf.parse(curDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            int day_of_week = calendar.get(Calendar.DAY_OF_WEEK);
            String[] weekArray = {"Sun", "Mon", "Tue", "Wed", "Thur", "Fri",
                    "Sat"};
            return weekArray[day_of_week - 1];
        } catch (ParseException e) {
            e.printStackTrace();
            return "Sun";
        }
    }

    public static long getDateDiff(String date1, String date2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date dateFirst = null;
        Date dateSecond = null;
        try {
            dateFirst = sdf.parse(date1);
            dateSecond = sdf.parse(date2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        TimeUnit timeUnit = TimeUnit.DAYS;
        long diffInMillies = dateSecond.getTime() - dateFirst.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static String getInitialDateofSelectedMonth(String selectedDate) {
        try {
            String firstDate = "01-" + selectedDate.split("-")[1] + "-" + selectedDate.split("-")[2];
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            Date date = sdf.parse(selectedDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, 1);
            return sdf.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getNextMonthSelectedDate(String curDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date date = sdf.parse(curDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, 1);
            return sdf.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static int getDaysInMonth(String selectedDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date date = sdf.parse(selectedDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            // Get the number of days in that month
            int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            return daysInMonth;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 30;
        }
    }

    /**
     * ex: convert date from "dd-MM-yyyy" to MMM dd
     *
     * @param currentFormat
     * @param requiredFormat
     * @param curDate
     * @return
     */
    public static String getFormattedDate(String currentFormat, String requiredFormat, String curDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(currentFormat, Locale.US);
            SimpleDateFormat requiredSdf = new SimpleDateFormat(requiredFormat, Locale.US);
            Date date = sdf.parse(curDate);
            return requiredSdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return curDate;
        }
    }

    public static String getFormattedDateFromMillis(long alarmTime, String format) {
        try {
            Date date = new Date(alarmTime);
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return alarmTime + "";
        }
    }

    private static int getDifferenceStartEndDate(String dateTimeOne, String dateTimeTwo) {
        try {

            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss");
            Date d1 = f.parse(dateTimeOne);
            Date d2 = f.parse(dateTimeTwo);

            return d1.compareTo(d2);
        } catch (Exception e) {
        }
        return 0;
    }

    private static Date getDate(String dateTime) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss");
        Date d1 = null;
        try {
            d1 = f.parse(dateTime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return d1;
    }

    public static int getAge(String birthday) {

        int ageYr = 0;
        String a[] = birthday.split("/");

        if (a.length > 1) {

            // set up date of birth
            Calendar calDOB = Calendar.getInstance();
            calDOB.set(Integer.parseInt(a[2]), Integer.parseInt(a[0]), Integer.parseInt(a[1]));
            // setup calNow as today.
            Calendar calNow = Calendar.getInstance();
            calNow.setTime(new java.util.Date());
            // calculate age in years.
            ageYr = (calNow.get(Calendar.YEAR) - calDOB.get(Calendar.YEAR));
            // calculate additional age in months, possibly adjust years.
            int ageMo = ((calNow.get(Calendar.MONTH) + 1) - calDOB.get(Calendar.MONTH));
            if (ageMo < 0) {
                // adjust years by subtracting one
                ageYr--;
            } else if (ageMo == 0 && calNow.get(Calendar.DATE) < calDOB.get(Calendar.DATE)) {
                ageYr--;
            }
        }
        return ageYr;

    }

    public static int calculateAge(Date birthDate) {
        int years = 0;
        int months = 0;
        int days = 0;
        // create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());
        // create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);
        // Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;
        // Get difference between months
        months = currMonth - birthMonth;
        // if month difference is in negative then reduce years by one and
        // calculate the number of months.
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }
        // Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        // Create new Age object
        return years;
    }

    public static String getAgeAgoArray(Date birthDate) {
        int years = 0;
        int months = 0;
        int days = 0;
        // create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());
        // create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);
        // Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;
        // Get difference between months
        months = currMonth - birthMonth;
        // if month difference is in negative then reduce years by one and
        // calculate the number of months.
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }
        // Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        int a[] = {days, months, years};
        String temp = getAgoString(a);
        // Create new Age object
        return temp;
    }

    private static String getAgoString(int[] a) {
        int days = a[0];
        int months = a[1];
        int years = a[2];

        if (years >= 1) {
            return years + " Year ago";
        } else if (months >= 1 && months < 12) {
            if (months == 1) {
                return "1 Month ago";
            } else {
                return months + " Months ago";
            }
        } else if (days >= 0 && days <= 31) {
            int weeks = days / 7;
            if (weeks == 1) {
                return "1 Week ago";
            } else if (weeks >= 2 && weeks <= 4) {
                return weeks + " Weeks ago";
            }
            if (days == 0) {
                return "Today";
            }
            if (days == 1) {
                return "Yesterday";
            } else {
                return days + " days ago";
            }
        }
        return "";
    }

	/*
     * private static int compareDateFromCurrent(Date date){ try { DateFormat
	 * dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); Date
	 * currentDate = new Date(); } catch (Exception e) { // TODO: handle
	 * exception }
	 * 
	 * 
	 * }
	 */

    public static long getDateTimeDiff(Date currentTime, Date activeTill) {
        if (activeTill != null && currentTime != null) {
            long milis = activeTill.getTime() - currentTime.getTime();
            return milis;
        } else {
            return 0;
        }

    }

    public static CharSequence getTimerText(Date activeTill) {
        long time = 0;
        final int SECOND = 1000;
        final int MINUTE = 60 * SECOND;
        final int HOUR = 60 * MINUTE;
        final int DAY = 24 * HOUR;
        if (activeTill != null)
            time = activeTill.getTime() - System.currentTimeMillis();

        StringBuffer text = new StringBuffer("");

        if (time > DAY) {
            text.append(String.format("%02d", time / DAY)).append(":");
            time %= DAY;
        } else {
            text.append("00").append("").append(":");
        }
        if (time > HOUR) {
            text.append(String.format("%02d", time / HOUR)).append(":");
            time %= HOUR;
        } else {
            text.append("00").append("").append(":");
        }
        if (time > MINUTE) {
            text.append(String.format("%02d", time / MINUTE)).append(":");
            time %= MINUTE;
        } else {
            text.append("00").append("").append(":");
        }
        if (time > SECOND) {
            text.append(String.format("%02d", time / SECOND)).append("");
            time %= SECOND;
        } else {
            text.append("00").append("");
        }

        return text.toString();

    }

    public static CharSequence getEventTimerText(Date activeTill) {
        long time = 0;
        final int SECOND = 1000;
        final int MINUTE = 60 * SECOND;
        final int HOUR = 60 * MINUTE;
        final int DAY = 24 * HOUR;
        if (activeTill != null)
            time = activeTill.getTime() - System.currentTimeMillis();

        StringBuffer text = new StringBuffer("");

        if (time > DAY) {
            text.append(String.format("%02d", time / DAY)).append(":");
            time %= DAY;
        } else {
            text.append("00").append("").append(":");
        }
        if (time > HOUR) {
            text.append(String.format("%02d", time / HOUR)).append(":");
            time %= HOUR;
        } else {
            text.append("00").append("").append(":");
        }
        if (time > MINUTE) {
            text.append(String.format("%02d", time / MINUTE)).append(":");
            time %= MINUTE;
        } else {
            text.append("00").append("").append(":");
        }
        if (time > SECOND) {
            text.append(String.format("%02d", time / SECOND)).append("");
            time %= SECOND;
        } else {
            text.append("00").append("");
        }

        return text.toString();

    }

    public static String getDateInfoArray(long milis) {
        String[] dateArray = new String[3];
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milis);
            if (calendar.get(Calendar.DATE) < 10) {
                dateArray[0] = "0" + calendar.get(Calendar.DATE);
            } else {
                dateArray[0] = calendar.get(Calendar.DATE) + "";
            }
            dateArray[1] = getMonthString(calendar.get(Calendar.MONTH));
            dateArray[2] = calendar.get(Calendar.YEAR) + "";
            return dateArray[0] + " " + dateArray[1] + " " + dateArray[2];
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    private static String getMonthString(int month) {
        switch (month) {
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";
            default:
                return "Dec";
        }
    }

    public static Date getTwentyYearAfterDate() {
        Date todayDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(todayDate);
        calendar.add(Calendar.YEAR, 20);
        Date expireDate = new Date(calendar.getTimeInMillis());
        return expireDate;
    }

    public static CharSequence getDateText(Date activatedAt) {
        SimpleDateFormat format1 = new SimpleDateFormat("dd MMM");
        DateTime todayDate = new DateTime();
        DateTime jodaActivatedTime = new DateTime(activatedAt);
        if (todayDate.toLocalDate().isEqual(jodaActivatedTime.toLocalDate())) {
            return "Today";
        }
        if (todayDate.minusDays(1).toLocalDate().isEqual(jodaActivatedTime.toLocalDate())) {
            return "Yesterday";
        }
        return format1.format(activatedAt);
    }
}
